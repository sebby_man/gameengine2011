#include "SceneLoader.h"

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <tinyxml/tinyxml.h>
#include "refinery/deindexer.h"
#include "refinery/polylists2triangles.h"
#include "refinery/axisconverter.h"
#include "Parsers.h"


void SceneLoader::createSceneFile(const std::string& fileName, const std::string& name, const std::string& outputDir)
{
    std::ifstream infile(fileName);
    std::string line;
    std::getline(infile, line);

    //std::istringstream iStream(line);
    //iStream >> rootQuat.x >> rootQuat.y >> rootQuat.z >> rootQuat.w;
    //std::getline(infile, line);
    //iStream = std::istringstream(line);
    //iStream >> rootTrans.x >> rootTrans.y >> rootTrans.z;
    //infile.close();



    // Create the XML file
	TiXmlDocument docXML;
    
    // Scene
 	TiXmlElement* headXML = new TiXmlElement("scene");
	docXML.LinkEndChild(headXML);

        // Mesh buffers
        TiXmlElement* meshBuffersXML = new TiXmlElement("mesh_buffers");
        headXML->LinkEndChild(meshBuffersXML);
            
        std::getline(infile, line);
        while(line != "models")
        {
            std::vector<std::string> data = parseSpaceSeparatedString(line);
            int vertexSize = atoi(data[0].c_str());
            int elementSize = atoi(data[1].c_str());
            int vboSize = atoi(data[2].c_str());
            int iboSize = atoi(data[3].c_str());

            TiXmlElement* meshBufferXML = new TiXmlElement("mesh_buffer");
            meshBuffersXML->LinkEndChild(meshBufferXML);
            meshBufferXML->SetAttribute("vertex_size", vertexSize);
            meshBufferXML->SetAttribute("element_size", elementSize);
            meshBufferXML->SetAttribute("vbo_size", vboSize);
            meshBufferXML->SetAttribute("ibo_size", iboSize);

            std::getline(infile, line);
        }
            
        // Model list
        TiXmlElement* modelListXML = new TiXmlElement("models");
        headXML->LinkEndChild(modelListXML);

        std::getline(infile, line);
        while(line != "entities")
        {
            std::string modelName = convertToFowardSlashes(line);
            TiXmlElement* modelXML = new TiXmlElement("model");
            modelListXML->LinkEndChild(modelXML);
            modelXML->SetAttribute("filename", modelName.c_str());

            std::getline(infile, line);
        }


        // Entities
        TiXmlElement* entitiesXML = new TiXmlElement("entities");
        headXML->LinkEndChild(entitiesXML);

        std::getline(infile, line);
        while(line != "lights")
        {
            TiXmlElement* entityXML = new TiXmlElement("entity");
            entitiesXML->LinkEndChild(entityXML);

            // entity name
            entityXML->SetAttribute("name", line.c_str());
            std::getline(infile, line);

            // model name
            TiXmlElement* modelSourceXML = new TiXmlElement("model");
            entityXML->LinkEndChild(modelSourceXML);
            TiXmlText* modelSourceTextXML = new TiXmlText(line.c_str());
            modelSourceXML->LinkEndChild(modelSourceTextXML);
            std::getline(infile, line);
            
            // translate, rotate, scale
            std::vector<std::string> data = parseSpaceSeparatedString(line);
            glm::vec3 translation((float)(atof(data[0].c_str())) , (float)(atof(data[1].c_str())) , (float)(atof(data[2].c_str())) );
            std::getline(infile, line);
            data = parseSpaceSeparatedString(line);
            glm::vec3 scale((float)(atof(data[0].c_str())) , (float)(atof(data[1].c_str())) , (float)(atof(data[2].c_str())) );
            // Swap y and z for blender->engine conversion
            float scaleTemp = scale.z;
            scale.z = scale.y;
            scale.y = scaleTemp;
            std::getline(infile, line);
            data = parseSpaceSeparatedString(line);
            glm::quat rotation((float)(atof(data[3].c_str())) , (float)(atof(data[0].c_str())) , (float)(atof(data[1].c_str())), (float)(atof(data[2].c_str())) );
            
            glm::mat4 rotateTranslateMatrix = glm::toMat4(rotation);
            rotateTranslateMatrix[3] = glm::vec4(translation, 1.0f);
            
            glm::quat returnedRotation;
            glm::vec3 returnedTranslation;
            axisConverter(rotateTranslateMatrix, returnedRotation, returnedTranslation);

            TiXmlElement* translateXML = new TiXmlElement("translate");
            entityXML->LinkEndChild(translateXML);
            std::stringstream translateSS;
            translateSS << returnedTranslation.x << " " << returnedTranslation.y << " " << returnedTranslation.z;
            TiXmlText* translateTextXML = new TiXmlText(translateSS.str().c_str());
            translateXML->LinkEndChild(translateTextXML);

            TiXmlElement* scaleXML = new TiXmlElement("scale");
            entityXML->LinkEndChild(scaleXML);
            std::stringstream scaleSS;
            scaleSS << scale.x << " " << scale.y << " " << scale.z;
            TiXmlText* scaleTextXML = new TiXmlText(scaleSS.str().c_str());
            scaleXML->LinkEndChild(scaleTextXML);

            TiXmlElement* rotateXML = new TiXmlElement("rotate");
            entityXML->LinkEndChild(rotateXML);
            std::stringstream rotateSS;
            rotateSS << returnedRotation.x << " " << returnedRotation.y << " " << returnedRotation.z <<  " " << returnedRotation.w;
            TiXmlText* rotateTextXML = new TiXmlText(rotateSS.str().c_str());
            rotateXML->LinkEndChild(rotateTextXML);
                 

            std::getline(infile, line);          
        }

        // Lights
        TiXmlElement* lightsXML = new TiXmlElement("lights");
        headXML->LinkEndChild(lightsXML);

        std::getline(infile, line);
        while(line != "")
        {
            TiXmlElement* lightXML = new TiXmlElement("light");
            lightsXML->LinkEndChild(lightXML);

            // entity name
            lightXML->SetAttribute("name", line.c_str());
            std::getline(infile, line);

            // light type
            TiXmlElement* lightTypeXML = new TiXmlElement("type");
            lightXML->LinkEndChild(lightTypeXML);
            TiXmlText* lightTypeTextXMl = new TiXmlText(line.c_str());
            lightTypeXML->LinkEndChild(lightTypeTextXMl);
            std::getline(infile, line);

            // light color
            TiXmlElement* lightColorXML = new TiXmlElement("color");
            lightXML->LinkEndChild(lightColorXML);
            TiXmlText* lightColorTextXML = new TiXmlText(line.c_str());
            lightColorXML->LinkEndChild(lightColorTextXML);
            std::getline(infile, line);

            // translate, rotate
            std::vector<std::string> data = parseSpaceSeparatedString(line);
            glm::vec3 translation((float)(atof(data[0].c_str())) , (float)(atof(data[1].c_str())) , (float)(atof(data[2].c_str())) );
            std::getline(infile, line);
            data = parseSpaceSeparatedString(line);
            glm::quat rotation((float)(atof(data[3].c_str())) , (float)(atof(data[0].c_str())) , (float)(atof(data[1].c_str())), (float)(atof(data[2].c_str())) );
            
            glm::mat4 rotateTranslateMatrix = glm::toMat4(rotation);
            rotateTranslateMatrix[3] = glm::vec4(translation, 1.0f);
            
            glm::quat returnedRotation;
            glm::vec3 returnedTranslation;
            axisConverter(rotateTranslateMatrix, returnedRotation, returnedTranslation);
            returnedRotation = glm::rotate(returnedRotation, -90.0f, glm::vec3(1,0,0)); // flip so default direction is point foward, not down

            TiXmlElement* translateXML = new TiXmlElement("translate");
            lightXML->LinkEndChild(translateXML);
            std::stringstream translateSS;
            translateSS << returnedTranslation.x << " " << returnedTranslation.y << " " << returnedTranslation.z;
            TiXmlText* translateTextXML = new TiXmlText(translateSS.str().c_str());
            translateXML->LinkEndChild(translateTextXML);

            TiXmlElement* rotateXML = new TiXmlElement("rotate");
            lightXML->LinkEndChild(rotateXML);
            std::stringstream rotateSS;
            rotateSS << returnedRotation.x << " " << returnedRotation.y << " " << returnedRotation.z << " " << returnedRotation.w;
            TiXmlText* rotateTextXML = new TiXmlText(rotateSS.str().c_str());
            rotateXML->LinkEndChild(rotateTextXML);

            std::getline(infile, line);
        }


	//// Get the visual scene from the collada file
	//domVisual_scene* visualScene = daeSafeCast<domVisual_scene>(root->getDescendant("visual_scene"));
	//domNode_Array nodes = visualScene->getNode_array();
    //
	//// For each node in the scene...
	//for (size_t i = 0; i < nodes.getCount(); i++)
	//{
    //    domNode* node = nodes[i];
    //    bool isLight = node->getInstance_light_array().getCount() == 1;
    //
    //    // Get scale and translation
    //    std::string translateValue = node->getTranslate_array()[0]->getCharData();
    //    std::string scaleValue = node->getScale_array()[0]->getCharData();
    //            
    //    // Rotation. Apply the three axis-aligned rotations into a single quat
    //    glm::fquat finalQuat;
    //    domRotate_Array rotations = node->getRotate_array();
    //    for(int j = 0; j < rotations.getCount(); j++)
    //    {
    //        domFloat4 rotation = rotations[j]->getValue();
    //        float angle = (float)(rotation[3]);
    //        glm::vec3 axis = glm::vec3((float)(rotation[0]), (float)(rotation[1]), (float)(rotation[2]));
    //        finalQuat = glm::rotate(finalQuat, angle, axis);
    //    }
    //
    //    // Rotate light 180 degrees about x axis so that light direction is pointing up instead of blender's default of pointing down
    //    if(isLight) finalQuat = glm::rotate(finalQuat, 180.0f, glm::vec3(1,0,0)); 
    //
    //    glm::vec3 finalAxis = glm::axis(finalQuat);
    //    float finalAngle = glm::angle(finalQuat);
    //    std::stringstream rotationValueSS;
    //    rotationValueSS << finalAxis[0] << " " << finalAxis[1] << " " << finalAxis[2] << " " << finalAngle;
    //    std::string rotationValue = rotationValueSS.str();
    //
    //    if(isLight)
    //    {
    //        domInstance_light* lightInstance = node->getInstance_light_array()[0];
    //        domLight* light = daeSafeCast<domLight>(lightInstance->getUrl().getElement());
    //
    //        // Get light type
    //        std::string lightType;
    //        if(light->getDescendant("directional") != 0) lightType = "Dir";
    //        else if(light->getDescendant("point") != 0) lightType = "Point";
    //        else if(light->getDescendant("spot") != 0) lightType = "Spot";
    //
    //        // Create light XML
    //        TiXmlElement* lightXML = new TiXmlElement("light");
    //        lightsXML->LinkEndChild(lightXML);
    //        lightXML->SetAttribute("type", lightType.c_str());
    //
    //        // Set name
    //        TiXmlElement* nameXML = new TiXmlElement("name");
    //        lightXML->LinkEndChild(nameXML);
    //            TiXmlText* nameText = new TiXmlText(node->getID());
    //            nameXML->LinkEndChild(nameText);
    //        
    //        // Color
    //        std::string colorString = light->getDescendant("color")->getCharData();
    //        TiXmlElement* colorXML = new TiXmlElement("color"); 
    //        lightXML->LinkEndChild(colorXML);
    //            TiXmlText* colorTextXML = new TiXmlText(colorString.c_str());
    //            colorXML->LinkEndChild(colorTextXML);
    //        
    //
    //        // Direction
    //        glm::vec3 direction = glm::normalize(glm::toMat3(finalQuat)[1]);
    //        std::stringstream directionSS;
    //        directionSS << direction[0] << " " << direction[1] << " " << direction[2];
    //        std::string directionString = directionSS.str();
    //
    //        if(lightType == "Spot")
    //        {               
    //            // Angle
    //            std::string angleString = light->getDescendant("falloff_angle")->getCharData();
    //            TiXmlElement* angleXML = new TiXmlElement("angle"); 
    //            lightXML->LinkEndChild(angleXML);
    //                TiXmlText* angleTextXML = new TiXmlText(angleString.c_str());
    //                angleXML->LinkEndChild(angleTextXML);
    //            
    //
    //            // Distance
    //            std::string distanceString = light->getDescendant("dist")->getCharData();
    //            TiXmlElement* distanceXML = new TiXmlElement("distance"); 
    //            lightXML->LinkEndChild(distanceXML);
    //                TiXmlText* distanceTextXML = new TiXmlText(distanceString.c_str());
    //                distanceXML->LinkEndChild(distanceTextXML);
    //            
    //
    //            // Direction
    //            TiXmlElement* directionXML = new TiXmlElement("direction");
    //            lightXML->LinkEndChild(directionXML);
    //                TiXmlText* directionTextXML = new TiXmlText(directionString.c_str());
    //                directionXML->LinkEndChild(directionTextXML);
    //            
    //
    //            // Translation
    //            TiXmlElement* translateXML = new TiXmlElement("translate");
    //            lightXML->LinkEndChild(translateXML);
    //                TiXmlText* translateTextXML = new TiXmlText(translateValue.c_str());
    //                translateXML->LinkEndChild(translateTextXML);
    //            
    //
    //            // Rotation
    //            TiXmlElement* rotateXML = new TiXmlElement("rotate");
    //            lightXML->LinkEndChild(rotateXML);
    //                TiXmlText* rotateTextXML = new TiXmlText(rotationValue.c_str());
    //                rotateXML->LinkEndChild(rotateTextXML);
    //            
    //        }
    //        else if(lightType == "Dir")
    //        {
    //            // Direction
    //            TiXmlElement* directionXML = new TiXmlElement("direction");
    //            lightXML->LinkEndChild(directionXML);
    //                TiXmlText* directionTextXML = new TiXmlText(directionString.c_str());
    //                directionXML->LinkEndChild(directionTextXML);
    //            
    //
    //            // Rotation
    //            TiXmlElement* rotateXML = new TiXmlElement("rotate");
    //            lightXML->LinkEndChild(rotateXML);
    //                TiXmlText* rotateTextXML = new TiXmlText(rotationValue.c_str());
    //                rotateXML->LinkEndChild(rotateTextXML);
    //            
    //
    //            // Translation
    //            TiXmlElement* translateXML = new TiXmlElement("translate");
    //            lightXML->LinkEndChild(translateXML);
    //                TiXmlText* translateTextXML = new TiXmlText(translateValue.c_str());
    //                translateXML->LinkEndChild(translateTextXML);
    //            
    //        }
    //        else if(lightType == "Point")
    //        {
    //            // Distance
    //            std::string distanceString = light->getDescendant("dist")->getCharData();
    //            TiXmlElement* distanceXML = new TiXmlElement("distance"); 
    //            lightXML->LinkEndChild(distanceXML);
    //                TiXmlText* distanceTextXML = new TiXmlText(distanceString.c_str());
    //                distanceXML->LinkEndChild(distanceTextXML);
    //            
    //
    //            // Translation
    //            TiXmlElement* translateXML = new TiXmlElement("translate");
    //            lightXML->LinkEndChild(translateXML);
    //                TiXmlText* translateTextXML = new TiXmlText(translateValue.c_str());
    //                translateXML->LinkEndChild(translateTextXML);
    //            
    //        }
    //        else
    //        {
    //            printf("not a supported light type");
    //            continue;
    //        }
    //    }
    //
	//    // If this node is a library-linked node
    //    if(node->getInstance_light_array().getCount() == 0 &&
    //        node->getInstance_camera_array().getCount() == 0 &&
    //        node->getInstance_controller_array().getCount() == 0 &&
    //        node->getInstance_node_array().getCount() == 0 &&
    //        node->getInstance_geometry_array().getCount() == 0)
    //    {
    //        // Get model name, find if it has a number attached
    //        bool newModelName = true;
    //
    //        std::string entityName = node->getID();
    //        std::string modelName = entityName;
    //
    //        // See if this entity is numbered, meaning it's a copy of a linked entity
    //        int indexToNumber = entityName.find_last_of('_');
    //        if(indexToNumber != std::string::npos)
    //        {
    //            if(isdigit((char)(entityName.at(indexToNumber+1))))
    //            {
    //                modelName = modelName.substr(0, indexToNumber);
    //                newModelName = false;
    //            }
    //        }
    //
    //        if(newModelName && modelNames.insert(modelName).second == true)
    //        {
    //            TiXmlElement* modelXML = new TiXmlElement("model");
    //            modelsXML->LinkEndChild(modelXML);
    //            modelXML->SetAttribute("name", modelName.c_str());
    //        }
    //
    //        // Entity XML
    //        TiXmlElement* entityXML = new TiXmlElement("entity");
    //        entitiesXML->LinkEndChild(entityXML);
    //
    //        // Name
    //        TiXmlElement* nameXML = new TiXmlElement("name");
    //        entityXML->LinkEndChild(nameXML);
    //            TiXmlText* nameTextXML = new TiXmlText(entityName.c_str());
    //            nameXML->LinkEndChild(nameTextXML);
    //        
    //        // Model
    //        TiXmlElement* modelXML = new TiXmlElement("model");
    //        entityXML->LinkEndChild(modelXML);
    //            TiXmlText* modelTextXML = new TiXmlText(modelName.c_str());
    //            modelXML->LinkEndChild(modelTextXML);
    //        
    //        // Translate
    //        TiXmlElement* translateXML = new TiXmlElement("translate");
    //        entityXML->LinkEndChild(translateXML);
    //            TiXmlText* translateTextXML = new TiXmlText(translateValue.c_str());
    //            translateXML->LinkEndChild(translateTextXML);
    //
    //        // Scale
    //        TiXmlElement* scaleXML = new TiXmlElement("scale");
    //        entityXML->LinkEndChild(scaleXML);
    //            TiXmlText* scaleTextXML = new TiXmlText(scaleValue.c_str());
    //            scaleXML->LinkEndChild(scaleTextXML);
    //
    //        // Rotate
    //        TiXmlElement* rotateXML = new TiXmlElement("rotate");
    //        entityXML->LinkEndChild(rotateXML);  
    //            TiXmlText* rotateTextXML = new TiXmlText(rotationValue.c_str());
    //            rotateXML->LinkEndChild(rotateTextXML);
    //                  
    //    }
    //}

    infile.close();
    std::string saveName = outputDir + name + ".scene";
    docXML.SaveFile(saveName.c_str());
}