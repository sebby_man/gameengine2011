#pragma once

#include <iostream>
#include <sstream>
#include <algorithm>
#include <gli/gli.hpp>
#include <gli/gtx/fetch.hpp>
#include <gli/gtx/gradient.hpp>
#include <gli/gtx/loader.hpp>
#include <fstream>
#include <istream>
#include <sstream>
#include <string>
typedef unsigned int uint;

struct TextureGroup
{
    TextureGroup(gli::format format, const glm::uvec2& resolution, uint numMips);

    bool operator==(const TextureGroup& other) const;
    void insertTexture(const std::string& textureName);

    std::vector<std::string> m_textures;
    gli::format m_format;
    const glm::uvec2 m_resolution;
    const uint m_numMips;
};

class TextureLoader
{

public:

    void exportTextures(const std::string& fileName);

private:

    std::vector<TextureGroup> m_textureGroups;

};