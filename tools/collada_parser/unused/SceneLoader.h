#pragma once

#include <string>
#include <set>

class SceneLoader
{

public:

    void createSceneFile(const std::string& fileName, const std::string& name, const std::string& outputName);

private:

    std::set<std::string> modelNames;

};