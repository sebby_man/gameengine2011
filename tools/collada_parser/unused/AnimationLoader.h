#pragma once

#include <vector>
#include <map>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

class TiXmlElement;
class daeElement;
class domInstance_controller;
class domNode;
class VertexGroup;
class domLibrary_animations;

class AnimationLoader
{

public:

    AnimationLoader();
	TiXmlElement* loadController(domInstance_controller* instanceController, VertexGroup* animatedVertexGroup);
    void loadAnimationClip(const std::string& file);
    void loadRootMatrixFile(const std::string& file);
    TiXmlElement* getAnimationClipsXML();

private:

    glm::quat correctQuaternion(const glm::quat& quaternion);

	void loadSkeleton(domNode* node, TiXmlElement* parentXML);
   
    
    TiXmlElement* animationClipsXML;

    glm::quat rootQuat;
    glm::vec3 rootTrans;

    std::vector<std::string> invBindPoseMatrixStrings;
    std::map<std::string, int> namesToIDs;
    std::vector<glm::quat> defaultRotations;
    std::vector<glm::vec3> defaultTranslations;
};

