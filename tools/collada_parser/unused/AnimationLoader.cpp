#include "AnimationLoader.h"

#include <fstream>
#include <istream>
#include <sstream>
#include <glm/gtx/epsilon.hpp>
#include <dae.h>
#include <dom/domCOLLADA.h>
#include <tinyxml/tinyxml.h>
#include "refinery/axisconverter.h"
#include "refinery/polylists2triangles.h"
#include "refinery/deindexer.h"
#include "GeometryLoader.h"
#include "Parsers.h"
#include "Model.h"


AnimationLoader::AnimationLoader()
{
    animationClipsXML = new TiXmlElement("animations");
}

TiXmlElement* AnimationLoader::loadController(domInstance_controller* instanceController, VertexGroup* vertexGroup)
{
    domController* controller = daeSafeCast<domController>(instanceController->getUrl().getElement());         

	// Bone names
	std::string boneNames = daeSafeCast<domSource>(controller->getSkin()->getVertex_weights()->getInput_array()[0]->getSource().getElement())->getName_array()->getCharData();

	// Get bind shape matrix
	std::string bindShapeMatrixString = controller->getSkin()->getBind_shape_matrix()->getCharData();
	glm::mat4 bindShapeMatrix = parseDataIntoMat4(bindShapeMatrixString, 1)[0];

	// Get num joints data
    int numVertices = controller->getSkin()->getVertex_weights()->getCount();
    std::string numJointsString = controller->getSkin()->getVertex_weights()->getVcount()->getCharData();
	std::vector<int> numJointsData = parseStringIntoInts(numJointsString, numVertices);
	
    // Get weights data
	domSource* weightsSource = daeSafeCast<domSource>(controller->getSkin()->getVertex_weights()->getInput_array()[1]->getSource().getElement());
	unsigned int numWeights = weightsSource->getFloat_array()->getCount();
	std::string weightsDataString = weightsSource->getFloat_array()->getCharData();
	std::vector<float> weightsData = parseDataIntoFloats(weightsDataString, numWeights);

	// Get joints and weights index data. Series of <joint ID, weight ID> pairs.
	std::string jointsAndWeightDataString = controller->getSkin()->getVertex_weights()->getV()->getCharData();
	std::vector<int> jointsAndWeightsData = parseStringIntoInts(jointsAndWeightDataString, numWeights*2);	
	

    // Set vertex data with joint data
    unsigned int jointDataIndex = 0;
    for(unsigned int i = 0; i < numJointsData.size(); i++)
    {
        int numJoints = numJointsData[i];
                
        VertexBoneData boneData;
        boneData.numBones = glm::max(numJoints, 4);
        for(int j = 0; j < numJoints; j++)
        {
            if(j < 4) // limit to max of 4 joints
            {
                int boneID = jointsAndWeightsData[jointDataIndex];
                float boneWeight = weightsData[jointsAndWeightsData[jointDataIndex + 1]];

                boneData.boneIDs[j] = boneID;
                boneData.boneWeights[j] = boneWeight;
            }

            jointDataIndex += 2;
        }

        vertexGroup->vertexBoneData[vertexGroup->numVerticesAnimated] = boneData;
        vertexGroup->numVerticesAnimated++;
    }


	// Set inv bind pose matrices
	domInputLocal_Array inputArray = controller->getSkin()->getJoints()->getInput_array();
	for(unsigned int i = 0; i < inputArray.getCount(); i++)
	{
		std::string semantic = inputArray[i]->getSemantic();
		if(semantic == "JOINT")
		{
			domSource* jointSource = daeSafeCast<domSource>(inputArray[i].cast()->getSource().getElement());
			for(unsigned int j = 0; j < jointSource->getName_array()->getCount(); j++)
			{
				std::string jointName = jointSource->getName_array()->getValue().get(j);
				namesToIDs[jointName] = j;
			}

		}
		else if(semantic == "INV_BIND_MATRIX") // must appear after the JOINT semantic
		{
			domSource* invBindMatricesSource = daeSafeCast<domSource>(inputArray[i]->getSource().getElement());

			unsigned int numMatrices = (unsigned int)invBindMatricesSource->getFloat_array()->getCount()/16;
			std::string matrixData = invBindMatricesSource->getFloat_array()->getCharData();
			std::vector<glm::mat4> matrices = parseDataIntoMat4(matrixData, numMatrices);
			
			for(unsigned int j = 0; j < numMatrices; j++)
			{
				matrices[j] = matrices[j]*bindShapeMatrix;
			}
			matrixData = convertMat4sToString(matrices);
			std::vector<std::string> matrixDataStrings = parseMatrixDataIntoStrings(matrixData, numMatrices);
			for(unsigned int j = 0; j < matrixDataStrings.size(); j++)
			{
				invBindPoseMatrixStrings.push_back(matrixDataStrings[j]);
			}
		}
    }

    defaultRotations.resize(namesToIDs.size());
    defaultTranslations.resize(namesToIDs.size());

    // Create skeleton XML node
    domNode* sketonRootJoint = daeSafeCast<domNode>(instanceController->getSkeleton_array()[0]->getValue().getElement());
    TiXmlElement* skeletonXML = new TiXmlElement("skeleton");
    loadSkeleton(sketonRootJoint, skeletonXML);

    return skeletonXML;
}

glm::quat AnimationLoader::correctQuaternion(const glm::quat& quaternion)
{
    glm::quat returnQuat = quaternion;
    if(returnQuat.w < 0.0f)
    {
        returnQuat *= -1.0f;
    }
    return returnQuat;
}


// Recursive
void AnimationLoader::loadSkeleton(domNode* node, TiXmlElement* parentXML)
{

	std::string name = node->getId();
	
    ////loop over the translations attached to this node (probably won't encounter this)
	//for (size_t i = 0; i < node->getTranslate_array().getCount(); i++) {
	//    domTranslate* translation = node->getTranslate_array()[i];
	//    domFloat3& value = translation->getValue();
	//    joint->translate(glm::vec3((float)value.get(0), (float)value.get(1), (float)value.get(2)), false);
	//}
    //
	////loop over the orientations attached to this node (probably won't encounter this)
	//for (size_t i = 0; i < node->getRotate_array().getCount(); i++) {
	//	domRotate* rotation = node->getRotate_array()[i];
	//	domFloat4& value = rotation->getValue();
	//	joint->rotate(glm::vec3((float)value.get(0), (float)value.get(1), (float)value.get(2)), (float)value.get(3), false);
	//}


	// Look for a transformation matrix (contains rotate and translate)
	if(node->getMatrix_array().getCount() == 1)
	{
		std::string matrixData = node->getMatrix_array()[0]->getCharData();
		glm::mat4 matrix = parseDataIntoMat4(matrixData, 1)[0];

        glm::fquat rotation = glm::quat_cast(glm::mat3(matrix));
		glm::vec3 translation = glm::vec3(matrix[3]);

        int jointIndex = namesToIDs.at(name);
        bool isRoot = jointIndex == 0;
		if(isRoot)
		{
            glm::mat4 combinedMat = glm::toMat4(rootQuat);
            combinedMat[3] = glm::vec4(rootTrans, 1.0f);
			axisConverter(combinedMat, rotation, translation);
		}

		rotation = correctQuaternion(rotation);

        defaultRotations[jointIndex] = rotation;
        defaultTranslations[jointIndex] = translation;

        std::stringstream translationStream;
		std::stringstream quatStream;
		translationStream << translation.x << " " << translation.y << " " << translation.z;
		quatStream << rotation.x << " " << rotation.y << " " << rotation.z << " " << rotation.w;
		std::string translationString = translationStream.str();
		std::string quatString = quatStream.str();
		
		std::string invBindPoseMatrixString = invBindPoseMatrixStrings[jointIndex];

		// Now do tinyXML stuff, incorporating trans, quat, invBindPoseMatrixString, and name
		TiXmlElement* nodeXML = new TiXmlElement("bone");
		nodeXML->SetAttribute("name", name.c_str());
		nodeXML->SetAttribute("id", jointIndex);
		parentXML->LinkEndChild(nodeXML);
			TiXmlElement* rotationXML = new TiXmlElement("rotation");
			nodeXML->LinkEndChild(rotationXML);
				TiXmlText* rotationData = new TiXmlText(quatString.c_str());
				rotationXML->LinkEndChild(rotationData);
			TiXmlElement* translationXML = new TiXmlElement("translation");
			nodeXML->LinkEndChild(translationXML);
				TiXmlText* translationData = new TiXmlText(translationString.c_str());
				translationXML->LinkEndChild(translationData);
			TiXmlElement* invBindPoseXML = new TiXmlElement("inv_bind_pose");
			nodeXML->LinkEndChild(invBindPoseXML);
				TiXmlText* invBindPoseTextXML = new TiXmlText(invBindPoseMatrixString.c_str());
				invBindPoseXML->LinkEndChild(invBindPoseTextXML);


		// Loop over children nodes
		for (unsigned int i = 0; i < node->getNode_array().getCount(); i++)
		{
			loadSkeleton(node->getNode_array()[i], nodeXML);
		}
	}
}

void AnimationLoader::loadRootMatrixFile(const std::string& file)
{
    std::ifstream infile(file);
    std::string line;
    std::getline(infile, line);
    std::istringstream iStream(line);
    iStream >> rootQuat.x >> rootQuat.y >> rootQuat.z >> rootQuat.w;
    std::getline(infile, line);
    iStream = std::istringstream(line);
    iStream >> rootTrans.x >> rootTrans.y >> rootTrans.z;
    infile.close();
}

TiXmlElement* AnimationLoader::getAnimationClipsXML()
{
    if(animationClipsXML->NoChildren())
    {
        return 0;
    }
    else
    {
        return animationClipsXML;
    }
}

void AnimationLoader::loadAnimationClip(const std::string& file)
{

    float framerate = 30.0f;

    std::ifstream infile(file);
    std::string line;
    std::getline(infile, line);
    while (line != "")
    {

        if(line == "pose_transforms")
        {
            std::getline(infile, line);
            while(line != "clip")
            {
                // Get bone name
                std::string boneName = line;
                int boneID = namesToIDs.at(boneName);
                //defaultTranslations

                std::getline(infile, line);
                std::istringstream quatIStream(line);
                std::getline(infile, line);
                std::istringstream vecIStream(line);
                std::getline(infile, line);

                glm::quat currQuat;
                glm::vec3 currVec;

                quatIStream >> currQuat.x >> currQuat.y >> currQuat.z >> currQuat.w;
                vecIStream >> currVec.x >> currVec.y >> currVec.z;

                currQuat = correctQuaternion(currQuat);

                defaultRotations[boneID] = currQuat;
                defaultTranslations[boneID] = currVec;

            }

        }

        if(line == "clip")
        {
            float duration = 0.0f;
            bool staticPose = true;

            // Create animation clip
            TiXmlElement* animationClipXML = new TiXmlElement("animation");
            animationClipsXML->LinkEndChild(animationClipXML);
            
            // Set clip name
            std::getline(infile, line);
            std::string animationClipName = line;
            animationClipXML->SetAttribute("name", line.c_str());
        
            // Get num keyframes
            std::getline(infile, line);
            int numKeyframes = atoi(line.c_str());

            // Loop over the bone targets
            std::getline(infile, line);
            while(line != "clip" && line != "")
            {

                // Get bone name
                std::string boneName = line;
                int boneID = namesToIDs.at(boneName);

                // Data storage
                std::vector<float> quatKeyframes;
                std::vector<glm::quat> quaternions;
                std::vector<float> translationKeyframes;
                std::vector<glm::vec3> translations;

                // Previous values
                glm::quat previousQuat(0.01f, 0.01f, 0.01f, 0.01f); // fake values
                glm::vec3 previousVec(666.0f, 666.0f, 666.0f); // fake values

                // Create string streams out of the data for this bone (frame times, quats, and vecs)
                std::getline(infile, line);
                std::istringstream quatIStream(line);
                std::getline(infile, line);
                std::istringstream vecIStream(line);
                std::getline(infile, line);

                std::vector<float> duplicateKeyframesQuats;
                std::vector<float> duplicateKeyframesVecs;

                // Loop over keyframe data and add keyframes to the lists
                for(int i = 0; i < numKeyframes; i++)
                {
                    // Get data for this particular frame
                    float currFrame = i/numKeyframes;
                    glm::quat currQuat;
                    glm::vec3 currVec;

                    quatIStream >> currQuat.x >> currQuat.y >> currQuat.z >> currQuat.w;
                    vecIStream >> currVec.x >> currVec.y >> currVec.z;

                    bool isRoot = boneID == 0;
                    if(isRoot)
		            {
                        glm::mat4 combinedMat = glm::toMat4(currQuat);
                        combinedMat[3] = glm::vec4(currVec, 1.0f);
			            axisConverter(combinedMat, currQuat, currVec);
		            }

                    currQuat = correctQuaternion(currQuat);

                    // Update duration
                    duration = glm::max(duration, currFrame);

                    // Check if the current quat is equal to the previous quat
                    float dot = glm::dot(currQuat, previousQuat);
                    bool quatsEqual = dot > .9995f;

                    if(quatsEqual)
                    {
                        duplicateKeyframesQuats.push_back(currFrame);
                    }
                    else
                    {
                        if(duplicateKeyframesQuats.size() > 0)
                        {
                            // Scenario: A series of duplicates in the beginning
                            if(duplicateKeyframesQuats.size() == i-1 && i > 0)
                            {
                                quatKeyframes[0] = duplicateKeyframesQuats.back();
                            }
                            // Scenario: A series of duplicates in the middle
                            else
                            {
                                quatKeyframes.push_back(duplicateKeyframesQuats.back());
                                quaternions.push_back(previousQuat);
                            }
                        }
                        
                        quaternions.push_back(currQuat);
                        quatKeyframes.push_back(currFrame);
                        duplicateKeyframesQuats.clear();
                        previousQuat = currQuat;
                    }

                    



                    // Check if the current vec is equal to the vec quat. If so, ignore the keyframe.
                    bool vecEqual = glm::all(glm::equalEpsilon(currVec, previousVec, epsilon));


                    if(vecEqual)
                    {
                        duplicateKeyframesVecs.push_back(currFrame);
                    }
                    else
                    {
                        if(duplicateKeyframesVecs.size() > 0)
                        {
                            // Scenario: A series of duplicates in the beginning
                            if(duplicateKeyframesVecs.size() == i-1 && i > 0)
                            {
                                translationKeyframes[0] = duplicateKeyframesVecs.back();
                            }
                            // Scenario: A series of duplicates in the middle
                            else
                            {
                                translationKeyframes.push_back(duplicateKeyframesVecs.back());
                                translations.push_back(previousVec);
                            }
                        }
                        
                        translations.push_back(currVec);
                        translationKeyframes.push_back(currFrame);
                        duplicateKeyframesVecs.clear();
                        previousVec = currVec;
                    }
                }

                //boneTargetXML->SetAttribute("num_keyframes", keyframes.size());
                
                

                
                bool isUnitQuaterion = glm::all(glm::equalEpsilon(glm::vec4(quaternions[0].w, quaternions[0].x, quaternions[0].y, quaternions[0].z), glm::vec4(1,0,0,0), epsilon));
                bool singleRedundantQuat = quatKeyframes.size() == 1 && (isUnitQuaterion || glm::dot(quaternions[0], defaultRotations[boneID]) > .9995f);
                bool singleRedundantTrans = translationKeyframes.size() == 1 && glm::all(glm::equalEpsilon(translations[0], defaultTranslations[boneID], epsilon));

                if((quatKeyframes.size() == 0 || singleRedundantQuat) && (translationKeyframes.size() == 0 || singleRedundantTrans))
                {
                    continue;
                }

                TiXmlElement* boneTargetXML = new TiXmlElement("bone");
                animationClipXML->LinkEndChild(boneTargetXML);
                boneTargetXML->SetAttribute("id", boneID);
                boneTargetXML->SetAttribute("name", boneName.c_str());

                if(quatKeyframes.size() > 1 || translationKeyframes.size() > 1) 
                    staticPose = false;


                for(int i = 1; i < quaternions.size(); i++)
                {
                    float dot = glm::dot(quaternions[i], quaternions[i-1]);
                    if(dot < 0.0f)
                    {
                        quaternions[i] *= -1.0f;
                    }
                }


                if(!singleRedundantQuat)
                {

                    // Output the rotation keyframes
                    std::stringstream rotationFramesSS;
                    for(unsigned int i = 0; i < quatKeyframes.size(); i++)
                    {
                        rotationFramesSS << quatKeyframes[i]/framerate << " ";
                    }
                    std::string rotationFramesString = rotationFramesSS.str();
                    rotationFramesString = rotationFramesString.substr(0, rotationFramesString.length()-1);

                    TiXmlElement* rotationFramesXML = new TiXmlElement("rotation_frames");
                    boneTargetXML->LinkEndChild(rotationFramesXML);
                    rotationFramesXML->SetAttribute("count", quatKeyframes.size());
                    TiXmlText* rotationFramesTextXML = new TiXmlText(rotationFramesString.c_str());
                    rotationFramesXML->LinkEndChild(rotationFramesTextXML);


                    // Output the quaternions
                    std::stringstream quatSStream;
                    for(unsigned int i = 0; i < quaternions.size(); i++)
                    {
                        quatSStream << quaternions[i].x << " " << quaternions[i].y << " " << quaternions[i].z << " " << quaternions[i].w << " ";
                    }
                    std::string quatsString = quatSStream.str();
                    quatsString = quatsString.substr(0, quatsString.length()-1);

                    TiXmlElement* rotationXML = new TiXmlElement("rotations");
                    boneTargetXML->LinkEndChild(rotationXML);
                    TiXmlText* rotationTextXML = new TiXmlText(quatsString.c_str());
                    rotationXML->LinkEndChild(rotationTextXML);
                }

                if(!singleRedundantTrans)
                {
                    // Output the translation keyframes
                    std::stringstream translationFramesSS;
                    for(unsigned int i = 0; i < translationKeyframes.size(); i++)
                    {
                        translationFramesSS << translationKeyframes[i]/framerate << " ";
                    }
                    std::string translationFramesString = translationFramesSS.str();
                    translationFramesString = translationFramesString.substr(0, translationFramesString.length()-1);

                    TiXmlElement* translationFramesXML = new TiXmlElement("translation_frames");
                    boneTargetXML->LinkEndChild(translationFramesXML);
                    translationFramesXML->SetAttribute("count", translationKeyframes.size());
                    TiXmlText* translationFramesTextXML = new TiXmlText(translationFramesString.c_str());
                    translationFramesXML->LinkEndChild(translationFramesTextXML);


                    // Output the translations
                    std::stringstream translationSStream;
                    for(unsigned int i = 0; i < translations.size(); i++)
                    {
                        translationSStream << translations[i].x << " " << translations[i].y << " " << translations[i].z << " ";
                    }
                    std::string translationsString = translationSStream.str();
                    translationsString = translationsString.substr(0, translationsString.length()-1);

                    TiXmlElement* translationXML = new TiXmlElement("translations");
                    boneTargetXML->LinkEndChild(translationXML);
                    TiXmlText* translationTextXML = new TiXmlText(translationsString.c_str());
                    translationXML->LinkEndChild(translationTextXML);
   
                }

                
            } 

            std::stringstream durationSS; 
            durationSS << duration/framerate;
            animationClipXML->SetAttribute("duration", durationSS.str().c_str());
            std::string boolString = staticPose ? "true" : "false"; 
            animationClipXML->SetAttribute("static_pose", boolString.c_str());
        }
    }


    infile.close();
    

    ////loop over animations
    //float duration = 0;

    //for(unsigned int i = 0; i < animationLibrary->getAnimation_array().getCount(); i++)
    //{
		
	   // domAnimation* animationNode = animationLibrary->getAnimation_array()[i];
	   // //TODO: handle more than one channel?
	   // std::string targetName = animationNode->getChannel_array()[0]->getTarget();
	   // //note: the reference element animation apparently doesn't matter
	   // daeSidRef sidRef(targetName, animationNode);
	   // daeSidRef::resolveData resolveData = sidRef.resolve();

		
	   // //the element in the visual scene that this animation channel is referencing
	   // daeElement* referenceElement = resolveData.elt;
	   // domNode* targetNode = daeSafeCast<domNode>(referenceElement->getParent());
	   // COLLADA_TYPE::TypeEnum type = referenceElement->getElementType();

	   // //is the target node a joint, or a regular node
	   // if(targetNode->getType() == NODETYPE_NODE)
	   // {

		  //  /*
		  //  //load the keyframe times and corresponding values
		  //  domInputLocal_Array inputArray = animationNode->getSampler_array()[0]->getInput_array();
				//
		  //  std::vector<float> keyframeTimes;
		  //  std::vector<float> values;
				//
		  //  for(size_t k = 0; k < inputArray.getCount(); k++)
		  //  {
			 //   std::string semantic = inputArray[k]->getSemantic();
			 //   if(semantic == "INPUT")
			 //   {
				//    domSource* timeSource = daeSafeCast<domSource>(inputArray[k]->getSource().getElement());
				//    unsigned int numKeyframes = (unsigned int)timeSource->getFloat_array()->getCount();
				//    std::string keyframeTimesString = timeSource->getFloat_array()->getCharData();
				//    keyframeTimes = parseKeyframeDataIntoFloats(keyframeTimesString, numKeyframes, duration);
			 //   }
			 //   else if(semantic == "OUTPUT")
			 //   {
				//    domSource* valuesSource = daeSafeCast<domSource>(inputArray[k]->getSource().getElement());
				//    unsigned int numKeyframes = (unsigned int)valuesSource->getFloat_array()->getCount();
				//    std::string valuesString = valuesSource->getFloat_array()->getCharData();
				//    values = parseDataIntoFloats(valuesString, numKeyframes);
			 //   }
		  //  }


		  //  //get the component that this animation is targetting (x, y, or z / x, y, z, or a)
		  //  int component;
		  //  for(unsigned int k = 0; k < resolveData.array->getCount(); k++)
		  //  {
			 //   if((daeDouble*)resolveData.array->getRaw(k) == resolveData.scalar)
			 //   {
				//    component= k;
			 //   }
		  //  }


		  //  //can be either a translation or rotation animation. Maybe scale later
		  //  if(type == COLLADA_TYPE::ROTATE)
		  //  {
			 //   AnimatedObject::RotateAnimation* rotationAnimation = new AnimatedObject::RotateAnimation();
			 //   domRotate* rotation = daeSafeCast<domRotate>(referenceElement);
			 //   domFloat4& value = rotation->getValue();
				//	
			 //   rotationAnimation->axis = glm::vec3((float)value.get(0), (float)value.get(1), (float)value.get(2));
			 //   rotationAnimation->angle = (float)value.get(3);
			 //   rotationAnimation->component = component;
			 //   rotationAnimation->keyframes = keyframeTimes;
			 //   rotationAnimation->values = values;

			 //   animationClip.animations.push_back(rotationAnimation);
		  //  }
		  //  else if(type == COLLADA_TYPE::TRANSLATE)
		  //  {
			 //   AnimatedObject::TranslateAnimation* translateAnimation = new AnimatedObject::TranslateAnimation();
			 //   domTranslate* translation = daeSafeCast<domTranslate>(referenceElement);
				//	
			 //   translateAnimation->component = component;
			 //   translateAnimation->keyframes = keyframeTimes;
			 //   translateAnimation->values = values;


			 //   animationClip.animations.push_back(translateAnimation);

		  //  }*/
	   // }
	   // else if(targetNode->getType() == NODETYPE_JOINT)
	   // {
		  //  //get the joint index that this animation is targetting
		  //  int jointIndex = namesToIDs.at(targetNode->getId());
			

		  //  bool isRoot = jointIndex == 0;


		  //  //load the keyframe times and corresponding values
		  //  domInputLocal_Array inputArray = animationNode->getSampler_array()[0]->getInput_array();
				//
		  //  std::string keyframeTimesString;
		  //  std::string quatsString;
		  //  std::string translationsString;
				//
		  //  unsigned int numKeyframes;
		  //  for(size_t k = 0; k < inputArray.getCount(); k++)
		  //  {
			 //   std::string semantic = inputArray[k]->getSemantic();
			 //   if(semantic == "INPUT")
			 //   {
				//    domSource* timeSource = daeSafeCast<domSource>(inputArray[k]->getSource().getElement());
				//    numKeyframes = (unsigned int)timeSource->getFloat_array()->getCount();
				//    keyframeTimesString = timeSource->getFloat_array()->getCharData();
				//    parseKeyframeData(keyframeTimesString, numKeyframes, duration);
				//	
			 //   }
			 //   //assumes input is processed before output beacuse of numKeyframes
			 //   else if(semantic == "OUTPUT")
			 //   {
				//    domSource* valuesSource = daeSafeCast<domSource>(inputArray[k]->getSource().getElement());
				//    std::string valuesString = valuesSource->getFloat_array()->getCharData();
				//    parseJointAnimMatrix(valuesString, numKeyframes, isRoot, translationsString, quatsString);
			 //   }
		  //  }


		  //  TiXmlElement* target = new TiXmlElement("target");
		  //  target->SetAttribute("joint_id", jointIndex);
		  //  target->SetAttribute("num_keyframes", numKeyframes);
		  //  parentElement->LinkEndChild(target);
			 //   TiXmlElement* time = new TiXmlElement("time");
			 //   target->LinkEndChild(time);
				//    TiXmlText* timeData = new TiXmlText(keyframeTimesString.c_str());
				//    time->LinkEndChild(timeData);
			 //   TiXmlElement* rotation = new TiXmlElement("rotation");
			 //   target->LinkEndChild(rotation);
				//    TiXmlText* rotationData = new TiXmlText(quatsString.c_str());
				//    rotation->LinkEndChild(rotationData);
			 //   TiXmlElement* translation = new TiXmlElement("translation");
			 //   target->LinkEndChild(translation);
				//    TiXmlText* translationData = new TiXmlText(translationsString.c_str());
				//    translation->LinkEndChild(translationData);
	   // }
    //}

    //parentElement->SetAttribute("id", "walk");
    //std::stringstream durationStream; durationStream << duration;
    //parentElement->SetAttribute("duration", durationStream.str().c_str());
    //parentElement->SetAttribute("loops", "true");
}
