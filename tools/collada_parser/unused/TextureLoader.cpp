#include "TextureLoader.h"



//-----------------------------------
//      Texture Group
//-----------------------------------

TextureGroup::TextureGroup(gli::texture2D::format_type format, const glm::uvec2& resolution, uint numLevels) :   
    m_resolution(resolution), 
    m_numMips(numLevels),
    m_format(format)
{

};

bool TextureGroup::operator==(const TextureGroup& other) const
{
    return  m_format == other.m_format && 
            m_resolution == other.m_resolution && 
            m_numMips == other.m_numMips;
}

void TextureGroup::insertTexture(const std::string& textureName)
{ 
    m_textures.push_back(textureName);
}


void TextureLoader::exportTextures(const std::string& fileName)
{
    std::ifstream infile(fileName);
    std::string line;
    std::getline(infile, line);
    while (line != "")
    {
        std::string textureName = line;

        glm::uvec2 dimensions;
        gli::format format;
        uint mipMapCount;

        gli::loadDDS10Info(textureName, dimensions.x, dimensions.y, format, mipMapCount);
	    
	    TextureGroup textureGroupProxy(format, dimensions, mipMapCount);

        // Search for texture group that already has these properties
        auto foundTextureGroup = std::find(m_textureGroups.begin(), m_textureGroups.end(), textureGroupProxy);
        if(foundTextureGroup == m_textureGroups.end())
        {
            textureGroupProxy.insertTexture(textureName);
            m_textureGroups.push_back(textureGroupProxy);
        }
        else
        {
            foundTextureGroup->insertTexture(textureName);
        }


        std::getline(infile, line);
    }

    std::remove(fileName.c_str());

    std::stringstream textureOutputSS;

    for(uint i = 0; i < m_textureGroups.size(); i++)
    {
        TextureGroup textureGroup = m_textureGroups[i];

        std::string gliFormat;
        if(textureGroup.m_format == gli::R8U) gliFormat = "R8U";
        else if(textureGroup.m_format == gli::RG8U) gliFormat = "RG8U";
        else if(textureGroup.m_format == gli::RGB8U) gliFormat = "RGB8U";
        else if(textureGroup.m_format == gli::RGBA8U) gliFormat = "RGBA8U";

        textureOutputSS << gliFormat << " " << textureGroup.m_numMips << " " << textureGroup.m_resolution.x << " " << textureGroup.m_resolution.y << " " << textureGroup.m_textures.size() << "\n";
        
        for(uint j = 0; j < textureGroup.m_textures.size(); j++)
        {
            std::string textureName = textureGroup.m_textures[j];
            textureOutputSS << textureName << "\n";
        }

    }

    std::ofstream textureOutputFile;
    textureOutputFile.open(fileName);
    textureOutputFile << textureOutputSS.str().c_str();
    textureOutputFile.close();

}
        
