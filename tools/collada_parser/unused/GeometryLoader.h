#pragma once

#include <glm/glm.hpp>
#include <vector>

class domGeometry;
class domLibrary_geometries;
class VertexGroup;
class LodMesh;
class MaterialLoader;



class GeometryLoader
{

public:

    void loadGeometry(VertexGroup* vertexGroup, domGeometry* geom, bool checkForDuplicates);
};