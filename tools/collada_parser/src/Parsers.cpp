#include "Parsers.h"


std::string getNameFromFilename(const std::string& filename)
{
	// Get the name of the file
	int start = filename.find_last_of("/") + 1;
	int end = filename.find_last_of(".");
	return filename.substr(start, end - start);
}

std::string getOutputDirFromFilename(const std::string& filename)
{
	// Get the name of the file
	int start = filename.find_last_of("/") + 1;
	int outputStart = filename.find(std::string("///"));
	if (outputStart == std::string::npos) outputStart = 0;
	else outputStart += 3;
	return filename.substr(outputStart, start - outputStart);
}

std::string convertToFowardSlashes(std::string filename)
{
    // Replace all \'s with /'s
    unsigned int found = filename.find("\\");
    while (found!=std::string::npos){
        filename.replace( found, 1, "/" );
        found = filename.find("\\");
    };

    return filename;
}

void axisConverter(const glm::mat4& matrix, glm::fquat& returnedRotation, glm::vec3& returnedTranslate)
{
	//special out of order rotation transformation which converts from z-up to y-up
	glm::fquat tempQuat;
	tempQuat = glm::rotate(tempQuat, 180, glm::vec3(0,0,1));
	tempQuat = glm::rotate(tempQuat, 90, glm::vec3(1,0,0));
	glm::mat4 converter =  glm::mat4_cast(tempQuat);

	glm::mat4 translation(1.0);
	translation[3] = matrix[3];
	glm::mat4 normalRotation = matrix;
	normalRotation[3] = glm::vec4(0,0,0,1);

	glm::mat4 finalMatrix = converter*translation*normalRotation;
		
	returnedRotation = glm::quat_cast(finalMatrix);
	returnedTranslate = glm::vec3(finalMatrix[3]);
}

std::string convertMat4sToString(const std::vector<glm::mat4>& matrices)
{
	std::stringstream matrixString;
	for(size_t i = 0; i < matrices.size(); i++)
	{
		matrixString << matrices[i][0].x << " " << matrices[i][1].x << " " << matrices[i][2].x << " " << matrices[i][3].x << " " <<
						matrices[i][0].y << " " << matrices[i][1].y << " " << matrices[i][2].y << " " << matrices[i][3].y << " " <<
						matrices[i][0].z << " " << matrices[i][1].z << " " << matrices[i][2].z << " " << matrices[i][3].z << " " <<
						matrices[i][0].w << " " << matrices[i][1].w << " " << matrices[i][2].w << " " << matrices[i][3].w << " ";
	}

	std::string matrixFinalString = matrixString.str();
	matrixFinalString = matrixFinalString.substr(0, matrixFinalString.length()-1);
	return matrixString.str();
}

std::vector<int> parseStringIntoInts(const std::string& configData, size_t size)
{
	std::vector<int> data;
	data.reserve(size);
	char* cstr;
	char* p;
	cstr = new char [configData.size()+1];
	strcpy (cstr, configData.c_str());

	p = strtok (cstr," ");
	while (p!=0)
	{

		data.push_back((int)std::atoi(p));
		p=strtok(0," ");
	}
	delete[] cstr;
	delete[] p;

	return data;
}

std::string parseIntsToString(const std::vector<int>& ints)
{
    std::stringstream intsStringStream;

    for(unsigned int i = 0; i < ints.size(); i++)
    {
        intsStringStream << ints[i] << " ";
    }

    std::string intsString = intsStringStream.str();
    intsString = intsString.substr(0, intsString.length()-1);
    return intsString;

}


void parseKeyframeData(const std::string& configData, size_t size, float& duration)
{
	std::vector<float> floats;
	floats.reserve(size);
	char* cstr;
	char* p;
	cstr = new char [configData.size()+1];
	strcpy (cstr, configData.c_str());

	p = strtok (cstr," ");
	while (p!=0)
	{
		float time = (float)std::atof(p);
		if(time > duration)
		{
			duration = time;
		}
		p=strtok(0," ");
	}
	delete[] cstr;
	delete[] p;
}
std::vector<float> parsePositionData(const std::string& configData, unsigned int numVertices, glm::vec3& min, glm::vec3& max)
{
	float xMin = FLT_MAX;
	float yMin = FLT_MAX;
	float zMin = FLT_MAX;

	float xMax =  FLT_MIN;
	float yMax =  FLT_MIN;
	float zMax =  FLT_MIN;

	std::vector<float> floats;
	floats.reserve(numVertices*3);

	char* cstr;
	char* p;
	cstr = new char [configData.size()+1];
	strcpy (cstr, configData.c_str());

	p = strtok (cstr," ");
	while (p!=0)
	{
		//convert strings to floats
		float x = (float)std::atof(p);
		floats.push_back(x);
		p=strtok(0," ");
		float y = (float)std::atof(p);
		floats.push_back(y);
		p=strtok(0," ");
		float z = (float)std::atof(p);
		floats.push_back(z);
		p=strtok(0," ");

		//test for the min and max x,y,z dimensions
		if(x < xMin) xMin = x;
		else if(x > xMax) xMax = x;
		if(y < yMin) yMin = y;
		else if(y > yMax) yMax = y;
		if(z < zMin) zMin = z;
		else if(z > zMax) zMax = z;
	}

	delete[] cstr;

    min.x = xMin;
    min.y = yMin;
    min.z = zMin;

    max.x = xMax;
    max.y = yMax;
    max.z = zMax;

	return floats;
}
std::vector<float> parseDataIntoFloats(const std::string& configData, size_t size)
{
	std::vector<float> floats;
	floats.reserve(size);
	char* cstr;
	char* p;
	cstr = new char [configData.size()+1];
	strcpy (cstr, configData.c_str());

	p = strtok (cstr," ");
	while (p!=0)
	{
		floats.push_back((float)std::atof(p));
		p=strtok(0," ");
	}
	delete[] cstr;
	delete[] p;

	return floats;
}
std::vector<std::string> parseSpaceSeparatedString(const std::string& configData)
{
	std::vector<std::string> tokens;
	std::istringstream iss(configData);
	std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter<std::vector<std::string> >(tokens));
	return tokens;
}
std::vector<std::string> parseMatrixDataIntoStrings(const std::string& configData, int numMatrices)
{
	std::vector<std::string> matrixStrings;
	std::vector<std::string> floats = parseSpaceSeparatedString(configData);
	for(unsigned int i = 0; i < floats.size(); i+=16)
	{
		std::stringstream matrixStringstream;
		for(int j = 0; j < 16; j++)
		{
			matrixStringstream << floats[i+j] << " ";
		}
		std::string matrixString = matrixStringstream.str();
		matrixString = matrixString.substr(0, matrixString.length()-1);
		matrixStrings.push_back(matrixString);
	}
	return matrixStrings;
}
std::vector<glm::mat4> parseDataIntoMat4(const std::string& configData, int numMatrices)
{
	//its ROW ORDER. Meaning the first 4 floats are the first row, not the first column
	std::vector<glm::mat4> matrices;
	matrices.reserve(numMatrices);
	std::vector<float> floats = parseDataIntoFloats(configData, numMatrices*16);
	for(int i = 0; i < numMatrices; i++)
	{
		glm::mat4 matrix(0);
		matrix[0].x = floats[i*16 + 0];
		matrix[1].x = floats[i*16 + 1];
		matrix[2].x = floats[i*16 + 2];
		matrix[3].x = floats[i*16 + 3];

		matrix[0].y = floats[i*16 + 4];
		matrix[1].y = floats[i*16 + 5];
		matrix[2].y = floats[i*16 + 6];
		matrix[3].y = floats[i*16 + 7];

		matrix[0].z = floats[i*16 + 8];
		matrix[1].z = floats[i*16 + 9];
		matrix[2].z = floats[i*16 + 10];
		matrix[3].z = floats[i*16 + 11];

		matrix[0].w = floats[i*16 + 12];
		matrix[1].w = floats[i*16 + 13];
		matrix[2].w = floats[i*16 + 14];
		matrix[3].w = floats[i*16 + 15];

		matrices.push_back(matrix);

	}
   
	return matrices;
}
//set the translation and quaternions of the animation keyframes
void parseJointAnimMatrix(const std::string& configData, int numFrames, bool isRoot, std::string& translationsString, std::string& quatsString)
{
	//its ROW ORDER. Meaning the first 4 floats are the first row, not the first column
	
	std::stringstream translationStream;
	std::stringstream quatStream;

	std::vector<float> floats = parseDataIntoFloats(configData, numFrames*16);
	for(int i = 0; i < numFrames; i++)
	{
		glm::mat4 matrix;

		//rotation
		matrix[0].x = floats[i*16 + 0];
		matrix[1].x = floats[i*16 + 1];
		matrix[2].x = floats[i*16 + 2];
		
		matrix[0].y = floats[i*16 + 4];
		matrix[1].y = floats[i*16 + 5];
		matrix[2].y = floats[i*16 + 6];
		
		matrix[0].z = floats[i*16 + 8];
		matrix[1].z = floats[i*16 + 9];
		matrix[2].z = floats[i*16 + 10];

		//translation
		matrix[3].x = floats[i*16 + 3];
		matrix[3].y = floats[i*16 + 7];
		matrix[3].z = floats[i*16 + 11];


		if(isRoot)
		{
			glm::fquat rotation;
			glm::vec3 translation;
			axisConverter(matrix, rotation, translation);
			translationStream << translation.x << " " << translation.y << " " << translation.z << " ";
			quatStream << rotation.x << " " << rotation.y << " " << rotation.z << " " << rotation.w << " ";
		}
		else
		{
			glm::fquat rotation = glm::quat_cast(matrix);
			glm::vec3 translation = glm::vec3(matrix[3]);

			translationStream << translation.x << " " << translation.y << " " << translation.z << " ";
			quatStream << rotation.x << " " << rotation.y << " " << rotation.z << " " << rotation.w << " ";
		}

	}
   
	translationsString = translationStream.str();
	translationsString = translationsString.substr(0, translationsString.length() - 1);

	quatsString = quatStream.str();
	quatsString = quatsString.substr(0, quatsString.length() - 1);


}