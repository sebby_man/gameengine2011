#pragma once

#include <string>
#include <vector>
#include <glm\glm.hpp>
class domGeometry;


class Model;
class daeElement;
class TiXmlElement;
class MaterialLoader;
class VertexGroup;
class domInstance_controller;


class MeshLoader
{

public:

    MeshLoader();
    ~MeshLoader();

    void exportMesh(const std::vector<std::string>& lodFiles, const std::string& meshName, const std::string& outputDir, bool checkForDuplicates, bool isPhysicsMesh, bool exportBinary, const std::string& outputDirInFile, bool webgl, bool scene);

private:

    daeElement* openDae(const std::string& file);
    void loadGeometry(VertexGroup* vertexGroup, domGeometry* geom, const std::vector<domGeometry*>& morphTargets, bool checkForDuplicates, const glm::mat4& matrix, bool scene);
	void loadController(domInstance_controller* instanceController, VertexGroup* vertexGroup);
};