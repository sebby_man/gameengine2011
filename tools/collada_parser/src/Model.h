#pragma once

#include <set>
#include <string>
#include <vector>
#include <glm/glm.hpp>

class TiXmlElement;

const float epsilon = .001f;

struct VertexBoneData
{
    VertexBoneData() :
        numBones(0), 
        boneWeights(0.0f), 
        boneIDs(0) 
    { }

    int numBones;
    glm::vec4 boneWeights;
    glm::ivec4 boneIDs;
};

struct Vertex
{
    Vertex()
    { 
        data[0] = glm::vec3(0.0f);
        data[1] = glm::vec3(0.0f);
        data[2] = glm::vec3(0.0f);
    }
    
    // position, normal, uv
    glm::vec3 data[3];
    VertexBoneData boneData;
    
};

struct MaterialGroup
{
    MaterialGroup();

	void applyVertexOffset(unsigned int vertexOffset);
    char* getElementArrayBinary(unsigned int& byteSize, int elementSize, unsigned int vertexOffset);
    std::string getElementArrayString(unsigned int vertexOffset);

    std::vector<int> elementArray;
	std::vector<std::string> materialNames;
    unsigned int materialAttachment;
};


struct VertexGroup
{
    VertexGroup(const std::string& name, bool isAnimated, unsigned int numVertices, unsigned int numMorphTargets);
    void addMaterialGroup(MaterialGroup* materialGroup);
    unsigned int addVertex(const Vertex& vertex, bool checkForDuplicates);
    void addMorphVertex(const Vertex& vertex, unsigned int index, unsigned int morphIndex);

    char* getMorphTargetsBinary(unsigned int& byteSize);
    char* getVertexDataBinary(bool isPhysicsMesh, bool webgl, unsigned int& byteSize);
    std::string getVertexDataString(bool isPhysicsMesh);

	int vertexSize;
	bool isAnimated;

    unsigned int numVertices;
    Vertex* vertexData; // No duplicates

    std::vector<std::string> morphTargetNames;
    std::vector<Vertex*> morphTargetsData;

    unsigned int numVerticesAnimated;
    VertexBoneData* vertexBoneData;
	
    glm::vec3 min;
    glm::vec3 max;

    std::vector<MaterialGroup*> materialGroups;

	std::string boneNames;

    std::string name;
};

struct LodMesh
{
    LodMesh();
    void addVertexGroup(VertexGroup* vertexGroup);

	std::vector<VertexGroup*> vertexGroups;

    glm::vec3 min;
    glm::vec3 max;
};


struct Mesh
{
	Mesh(const std::string& name);
    void addLod(LodMesh* lodMesh);
	void exportMesh(bool isPhysicsMesh, bool exportBinary, const std::string& outputDir, const std::string& outputName, const std::string& outputDirInFile, bool webgl);
    void finalize(); // Finalize the material attachments of the material groups

    std::string name;
	std::vector<LodMesh*> lodMeshes;
    glm::vec3 dimensions;
};