#include "MeshLoader.h"

#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <tinyxml/tinyxml.h>
#include <dae.h>
#include <dom/domCOLLADA.h>
#include "refinery/axisconverter.h"
#include "refinery/polylists2triangles.h"
#include "refinery/deindexer.h"
#include "Model.h"
#include "Parsers.h"
#include "Model.h"



MeshLoader::MeshLoader()
{

}

MeshLoader::~MeshLoader()
{

}

daeElement* MeshLoader::openDae(const std::string& file)
{
	// Open the collada file
	DAE* dae = new DAE();
	daeElement* root = dae->open(file);
	if (!root) {
		std::cout << file << ": Document import failed." << std::endl;
		return 0;
	}

	// Apply axis converter, polylists2triangles
	Axisconverter axisConverter;
	axisConverter.execute(dae, file);

	Polylist2Triangles polylist2Triangles;
	polylist2Triangles.execute(dae, file);

	return root;
}

VertexGroup* getExistingVertexGroup(const std::string& geomName, LodMesh* lodMesh, bool scene)
{
	if (scene) return 0; // Always duplicate if the scene flag is set

	// See if this is a copy
	for (unsigned int i = 0; i < lodMesh->vertexGroups.size(); i++)
	{
		VertexGroup* currVertexGroup = lodMesh->vertexGroups[i];
		if (currVertexGroup->name == geomName)
		{
			return currVertexGroup;
		}
	}

	return 0;
}

unsigned int getMaxVertices(domGeometry* geom)
{
	unsigned int maxVertices = 0; // Estimate the size to make a statically sized array
	for (unsigned int tri = 0; tri < geom->getMesh()->getTriangles_array().getCount(); tri++)
	{
		maxVertices += geom->getMesh()->getTriangles_array()[tri]->getCount() * 3;
	}

	return maxVertices;
}

void addMaterials(VertexGroup* vertexGroup, const domBind_materialRef& material)
{
	// Add materials. Do this even if a matching vertex group was already found, because this helps when the export data goes back into Blender.
	domInstance_material_Array& materialArray = material->getTechnique_common()->getInstance_material_array();
	for (unsigned int i = 0; i < materialArray.getCount(); i++)
	{
		std::string materialName = materialArray[i]->getSymbol();
		vertexGroup->materialGroups[i]->materialNames.push_back(materialName);
	}
}

std::vector<domGeometry*> getMorphTargets(daeElement* root, const std::string& name)
{
    std::vector<domGeometry*> morphTargets;

    // Get any morph targets
    domMorph* morph = daeSafeCast<domMorph>(root->getDescendant("morph"));
    if (!morph) return morphTargets;

    // Check if the name matches
    domGeometry* source = daeSafeCast<domGeometry>(morph->getSource().getElement());
    std::string geomName = source->getId();
    if (geomName != name) morphTargets;

    // Get the keys, and the geometry node associated with each key
    domGeometry_Array& geometries = daeSafeCast<domLibrary_geometries>(root->getDescendant("library_geometries"))->getGeometry_array();
    
    std::vector<std::string> keynames = parseSpaceSeparatedString((daeSafeCast<domIDREF_array>(morph->getDescendant("IDREF_array")))->getCharData());
    for (int i = 0; i < keynames.size(); i++)
    {
        std::string key = keynames[i];
        for (int j = 0; j < geometries.getCount(); j++)
        {
            domGeometry* geometry = geometries[j];
            std::string name = geometry->getId();
            if (key == name)
            {
                morphTargets.push_back(geometry);
                break;
            }
        }
    }
    return morphTargets;
}

void MeshLoader::exportMesh(const std::vector<std::string>& lodFiles, const std::string& name, const std::string& outputDir, bool checkForDuplicates, bool isPhysicsMesh, bool exportBinary, const std::string& outputDirInFile, bool webgl, bool scene)
{
	Mesh mesh(name);
	// Load the LODs (each LOD is in its own dae file)
	for (unsigned int i = 0; i < lodFiles.size(); i++)
	{
		std::string file = lodFiles[i];
		daeElement* root = openDae(file);

		LodMesh* lodMesh = new LodMesh();

		// Loop over the nodes in the scene
		domVisual_scene* visualScene = daeSafeCast<domVisual_scene>(root->getDescendant("visual_scene"));
		domNode_Array& nodes = visualScene->getNode_array();

		for (size_t i = 0; i < nodes.getCount(); i++)
		{
			// Check if it's a geometry node or a controller node
			if (nodes[i]->getInstance_geometry_array().getCount() == 1)
			{
				// Load geometry node
				domInstance_geometry* instanceGeom = nodes[i]->getInstance_geometry_array()[0];
				domGeometry* geom = daeSafeCast<domGeometry>(instanceGeom->getUrl().getElement());

				std::string geomName = geom->getId();
				VertexGroup* matchVertexGroup = getExistingVertexGroup(geomName, lodMesh, scene);

				// Create new vertex group
				if (matchVertexGroup == 0)
				{
					glm::mat4 matrix;
					if (scene)
					{
						std::string translationData = nodes[i]->getTranslate_array()[0]->getCharData();
						std::vector<float> translationFloats = parseDataIntoFloats(translationData, 3);
						glm::vec3 translation(translationFloats[0], translationFloats[1], translationFloats[2]);
						glm::mat4 translationMat = glm::translate(glm::mat4(), translation);

						std::string scaleData = nodes[i]->getScale_array()[0]->getCharData();
						std::vector<float> scaleFloats = parseDataIntoFloats(scaleData, 3);
						glm::vec3 scale(scaleFloats[0], scaleFloats[1], scaleFloats[2]);
						glm::mat4 scaleMat = glm::scale(glm::mat4(), scale);

						glm::quat rotateQuat;
						for (int j = 0; j < nodes[i]->getRotate_array().getCount(); j++)
						{
							std::string rotateData = nodes[i]->getRotate_array()[j]->getCharData();
							std::vector<float> floats = parseDataIntoFloats(rotateData, 4);
							glm::vec3 axis = glm::vec3(floats[0], floats[1], floats[2]);
							float angle = floats[3];
							glm::quat axisRot = glm::angleAxis(angle, axis);
							rotateQuat = rotateQuat * axisRot;
						}
						glm::mat4 rotateMat = glm::toMat4(rotateQuat);
						matrix = translationMat * rotateMat * scaleMat;
					}

                    std::vector<domGeometry*> morphTargets = getMorphTargets(root, geomName);
                    VertexGroup* vertexGroup = new VertexGroup(geomName, false, getMaxVertices(geom), morphTargets.size());

					loadGeometry(vertexGroup, geom, morphTargets, checkForDuplicates, matrix, scene);
					lodMesh->addVertexGroup(vertexGroup);
					matchVertexGroup = vertexGroup;
				}

				addMaterials(matchVertexGroup, instanceGeom->getBind_material());
			}
			else if (nodes[i]->getInstance_controller_array().getCount() == 1)
			{
				// Load the animated mesh
				domInstance_controller* instanceController = nodes[i]->getInstance_controller_array()[0];
				domController* controller = daeSafeCast<domController>(instanceController->getUrl().getElement());
				domGeometry* skin = daeSafeCast<domGeometry>(controller->getSkin()->getSource().getElement());

				std::string geomName = skin->getId();
				VertexGroup* matchVertexGroup = getExistingVertexGroup(geomName, lodMesh, scene);

				// Create new vertex group and load animation stuff
				if (matchVertexGroup == 0)
				{
                    std::vector<domGeometry*> morphTargets = getMorphTargets(root, geomName);

					VertexGroup* animatedVertexGroup = new VertexGroup(geomName, true, getMaxVertices(skin), morphTargets.size());
					loadController(instanceController, animatedVertexGroup);

					// Load the skin
					loadGeometry(animatedVertexGroup, skin, morphTargets, checkForDuplicates, glm::mat4(), scene);

					lodMesh->addVertexGroup(animatedVertexGroup);
					matchVertexGroup = animatedVertexGroup;
				}

				addMaterials(matchVertexGroup, instanceController->getBind_material());
			}
		}

		mesh.addLod(lodMesh);
	}


	mesh.exportMesh(isPhysicsMesh, exportBinary, outputDir, name, outputDirInFile, webgl);
}


void getSources(domGeometry* geometry, std::vector<domSource*>& sources, std::vector<int>& componentCount)
{
    for (size_t i = 0; i < geometry->getMesh()->getTriangles_array()[0]->getInput_array().getCount(); i++) {

        domInputLocalOffset* input = geometry->getMesh()->getTriangles_array()[0]->getInput_array()[i];
        std::string semantic = input->getSemantic();

        if (semantic == "VERTEX") // Get position data, including the positionString, numVertices, and containsPositions
        {
            domVertices* vertexSource = daeSafeCast<domVertices>(input->getSource().getElement());
            domSource* source = daeSafeCast<domSource>(vertexSource->getInput_array()[0]->getSource().getElement());
            sources.push_back(source);
            componentCount.push_back(3);
        }
        else if (semantic == "NORMAL") // Get normal data, including the normals string and containsNormals
        {
            domSource* source = daeSafeCast<domSource>(input->getSource().getElement());
            sources.push_back(source);
            componentCount.push_back(3);
        }
        else if (semantic == "TEXCOORD") // Get UV data.
        {
            domSource* source = daeSafeCast<domSource>(input->getSource().getElement());
            sources.push_back(source);
            componentCount.push_back(2);
        }
    }
}

void MeshLoader::loadGeometry(VertexGroup* vertexGroup, domGeometry* baseGeometry, const std::vector<domGeometry*>& morphTargets, bool checkForDuplicates, const glm::mat4& matrix, bool scene)
{
    unsigned int numMorphTargets = morphTargets.size();
    vertexGroup->name = baseGeometry->getId();


	std::vector<domSource*> baseSources;
	std::vector<int> componentCount;
    std::vector<std::vector<domSource*>> morphSources(numMorphTargets);

    getSources(baseGeometry, baseSources, componentCount);
    
    
    for (unsigned int i = 0; i < numMorphTargets; i++)
    {
        getSources(morphTargets[i], morphSources[i], std::vector<int>()); // Give it a bogus componentCount vector
        
        std::string morphName = morphTargets[i]->getName();
        vertexGroup->morphTargetNames.push_back(morphName);
    }

	unsigned int numAttributes = baseSources.size();

    // Re-used variables
    Vertex vertex;
    std::vector<Vertex> morphVertices(numMorphTargets);

	// Loop over all the triangle commands
    for (unsigned int tri = 0; tri < baseGeometry->getMesh()->getTriangles_array().getCount(); tri++)
	{
        domTriangles* triangleDraw = baseGeometry->getMesh()->getTriangles_array()[tri];

		// Add a new material group
		MaterialGroup* materialGroup = new MaterialGroup();
		vertexGroup->addMaterialGroup(materialGroup);

		unsigned int numElements = triangleDraw->getCount() * 3;
		materialGroup->elementArray.resize(numElements);

		for (unsigned int elem = 0; elem < numElements; elem++)
		{
            // Construct the vertex (and morph vertices)
			for (unsigned int attr = 0; attr < numAttributes; attr++)
			{
				int dataIndex = triangleDraw->getP()->getValue()[elem*numAttributes + attr];
				unsigned int numComponents = componentCount[attr];
				for (unsigned int comp = 0; comp < numComponents; comp++)
				{
					vertex.data[attr][comp] = baseSources[attr]->getFloat_array()->getValue()[dataIndex*numComponents + comp];
                    for (unsigned int m = 0; m < numMorphTargets; m++)
                    {
                        morphVertices[m].data[attr][comp] = morphSources[m][attr]->getFloat_array()->getValue()[dataIndex*numComponents + comp];
                    }
                }
			}

			if (vertexGroup->isAnimated)
			{
				// Use the same index that points to the position to retrieve the bone data
				int boneDataIndex = triangleDraw->getP()->getValue()[elem*numAttributes + 0];
				VertexBoneData boneData = vertexGroup->vertexBoneData[boneDataIndex];
				vertex.boneData = boneData;
			}

			if (scene)
			{
				vertex.data[0] = glm::vec3(matrix * glm::vec4(vertex.data[0], 1.0));
				vertex.data[1] = glm::mat3(matrix) * vertex.data[1];
			}

			// Flip UVs (doesn't matter for morphs because they don't use UVs)
			//vertex.data[2].y = 1.0f - vertex.data[2].y;


			unsigned int index = vertexGroup->addVertex(vertex, checkForDuplicates);

            for (unsigned int m = 0; m < numMorphTargets; m++)
            {
                vertexGroup->addMorphVertex(morphVertices[m], index, m);
            }

			materialGroup->elementArray[elem] = index;
		}
	}
}

void MeshLoader::loadController(domInstance_controller* instanceController, VertexGroup* vertexGroup)
{
	domController* controller = daeSafeCast<domController>(instanceController->getUrl().getElement());

	// Bone names
	std::string boneNames = daeSafeCast<domSource>(controller->getSkin()->getVertex_weights()->getInput_array()[0]->getSource().getElement())->getName_array()->getCharData();
	vertexGroup->boneNames = boneNames;

	// Get num joints data
	int numVertices = controller->getSkin()->getVertex_weights()->getCount();
	std::string numJointsString = controller->getSkin()->getVertex_weights()->getVcount()->getCharData();
	std::vector<int> numJointsData = parseStringIntoInts(numJointsString, numVertices);

	// Get weights data
	domSource* weightsSource = daeSafeCast<domSource>(controller->getSkin()->getVertex_weights()->getInput_array()[1]->getSource().getElement());
	unsigned int numWeights = weightsSource->getFloat_array()->getCount();
	std::string weightsDataString = weightsSource->getFloat_array()->getCharData();
	std::vector<float> weightsData = parseDataIntoFloats(weightsDataString, numWeights);

	// Get joints and weights index data. Series of <joint ID, weight ID> pairs.
	std::string jointsAndWeightDataString = controller->getSkin()->getVertex_weights()->getV()->getCharData();
	std::vector<int> jointsAndWeightsData = parseStringIntoInts(jointsAndWeightDataString, numWeights * 2);

	// Set vertex data with joint data
	unsigned int jointDataIndex = 0;
	for (unsigned int i = 0; i < numJointsData.size(); i++)
	{
		int numJoints = numJointsData[i];

		VertexBoneData boneData;
		boneData.numBones = glm::max(numJoints, 4);
		for (int j = 0; j < numJoints; j++)
		{
			if (j < 4) // limit to max of 4 joints
			{
				int boneID = jointsAndWeightsData[jointDataIndex];
				float boneWeight = weightsData[jointsAndWeightsData[jointDataIndex + 1]];

				boneData.boneIDs[j] = boneID;
				boneData.boneWeights[j] = boneWeight;
			}

			jointDataIndex += 2;
		}

		vertexGroup->vertexBoneData[vertexGroup->numVerticesAnimated] = boneData;
		vertexGroup->numVerticesAnimated++;
	}
}
