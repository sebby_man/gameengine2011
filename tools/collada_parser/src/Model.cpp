#include "Model.h"

#include <map>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <tinyxml/tinyxml.h>
#include <glm/gtx/epsilon.hpp>
#include "Parsers.h"
#include <fstream>
#include <iostream>



MaterialGroup::MaterialGroup()
{ 

}

void MaterialGroup::applyVertexOffset(unsigned int vertexOffset)
{
	for (unsigned int i = 0; i < elementArray.size(); i++)
	{
		elementArray[i] += vertexOffset;
	}
}

char* MaterialGroup::getElementArrayBinary(unsigned int& byteSize, int elementSize, unsigned int vertexOffset)
{
	
	applyVertexOffset(vertexOffset);

    if(elementSize == 2)
    {
        std::vector<unsigned short>* ushorts = new std::vector<unsigned short>(elementArray.begin(), elementArray.end());
        byteSize = elementSize * elementArray.size();
        return (char*)(&ushorts->at(0));
    }
    else if(elementSize == 4)
    {
        std::vector<unsigned int>* uints = new std::vector<unsigned int>(elementArray.begin(), elementArray.end());
        byteSize = elementSize * elementArray.size();
        return (char*)(&uints->at(0));
    }
    
    return 0;
}

std::string MaterialGroup::getElementArrayString(unsigned int vertexOffset)
{
	applyVertexOffset(vertexOffset);
    return parseIntsToString(elementArray);
}

VertexGroup::VertexGroup(const std::string& name, bool isAnimated, unsigned int maxVertices, unsigned int numMorphTargets) : 
    name(name),
    vertexSize(isAnimated ? 64: 32),
	isAnimated(isAnimated),
	min(FLT_MAX),
    max(FLT_MIN)
{
    vertexData = new Vertex[maxVertices];

    for (unsigned int i = 0; i < numMorphTargets; i++)
    {
        Vertex* morphTargetData = new Vertex[maxVertices];
        morphTargetsData.push_back(morphTargetData);
    }

    if(isAnimated)
    vertexBoneData = new VertexBoneData[maxVertices];

    // Set back to zero because the actual number of vertices may be less due to duplicates
    numVertices = 0;
    numVerticesAnimated = 0;
}

void VertexGroup::addMaterialGroup(MaterialGroup* materialGroup)
{
    materialGroups.push_back(materialGroup);
}

// Used during export only
struct VertexGame
{
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 UV;
};

struct AnimatedVertexGame
{
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 UV;
    glm::vec4 boneWeights;
    glm::uvec4 boneIDs;
};

struct AnimatedVertexGameWebGL
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 UV;
	glm::vec4 boneWeights;
	glm::vec4 boneIDs;
};

struct MorphVertex
{
    glm::vec3 positionDiff;
    glm::vec3 normalDiff;
};

char* VertexGroup::getMorphTargetsBinary(unsigned int& byteSize)
{
    // Morph targets data
    unsigned int numMorphTargets = morphTargetsData.size();
    if (numMorphTargets == 0) return 0;
    MorphVertex* morphVertices = new MorphVertex[numMorphTargets*numVertices];
    for (unsigned int m = 0; m < numMorphTargets; m++)
    {
        Vertex* morphData = morphTargetsData[m];
        for (unsigned int i = 0; i < numVertices; i++)
        {
            int index = m*numVertices + i;

            morphVertices[index].positionDiff = morphData[i].data[0] - vertexData[i].data[0];
            morphVertices[index].normalDiff = morphData[i].data[1] - vertexData[i].data[1];
        }
    }

    byteSize = sizeof(MorphVertex)*numMorphTargets*numVertices;
    return (char*)(&morphVertices[0].positionDiff.x);
}

char* VertexGroup::getVertexDataBinary(bool isPhysicsMesh, bool webgl, unsigned int& byteSize)
{
    if(isPhysicsMesh)
    {
        glm::vec3* vertices = new glm::vec3[numVertices];
        for(unsigned int i = 0; i < numVertices; i++)
	    {
            vertices[i] = vertexData[i].data[0];
        }
        byteSize = sizeof(glm::vec3)*numVertices;
        return (char*)(&vertices[0].x);
    }

    if(isAnimated)
    {
		if (webgl)
		{
			AnimatedVertexGameWebGL* animatedVertices = new AnimatedVertexGameWebGL[numVertices];
			for (unsigned int i = 0; i < numVertices; i++)
			{
				Vertex& vertex = vertexData[i];
				AnimatedVertexGameWebGL animatedVertex;
				animatedVertex.position = vertex.data[0];
				animatedVertex.normal = vertex.data[1];
				animatedVertex.UV = glm::vec2(vertex.data[2]);
				animatedVertex.boneWeights = vertex.boneData.boneWeights;
				animatedVertex.boneIDs = glm::vec4(vertex.boneData.boneIDs) + glm::vec4(0.1); // Webgl can't use int attributes, so need to convert to float. Add a little extra so cast from float to int in glsl shader works.
				animatedVertices[i] = animatedVertex;
			}
			byteSize = sizeof(AnimatedVertexGameWebGL)*numVertices;
			return (char*)(&animatedVertices[0].position.x);
		}
		else
		{
			AnimatedVertexGame* animatedVertices = new AnimatedVertexGame[numVertices];
			for (unsigned int i = 0; i < numVertices; i++)
			{
				Vertex& vertex = vertexData[i];
				AnimatedVertexGame animatedVertex;
				animatedVertex.position = vertex.data[0];
				animatedVertex.normal = vertex.data[1];
				animatedVertex.UV = glm::vec2(vertex.data[2]);
				animatedVertex.boneWeights = vertex.boneData.boneWeights;
				animatedVertex.boneIDs = vertex.boneData.boneIDs;
				animatedVertices[i] = animatedVertex;
			}
			byteSize = sizeof(AnimatedVertexGame)*numVertices;
			return (char*)(&animatedVertices[0].position.x);
		}
        
    }
    else if(!isAnimated)
    {
        // Memory leak
        VertexGame* staticVertices = new VertexGame[numVertices];
        for(unsigned int i = 0; i < numVertices; i++)
        {
            Vertex& vertex = vertexData[i];

            VertexGame staticVertex;
            staticVertex.position = vertex.data[0];
            staticVertex.normal = vertex.data[1];
            staticVertex.UV = glm::vec2(vertex.data[2]);

            staticVertices[i] = staticVertex;
        }
        byteSize = sizeof(VertexGame)*numVertices;
        return (char*)(&staticVertices[0].position.x);
    }

    return 0;
}

std::string VertexGroup::getVertexDataString(bool isPhysicsMesh)
{
    // Create the vertex data string
	std::stringstream vertexDataStream;
    for(unsigned int i = 0; i < numVertices; i++)
	{
        Vertex vertex = vertexData[i];

        vertexDataStream << vertex.data[0].x << " " << vertex.data[0].y << " " << vertex.data[0].z << " ";
		
        if(!isPhysicsMesh)
        {
            vertexDataStream << vertex.data[1].x << " " << vertex.data[1].y << " " << vertex.data[1].z << " ";
		    vertexDataStream << vertex.data[2].x << " " << vertex.data[2].y << " ";
        }

		if(isAnimated)
		{
		    //vertexDataStream << animVertex.numBones << " ";
            for(int i = 0; i < 4; i++)
			{
				vertexDataStream << vertex.boneData.boneWeights[i] << " ";
			}
            for(int i = 0; i < 4; i++)
			{
				vertexDataStream << vertex.boneData.boneIDs[i] << " ";
			}
		}
    }

	std::string vertexDataString = vertexDataStream.str();
	vertexDataString = vertexDataString.substr(0, vertexDataString.length()-1); // Get rid of the trailing " "
    return vertexDataString;
}

unsigned int VertexGroup::addVertex(const Vertex& vertex, bool checkForDuplicates)
{
    if(checkForDuplicates)
    {
        for(unsigned int i = 0; i < numVertices; i++)
        {
            Vertex other = vertexData[i];
            bool positionsEqual = glm::all(glm::equalEpsilon(vertex.data[0], other.data[0], epsilon));
            bool normalsEqual = glm::all(glm::equalEpsilon(vertex.data[1], other.data[1], epsilon));
            bool uvsEqual = glm::all(glm::equalEpsilon(vertex.data[2], other.data[2], epsilon));
            if(positionsEqual && normalsEqual && uvsEqual)
            {
                return i;
            }
        }
    }

    // If the vertex wasn't found
    vertexData[numVertices] = vertex;
    numVertices++;
	min = glm::min(min, vertex.data[0]);
    max = glm::max(max, vertex.data[0]);
    return numVertices-1;
}

void VertexGroup::addMorphVertex(const Vertex& vertex, unsigned int index, unsigned int morphIndex)
{
    morphTargetsData[morphIndex][index] = vertex;
}


LodMesh::LodMesh() :
    min(FLT_MAX),
    max(FLT_MIN)
{

}
 
void LodMesh::addVertexGroup(VertexGroup* vertexGroup)
{
    vertexGroups.push_back(vertexGroup);

    min = glm::min(min, vertexGroup->min);
    max = glm::max(max, vertexGroup->max);

    // If animation, switch height and depth because did not apply axis conversion
    if(vertexGroup->isAnimated)
    {
        float temp = max.y;
        max.y = max.z;
        max.z = temp;

        temp = min.y;
        min.y = min.z;
        min.z = temp;
    }
}


Mesh::Mesh(const std::string& name) : 
    name(name)
{

}

void Mesh::addLod(LodMesh* lodMesh)
{
    lodMeshes.push_back(lodMesh);

    dimensions = lodMesh->max - lodMesh->min;
}


struct VertexGroupSorter {
    bool operator ()(VertexGroup* const& a, VertexGroup* const& b) const 
    {
        return (a->numVertices > b->numVertices);
    }
};

struct MaterialGroupSorter {
    bool operator ()(MaterialGroup* const& a, MaterialGroup* const& b) const 
    {
        return (a->elementArray.size() > b->elementArray.size());
    }
};

void Mesh::finalize()
{
    std::vector<std::vector<std::string>> materialAttachments;
    for(unsigned int i = 0; i < lodMeshes.size(); i++)
    {
        LodMesh* lodMesh = lodMeshes[i];
        // Sort vertex groups by number of vertices, sort material groups by number of elements
        // Sort the vertex groups
        std::sort(lodMesh->vertexGroups.begin(), lodMesh->vertexGroups.end(), VertexGroupSorter());

        for(unsigned int i = 0; i < lodMesh->vertexGroups.size(); i++)
        {
            VertexGroup* vertexGroup = lodMesh->vertexGroups[i];

            // Sort the material groups inside the vertex group
            //std::sort(vertexGroup->materialGroups.begin(), vertexGroup->materialGroups.end(), MaterialGroupSorter());

            // Assign material attachments to the material groups and append materials to the material names vector
            for(unsigned int i = 0; i < vertexGroup->materialGroups.size(); i++)
            {
                MaterialGroup* materialGroup = vertexGroup->materialGroups[i];
                std::vector<std::string> materialNames = materialGroup->materialNames;

                bool found = false;
                for(unsigned int i = 0; i < materialNames.size(); i++)
                for(unsigned int j = 0; j < materialAttachments.size(); j++)
                for(unsigned int k = 0; k < materialAttachments[j].size(); k++)
                {
                    if(materialNames[i] == materialAttachments[j][k] && !found)
                    {
                        materialGroup->materialAttachment = j;
                        found = true;
                    }
                }

                // Not found
                if(!found)
                {
                    materialGroup->materialAttachment = materialAttachments.size();
                    materialAttachments.push_back(materialNames);
                }
            }
        }
    }
}


void Mesh::exportMesh(bool isPhysicsMesh, bool exportBinary, const std::string& outputDir, const std::string& outputName, const std::string& outputDirInFile, bool webgl)
{
	finalize(); // Finalize the material attachments of the material groups

    std::stringstream widthStream; widthStream << dimensions.x;
    std::stringstream heightStream; heightStream << dimensions.y;
    std::stringstream depthStream; depthStream << dimensions.z;

    TiXmlElement* meshXML = new TiXmlElement("mesh");

    // Mesh buffer info
    TiXmlElement* meshBufferXML = new TiXmlElement("mesh_buffer");
    meshXML->LinkEndChild(meshBufferXML);

    // Dimensions
    TiXmlElement* dimensionsXML = new TiXmlElement("dimensions");
	meshXML->LinkEndChild(dimensionsXML);
    dimensionsXML->SetAttribute("width", widthStream.str().c_str());
    dimensionsXML->SetAttribute("height", heightStream.str().c_str());
    dimensionsXML->SetAttribute("depth", depthStream.str().c_str());

	// Animated
	if (lodMeshes[0]->vertexGroups[0]->isAnimated)
	{
		TiXmlElement* animatedXML = new TiXmlElement("skinned");
		meshXML->LinkEndChild(animatedXML);
		TiXmlText* animatedTextXML = new TiXmlText("true");
		animatedXML->LinkEndChild(animatedTextXML);
	}

    if (isPhysicsMesh)
    {
        TiXmlElement* physicsXML = new TiXmlElement("physics");
        meshXML->LinkEndChild(physicsXML);
        TiXmlText* physicsTextXML = new TiXmlText("true");
        physicsXML->LinkEndChild(physicsTextXML);
    }

    // Binary file
    std::string binaryOutputName = outputDir + outputName + ".data";
    std::ofstream outputBinaryFStream;
    if(exportBinary)
    {
        TiXmlElement* binaryFileXML = new TiXmlElement("binary_data");
        meshXML->LinkEndChild(binaryFileXML);

		std::string binaryOutputDirInFile = outputDirInFile + outputName + ".data";
		TiXmlText* binaryFileTextXML = new TiXmlText(binaryOutputDirInFile.c_str());
        binaryFileXML->LinkEndChild(binaryFileTextXML);
        std::remove(binaryOutputName.c_str()); // Remove in case it already exists
        outputBinaryFStream.open(binaryOutputName.c_str(), std::ofstream::out | std::ofstream::binary | std::ofstream::app);
    }
           
	std::map<std::string, unsigned int> materials; // Name -> attachment index
	unsigned int totalVertices = 0;
	unsigned int totalElements = 0;
	unsigned int elementSize = 0;
	unsigned int vertexSize = 0;

	for (unsigned int i = 0; i < lodMeshes.size(); i++)
	{
		LodMesh* lodMesh = lodMeshes[i];

		TiXmlElement* lodXML = new TiXmlElement("lod");
		meshXML->LinkEndChild(lodXML);
		lodXML->SetAttribute("level", i);
        
		unsigned int lodVertices = 0;
		std::string vertexDataText = "";
		std::vector<std::vector<std::pair<MaterialGroup*, unsigned int>>> materialGroups; // Sort all material groups in this lod by attachment

		// Loop over the vertex groups, export all vertex data and ordering the material groups
		for (unsigned int j = 0; j < lodMesh->vertexGroups.size(); j++)
		{
			VertexGroup* vertexGroup = lodMesh->vertexGroups[j];
			int numVertices = vertexGroup->numVertices;

			// Export vertex data
			if (exportBinary)
			{
				unsigned int byteSize;
				char* vertexDataBinary = vertexGroup->getVertexDataBinary(isPhysicsMesh, webgl, byteSize);
                outputBinaryFStream.write(vertexDataBinary, byteSize);
                char* morphTargetsBinary = vertexGroup->getMorphTargetsBinary(byteSize);
                if (morphTargetsBinary)
                    outputBinaryFStream.write(morphTargetsBinary, byteSize);
			}
			else
			{
				vertexDataText += vertexDataText == "" ? "" : " "; // Spacing dependent on if vertex data is appended
				vertexDataText += vertexGroup->getVertexDataString(isPhysicsMesh);
			}

			// Order material groups
			materialGroups.resize(vertexGroup->materialGroups.size()+materialGroups.size()); // Conservative size
			for (unsigned int k = 0; k < vertexGroup->materialGroups.size(); k++)
			{
				MaterialGroup* materialGroup = vertexGroup->materialGroups[k];
				unsigned int materialAttachment = materialGroup->materialAttachment;
				totalElements += materialGroup->elementArray.size();
				materialGroups[materialAttachment].push_back(std::pair<MaterialGroup*, unsigned int>(materialGroup, lodVertices));

				// Add materials to materials map
				std::vector<std::string> materialNames = materialGroup->materialNames;
				for (unsigned int i = 0; i < materialNames.size(); i++)
				{
					std::string materialName = materialNames[i].substr(0, materialNames[i].find("-material"));
					materials[materialName] = materialAttachment;
				}
			}

			lodVertices += numVertices;
		}

		TiXmlElement* vertexArrayXML = new TiXmlElement("vertex_array");
		lodXML->LinkEndChild(vertexArrayXML);
		vertexArrayXML->SetAttribute("count", lodVertices);

        // Morph targets
        std::vector<std::string>& morphTargetNames = lodMesh->vertexGroups[0]->morphTargetNames;
        for (unsigned int m = 0; m < morphTargetNames.size(); m++)
        {
            TiXmlElement* morphXML = new TiXmlElement("blend_shape");
            lodXML->LinkEndChild(morphXML);
            morphXML->SetAttribute("name", morphTargetNames[m].c_str());
        }

		if (!exportBinary)
		{
			TiXmlText* vertexArrayTextXML = new TiXmlText(vertexDataText.c_str());
			vertexArrayXML->LinkEndChild(vertexArrayTextXML);
		}

		totalVertices += lodVertices;
		elementSize = lodVertices < 65536 ? 2 : 4;
		vertexSize = lodMesh->vertexGroups[0]->isAnimated ? 64 : 32;

		// Loop over the material groups, export all index data
		for (unsigned int j = 0; j < materialGroups.size(); j++)
		{
			if (materialGroups[j].size() == 0) continue;

			std::string elementDataText = "";
			unsigned int numElementsForAttachment = 0;
			for (unsigned int k = 0; k < materialGroups[j].size(); k++)
			{
				MaterialGroup* materialGroup = materialGroups[j][k].first;
				unsigned int vertexOffset = materialGroups[j][k].second;
				int numElements = materialGroup->elementArray.size();
				numElementsForAttachment += numElements;

				if (exportBinary)
				{
					unsigned int byteSize;
					char* elementDataBinary = materialGroup->getElementArrayBinary(byteSize, elementSize, vertexOffset);
					outputBinaryFStream.write(elementDataBinary, byteSize);
				}
				else
				{
					elementDataText += elementDataText == "" ? "" : " "; // Spacing dependent on if vertex data is appended
					elementDataText += materialGroup->getElementArrayString(vertexOffset);
				}
			}

			TiXmlElement* elementArrayXML = new TiXmlElement("element_array");
			elementArrayXML->SetAttribute("count", numElementsForAttachment);
			elementArrayXML->SetAttribute("materialAttachment", j);
			lodXML->LinkEndChild(elementArrayXML);

			if (!exportBinary)
			{
				TiXmlText* elementArrayTextXML = new TiXmlText(elementDataText.c_str());
				elementArrayXML->LinkEndChild(elementArrayTextXML);
			}
		}
	}

	if (exportBinary)
	{
		outputBinaryFStream.close();
	}

	meshBufferXML->SetAttribute("vertex_size", vertexSize);
	meshBufferXML->SetAttribute("element_size", elementSize);
	meshBufferXML->SetAttribute("num_vertices", totalVertices);
	meshBufferXML->SetAttribute("num_elements", totalElements);

	// Create mesh info file
	std::stringstream meshInfoSS;

	meshInfoSS << vertexSize << " " << elementSize << " " << totalVertices << " " << totalElements << "\n";

	for (std::map<std::string, unsigned int>::iterator it = materials.begin(); it != materials.end(); it++)
	{
		meshInfoSS << it->first << " " << it->second << "\n";
	}

	// Export .mesh file
	TiXmlDocument meshDoc;
	std::string meshOutputName = outputDir + outputName + ".mesh";
	meshDoc.LinkEndChild(meshXML);
	meshDoc.SaveFile(meshOutputName.c_str());

	// Export .meshinfo file (used in Blender to finalize things)
	std::ofstream meshInfoFile;
	std::string meshInfoFilename = outputDir + outputName + ".meshinfo";
	meshInfoFile.open(meshInfoFilename);
	meshInfoFile << meshInfoSS.str();
	meshInfoFile.close();

	//// Export .boneinfo file (used in Blender)
	//std::string boneNames = lodMeshes[0]->vertexGroups[0]->boneNames;
	//if (boneNames != "")
	//{
	//	std::ofstream boneInfoFile;
	//	std::string boneInfoFilename = outputDir + outputName + ".boneinfo";
	//	boneInfoFile.open(boneInfoFilename);
	//	boneInfoFile << boneNames << "\n";
	//	boneInfoFile.close();
	//}
}