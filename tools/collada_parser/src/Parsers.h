#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <vector>
#include <sstream>
#include <istream>
#include <iterator>


std::string getNameFromFilename(const std::string& filename);

std::string getOutputDirFromFilename(const std::string& filename);

std::string convertToFowardSlashes(std::string filename);

void axisConverter(const glm::mat4& matrix, glm::fquat& returnedRotation, glm::vec3& returnedTranslation);

std::string convertMat4sToString(const std::vector<glm::mat4>& matrices);

std::vector<int> parseStringIntoInts(const std::string& rawData, unsigned int size);
std::string parseIntsToString(const std::vector<int>& ints);

void parseKeyframeData(const std::string& rawData, unsigned int size, float& returnedDuration);

std::vector<float> parsePositionData(const std::string& rawData, unsigned int numVertices, glm::vec3& min, glm::vec3& max);

std::vector<float> parseDataIntoFloats(const std::string& rawData, unsigned int numFloats);

std::vector<std::string> parseSpaceSeparatedString(const std::string& rawData);

std::vector<std::string> parseMatrixDataIntoStrings(const std::string& rawData, int numMatrices);

std::vector<glm::mat4> parseDataIntoMat4(const std::string& rawData, int numMatrices);

//set the translation and quaternions of the animation keyframes
void parseJointAnimMatrix(const std::string& rawData, int numFrames, bool isRoot, std::string& translationsString, std::string& quatsString);