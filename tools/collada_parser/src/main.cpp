
#include <algorithm>
#include "MeshLoader.h"
#include "Parsers.h"

int main(int argc, char *argv[])
{
	bool checkForDuplicates = true;
	bool exportBinary = false;
    bool isPhysics = false;
	bool webgl = false;
	bool scene = false;
	std::string outputDirInFile = ""; // Needs to know the game-centric output location for the <binary_data> tag. E.g. "Assets/entities/car/"

    std::vector<std::string> inputs;
	std::vector<std::string> lodFiles;
	std::vector<std::string> otherFiles; // Should be empty right now

    // Get the export type and the inputs
    for(int i = 1; i < argc; i++)
    {
        std::string param = std::string(argv[i]);
        isPhysics |= param == "-physics";
		exportBinary |= param == "-binary";
		webgl |= param == "-webgl";
		scene |= param == "-scene";
		

		std::string outputDirName = "-output_dir=";
		unsigned int outputDirStart = param.find(outputDirName);
		if (outputDirStart != std::string::npos)
		{
			outputDirInFile = param.substr(outputDirName.length());
			outputDirInFile = convertToFowardSlashes(outputDirInFile);
		}

        // If it's a filename and not a parameter ...
        if(param.find('-') != 0)
        {
            inputs.push_back(param);
        }
    }

	// Organize the inputs
    for(unsigned int i = 0; i < inputs.size(); i++)
    {
        inputs[i] = convertToFowardSlashes(inputs[i]);

        // And "file:///" in front of every filename so that Collada can load it correctly
        if(inputs[i].find(".dae") != std::string::npos)
        {
            if(inputs[i].find("/") != std::string::npos)
            {
                inputs[i] = std::string("file:///") + inputs[i];
            }

			lodFiles.push_back(inputs[i]);
        }
		else
		{
			otherFiles.push_back(inputs[i]);
		}
    }


	std::string meshName = getNameFromFilename(lodFiles[0]);
	std::string outputDir = getOutputDirFromFilename(lodFiles[0]);

	MeshLoader meshLoader;
	meshLoader.exportMesh(lodFiles, meshName, outputDir, checkForDuplicates, isPhysics, exportBinary, outputDirInFile, webgl, scene);

	return 0;
}