#include <boost/unordered_map.hpp>
#include <boost/filesystem.hpp>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <Utils/FileUtils.h>
#include <tinyxml/tinyxml.h>

#define OUTPUT_CLASS_NAME "Factory"
#define GAME_OBJECT_CLASSES_LOCATION "src/GameObjects/"
#define CLASS_DEFINITIONS_LOCATION "data/class_definitions/"
#define GAME_OBJECT_FILES_LOCATION "data/game_objects/"
#define MESHES_FOLDER_NAME "meshes"
#define MESHES_LOCATION "data/meshes/"
#define MATERIALS_LOCATION "data/materials/"




//foward ref
void lookAtGameObjectFiles(std::string filename, std::string filepath);


std::string filesToInclude;
std::string staticVarsInHeader;

std::string staticVarsInCpp;
std::string gameObjectData;
std::string meshInitializations;
std::string materialData;


//loop over all files starting with the given directory, call the function pointer on each file
void loopOverFilePath(const boost::filesystem3::path dir_path, void (*operation)(std::string, std::string))
{
	if ( !exists( dir_path ) ) return;
    boost::filesystem3::directory_iterator end_itr;
    for (boost::filesystem3::directory_iterator itr(dir_path); itr != end_itr; ++itr )
    {
        if (is_directory(itr->status()))
        {
            loopOverFilePath(itr->path(), operation);
        }
        else
        {
			operation(itr->path().stem().string(), itr->path().string());
        }
    }
}

//loop over all the meshes in the meshes folder.
std::vector<std::string> activeFolders;
void loopOverMeshFiles(const boost::filesystem3::path dir_path)
{
	if ( !exists( dir_path ) ) return;
    boost::filesystem3::directory_iterator end_itr;
    for (boost::filesystem3::directory_iterator itr(dir_path); itr != end_itr; ++itr )
    {
        if (is_directory(itr->status()))
        {
			std::string folderName = itr->path().stem().string();
			activeFolders.push_back(folderName);
			//before
			meshInitializations += "    std::vector<std::string> " + folderName + "FolderFiles;\n";
            loopOverMeshFiles(itr->path());
			//after
			meshInitializations += "    meshFolderMap[\"" + folderName + "\"] = " + folderName + "FolderFiles;\n";
			activeFolders.pop_back();
        }
        else
        {
			
			std::string filepath = itr->path().string();
			std::string filename = itr->path().stem().string();
			if(filepath.substr(filepath.length()-4).compare(".xml") == 0)
			{
				//during
				std::string filepathRelative = filepath;
				filepathRelative = filepathRelative.substr(filepathRelative.find("data"));
				filepathRelative = Utils::convertToFowardSlashes(filepathRelative);


				meshInitializations += "    meshFilenameMap[\"" + filename + "\"] = \"" + filepathRelative + "\";\n";

				for(unsigned int i = 0; i < activeFolders.size(); i++)
				{
					meshInitializations += "    " + activeFolders[i] + "FolderFiles.push_back(\"" + filepathRelative + "\");\n";
				}
			}
        }
    }
}

//loop over all materials in the material folder (assumed to be a flat depth)
void lookAtMaterials(std::string filename, std::string filepath)
{
	if(filepath.substr(filepath.length()-4).compare(".xml") == 0)
	{

		std::string filepathRelative = filepath;
		filepathRelative = filepathRelative.substr(filepathRelative.find("data"));
		filepathRelative = Utils::convertToFowardSlashes(filepathRelative);

		materialData += "    materialFilenameMap.push_back(std::pair<std::string, std::string>(\"" + filename + "\", \"" + filepathRelative + "\"));\n"; 
	}

}


struct GameObjectData
{
	bool isArchetype;
	std::string relativeFilePath;

	std::string baseClassName;
};


boost::unordered_map<std::string, GameObjectData*> gameObjectsPermanent;
boost::unordered_map<std::string, GameObjectData*> gameObjects;


GameObjectData* findGameObjectByName(boost::unordered_map<std::string, GameObjectData*>& map, std::string name)
{
	boost::unordered_map<std::string, GameObjectData*>::iterator objectIter = map.find(name);	
	if(objectIter != map.end())
	{
		return objectIter->second;
	}

	return 0;
}


void processGameObject(std::string shortName)
{

	GameObjectData* data = findGameObjectByName(gameObjects, shortName);
	if(data != 0)
	{
		std::string filepath = data->relativeFilePath;

		//load xml
		TiXmlDocument theDoc;
		theDoc.LoadFile(filepath.c_str());

		if(theDoc.Error())
			throw std::runtime_error(theDoc.ErrorDesc());

		TiXmlHandle docHandle(&theDoc);

		//check if the derived class has been looked at already
		std::string derivedClass = docHandle.FirstChild("derived_class").ToElement()->FirstChild()->Value();
		if(findGameObjectByName(gameObjects, derivedClass))
		{
			processGameObject(derivedClass);
		}

		GameObjectData* derivedObjectData = findGameObjectByName(gameObjectsPermanent, derivedClass);
		if(derivedObjectData)
		{
			if(derivedObjectData->isArchetype)
			{
				data->baseClassName = derivedClass;
			}
			else
			{
				data->baseClassName = derivedObjectData->baseClassName;
			}
		}

			


		//handle attachments
		const TiXmlElement* node = docHandle.FirstChild().ToElement();
		for(; node != 0; node = node->NextSiblingElement())
		{
			if(std::string(node->Value()) == "attached")
			{
				std::string attachedName = node->FirstChild()->Value();
				if(findGameObjectByName(gameObjects, attachedName))
				{
					processGameObject(attachedName);
				}
			}
		}



		//process file
		if(data->isArchetype)
		{

			std::string shortNameUppercase = shortName;
			shortNameUppercase.at(0) -= 32;
			staticVarsInHeader += "    static " + shortNameUppercase + "* " + shortName + ";\n";

			filesToInclude += "#include \"GameObjects/" + shortName + ".h\"\n";

			gameObjectData += "    " + std::string(OUTPUT_CLASS_NAME) + "::" + shortName + " = new " + shortNameUppercase + "(loadDocFromFile(\"" + filepath + "\"), 0);\n";
			gameObjectData += "    gameObjectList[\"" + shortName + "\"] = " + shortName + ";\n";

			staticVarsInCpp += shortNameUppercase + "* " + OUTPUT_CLASS_NAME + "::" + shortName + ";\n";

		}
		else
		{
			std::string baseClass = data->baseClassName;
			std::string baseClassUppercase = baseClass;
			baseClassUppercase.at(0) -= 32;

			staticVarsInHeader += "    static " + baseClassUppercase + "* " + shortName + ";\n";

			gameObjectData += "    " + std::string(OUTPUT_CLASS_NAME) + "::" + shortName + " = new " + baseClassUppercase + "(loadDocFromFile(\"" + filepath + "\"), Factory::" + derivedClass + ");\n";
			gameObjectData += "    gameObjectList[\"" + shortName + "\"] = " + shortName + ";\n";
				

			staticVarsInCpp += baseClassUppercase + "* " + OUTPUT_CLASS_NAME + "::" + shortName + ";\n";
		}

		//remove this class from the sourceClassFiles map and game object map
		gameObjects.erase(shortName);
	}
}




void pushClassArchetypes(std::string filename, std::string filepath)
{
	if(filepath.substr(filepath.length()-4).compare(".xml") == 0)
	{

		std::string filepathRelative = filepath;
		filepathRelative = filepathRelative.substr(filepathRelative.find("data"));
		filepathRelative = Utils::convertToFowardSlashes(filepathRelative);

		GameObjectData* data = new GameObjectData();
		data->isArchetype = true;
		data->relativeFilePath = filepathRelative;
			
		gameObjectsPermanent[filename] = data;
		gameObjects[filename] = data;
	}
}

void pushGameObjects(std::string filename, std::string filepath)
{
	if(filepath.substr(filepath.length()-4).compare(".xml") == 0)
	{
		std::string filepathRelative = filepath;
		filepathRelative = filepathRelative.substr(filepathRelative.find("data"));
		filepathRelative = Utils::convertToFowardSlashes(filepathRelative);
		
		GameObjectData* data = new GameObjectData();
		data->isArchetype = false;
		data->relativeFilePath = filepathRelative;
			
		gameObjectsPermanent[filename] = data;
		gameObjects[filename] = data;
	}
}


int main()
{
	//sets the root path of the executable
    Utils::setFilePath(boost::filesystem3::current_path());

	/*----------------------------
		Create .h file
	-----------------------------*/

	boost::filesystem3::path classDefinitionsPath(Utils::getFilePath(CLASS_DEFINITIONS_LOCATION));
	loopOverFilePath(classDefinitionsPath, &pushClassArchetypes);

	boost::filesystem3::path gameObjectsPath(Utils::getFilePath(GAME_OBJECT_FILES_LOCATION));
	loopOverFilePath(gameObjectsPath, &pushGameObjects);


	while(gameObjects.size() > 0)
	{

		boost::unordered_map<std::string, GameObjectData*>::iterator it = gameObjects.begin();
		processGameObject(it->first);
	}



	
	//loop over meshes
	boost::filesystem3::path meshesPath(Utils::getFilePath(MESHES_LOCATION));
	activeFolders.push_back(MESHES_FOLDER_NAME);
	meshInitializations += "    std::vector<std::string> " + std::string(MESHES_FOLDER_NAME) + "FolderFiles;\n";
	loopOverMeshFiles(meshesPath);
	meshInitializations += "    meshFolderMap[\"" + std::string(MESHES_FOLDER_NAME) + "\"] = " + MESHES_FOLDER_NAME + "FolderFiles;\n";

	//loop over materials
	boost::filesystem3::path materialsSource(Utils::getFilePath(MATERIALS_LOCATION));
	loopOverFilePath(materialsSource, &lookAtMaterials);


	std::stringstream hFile;

	hFile << 
	"//*** THIS FILE IS AUTOGENERATED - DO NOT MODIFY ***\n\
#pragma once\n\
#include <boost/unordered_map.hpp>\n\
#include <tinyxml/tinyxml.h>\n\
#include \"Utils/FileUtils.h\"\n\
#include \"Rendering/GL_MeshPrimitive.h\"\n\
#include \"Singleton.h\"\n\
\n"
	<< filesToInclude
	<<
"\n\
class " << OUTPUT_CLASS_NAME << "\n\
{\n\
public:\n"
	<< staticVarsInHeader
	<<
	
	"\n\
	boost::unordered_map<std::string, std::string> meshFilenameMap;\n\
	boost::unordered_map<std::string, std::vector<std::string>> meshFolderMap;\n\
	boost::unordered_map<std::string, GameObject*> gameObjectList;\n\
	std::vector<std::pair<std::string, std::string>> materialFilenameMap;\n\
	\n" 
	<<
	"    " << OUTPUT_CLASS_NAME << "();\n\
	void loadGameObjects();\n\
};\n";


	std::stringstream cppFile;

	cppFile <<
	"//*** THIS FILE IS AUTOGENERATED - DO NOT MODIFY ***\n\
#include \"" << OUTPUT_CLASS_NAME << ".h\"\n\
\n\
TiXmlDocument loadDocFromFile(std::string filepathRelative)\n\
{\n\
	std::string filepath = Utils::getFilePath(filepathRelative);\n\
	TiXmlDocument theDoc;\n\
	theDoc.LoadFile(filepath.c_str());\n\
	\n\
	if(theDoc.Error())\n\
		throw std::runtime_error(theDoc.ErrorDesc());\n\
	\n\
	return theDoc;\n\
}\n\
\n"

	<< staticVarsInCpp
	<< 
"\n"
	<<
	OUTPUT_CLASS_NAME << "::" << OUTPUT_CLASS_NAME << "()\n\
{\n"
	<< materialData
	<<
"\n"
	<< meshInitializations
	<<
"}\n\n"
	<<
"void " << OUTPUT_CLASS_NAME << "::loadGameObjects()\n\
{\n"
	<< gameObjectData
	<<
"}\n";


	std::ofstream sourceFile;
	sourceFile.open (Utils::getFilePath("src/") + OUTPUT_CLASS_NAME + ".cpp");
	sourceFile << cppFile.str();
	sourceFile.close();	

	//TODO save in the right spot
	std::ofstream headerFile;
	headerFile.open (Utils::getFilePath("src/") + OUTPUT_CLASS_NAME + ".h");
	headerFile << hFile.str();
	headerFile.close();	
	
	system("pause");
	return 0;
}