#pragma once


#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <vector>
#include "Utils/MathUtils.h"






class Transformations
{
public:

    Transformations(void);
    virtual ~Transformations(void);


    void translateX(float amount);
    void translateY(float amount);
    void translateZ(float amount);
    void translate(glm::vec3 vector);

    void scaleX(float amount);
    void scaleY(float amount);
    void scaleZ(float amount);
    void scale(float amount);
    void scale(glm::vec3 vector);

    void yaw(float angle);
    void pitch(float angle);
    void roll(float angle);
    void rotate(glm::vec3 axis, float angle);

    void setTranslationX(float amount);
    void setTranslationY(float amount);
    void setTranslationZ(float amount);
    void setTranslation(glm::vec3 vector);
    void setScaleX(float amount);
    void setScaleY(float amount);
    void setScaleZ(float amount);
    void setScale(float amount);
    void setScale(glm::vec3 vector);
    void setRotation(glm::vec3 axis, float angle);
	void setRotation(glm::fquat rotation);
	void setRotation(glm::mat4 rotation);

    static glm::mat4 getTranslationMatrix(glm::vec3 vector);
    static glm::mat4 getScaleMatrix(float amount);
    static glm::mat4 getScaleMatrix(glm::vec3 vector);
    static glm::mat4 getRotationMatrixDegrees(glm::vec3 axis, float angleDegrees);
    static glm::mat4 getRotationMatrixRads(glm::vec3 axis, float angleRads);

    bool uniformScale;

    glm::mat4 getTransformationMatrix(void);
	void setTransformationMatrix(glm::mat4 transformationMatrix);


protected:



    glm::mat4 translationMatrix;
    glm::mat4 scaleMatrix;
    glm::mat4 rotationMatrix;

    glm::fquat orientation;

    glm::mat4 transformationMatrix;

    void updateTransformationMatrix();

};



