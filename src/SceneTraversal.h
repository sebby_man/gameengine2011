#pragma once

#include <stack>
#include <vector>
#include <string>
#include <glm/glm.hpp>

class GameObject;
class JointObject;
class AnimatedObject;
class LightObject;
class Scene;


namespace SceneTraversers
{
	//Traversal actions
	struct TraversalAction
	{
		//doesnt do anything besides clump them all as one thing
	};

	/*---------------------------------------------
	Finder: finds an object and stores it when found
	---------------------------------------------*/
	struct Finder : public TraversalAction
	{
		std::string name;
		GameObject* resultStorer;
		bool doneTraversing;

		Finder(std::string name);
		void traverse(GameObject* gameObject);

	};

	/*-------------------------------------------------------------
	Deleter: searches and deletes an object (and its descendents)
	-------------------------------------------------------------*/
	struct Deleter : public TraversalAction
	{
		Deleter(GameObject* gameObject);
		void traverse(GameObject* gameObject);
	};

	/*-------------------------------------------------------------
	TransformsUpdater: Updates object transformtions from top->down
	-------------------------------------------------------------*/
	struct TransformsUpdater : public TraversalAction
	{
		std::stack<glm::mat4> transformsStack;
		TransformsUpdater(GameObject* gameObject);
		void traverse(GameObject* gameObject);
	};

	/*-------------------------------------------------------------
	Updater: Calls the update() function for all game objects
	-------------------------------------------------------------*/
	struct Updater : public TraversalAction
	{
		void traverse(GameObject* gameObject);
	};

	/*-------------------------------------------------------------
	AddGameObject: Adds an object to the scene (partitions into different lists)
	-------------------------------------------------------------*/
	struct AddGameObject : public TraversalAction
	{
		Scene* scene;
		AddGameObject(Scene* scene);
		void traverse(GameObject* gameObject);
	};
};