#include "GameState.h"

GameState::GameState()
{
	Singleton<InputHandler>::Instance()->addEnterFrameEventListener(EnterFrameReceiver::from_method<GameState,&GameState::enterFrame>(this));
}
GameState::~GameState(void)
{
	Singleton<InputHandler>::Instance()->removeEnterFrameListener(EnterFrameReceiver::from_method<GameState,&GameState::enterFrame>(this));
}

void GameState::enterFrame(void)
{
	Singleton<PhysicsWorld>::Instance()->update(); //Update physics before rendering
	world.update();
}
void GameState::resizeApp(int width, int height)
{
	Singleton<GL_GameState>::Instance()->reshape(width, height);
}