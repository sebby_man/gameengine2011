#include "Transformations.h"


Transformations::Transformations(void)
{

    translationMatrix = glm::mat4(1.0f);
    scaleMatrix = glm::mat4(1.0f);
    rotationMatrix = glm::mat4(1.0f);
   
    orientation = glm::fquat(1.0f, 0.0f, 0.0f, 0.0f);

    uniformScale = true;


}

void Transformations::translateX(float amount)
{
    translationMatrix[3].x += amount;
	updateTransformationMatrix();
}
void Transformations::translateY(float amount)
{
    translationMatrix[3].y += amount;
	updateTransformationMatrix();
}
void Transformations::translateZ(float amount)
{
    translationMatrix[3].z += amount;
	updateTransformationMatrix();
}
void Transformations::translate(glm::vec3 vector)
{
    translationMatrix[3].x += vector.x;
    translationMatrix[3].y += vector.y;
    translationMatrix[3].z += vector.z;
	updateTransformationMatrix();
}

void Transformations::setTranslationX(float amount)
{
    translationMatrix[3].x = amount;
	updateTransformationMatrix();
}
void Transformations::setTranslationY(float amount)
{
    translationMatrix[3].y = amount;
	updateTransformationMatrix();
}
void Transformations::setTranslationZ(float amount)
{
    translationMatrix[3].z = amount;
	updateTransformationMatrix();
}
void Transformations::setTranslation(glm::vec3 vector)
{
    translationMatrix = getTranslationMatrix(vector);
	updateTransformationMatrix();
}

void Transformations::scaleX(float amount)
{
    scaleMatrix[0].x *= amount;
	updateTransformationMatrix();
}

void Transformations::scaleY(float amount)
{
    scaleMatrix[1].y *= amount;
	updateTransformationMatrix();
}

void Transformations::scaleZ(float amount)
{
    scaleMatrix[2].z *= amount;
	updateTransformationMatrix();
}

void Transformations::scale(float amount)
{
    scaleMatrix[0].x *= amount;
    scaleMatrix[1].y *= amount;
    scaleMatrix[2].z *= amount;
	updateTransformationMatrix();
}

void Transformations::scale(glm::vec3 vector)
{
    scaleMatrix[0].x *= vector.x;
    scaleMatrix[1].y *= vector.y;
    scaleMatrix[2].z *= vector.z;
	updateTransformationMatrix();
}


void Transformations::setScale(float amount)
{
    scaleMatrix = getScaleMatrix(amount);
	updateTransformationMatrix();
}

void Transformations::setScale(glm::vec3 vector)
{
    scaleMatrix = getScaleMatrix(vector);
	updateTransformationMatrix();
}

void Transformations::rotate(glm::vec3 axis, float angle)
{

    //uses quaternions to rotate the mesh from its previous orientation

    float fAngRad = angle*Utils::degToRad;
    glm::vec3 axisNorm = glm::normalize(axis);
    axisNorm = axisNorm * sinf(fAngRad / 2.0f);
    float scalar = cosf(fAngRad / 2.0f);

    glm::fquat offset(scalar, axisNorm.x, axisNorm.y, axisNorm.z);
    orientation = orientation * offset;
    orientation = glm::normalize(orientation);

    rotationMatrix = glm::mat4_cast(orientation);
	updateTransformationMatrix();
}

void Transformations::yaw(float angle)
{
    glm::vec3 yawAxis(0,1,0);
    rotate(yawAxis, angle);
}
void Transformations::pitch(float angle)
{
    glm::vec3 pitchAxis(1,0,0);
    rotate(pitchAxis, angle);
}
void Transformations::roll(float angle)
{
    glm::vec3 rollAxis(0,0,1);
    rotate(rollAxis, angle);
}



void Transformations::setRotation(glm::vec3 axis, float angle)
{

	//uses quaternions to rotate the mesh from its previous orientation

    glm::mat4 newRotationMatrix = getRotationMatrixDegrees(axis, angle);
    rotationMatrix = newRotationMatrix;
    orientation = glm::quat_cast(newRotationMatrix);
	updateTransformationMatrix();
}

void Transformations::setRotation(glm::fquat rotation)
{
	orientation = rotation;
	rotationMatrix = glm::mat4_cast(orientation);
	updateTransformationMatrix();
}

void Transformations::setRotation(glm::mat4 rotation)
{
	rotationMatrix = rotation;
	orientation = glm::quat_cast(rotation);
	updateTransformationMatrix();
}


glm::mat4 Transformations::getTranslationMatrix(glm::vec3 vector)
{
    glm::mat4 theMat(1.0f);
    theMat[3].x = vector.x;
    theMat[3].y = vector.y;
    theMat[3].z = vector.z;
    return theMat;
}
glm::mat4 Transformations::getScaleMatrix(float amount)
{
    glm::mat4 theMat(1.0f);
    theMat[0].x = amount;
    theMat[1].y = amount;
    theMat[2].z = amount;
    return theMat;
}

glm::mat4 Transformations::getScaleMatrix(glm::vec3 vector)
{
    glm::mat4 theMat(1.0f);
    theMat[0].x = vector.x;
    theMat[1].y = vector.y;
    theMat[2].z = vector.z;
    return theMat;
}

glm::mat4 Transformations::getRotationMatrixDegrees(glm::vec3 axis, float angleDegrees)
{
    return getRotationMatrixRads(axis, angleDegrees * Utils::degToRad);
}

glm::mat4 Transformations::getRotationMatrixRads(glm::vec3 axis, float angleRads)
{
    float fCos = cosf(angleRads);
    float fInvCos = 1.0f - fCos;
    float fSin = sinf(angleRads);
    //float fInvSin = 1.0f - fSin;

    glm::vec3 axisNorm = glm::normalize(axis);

    glm::mat4 theMat(1.0f);
    theMat[0].x = (axisNorm.x * axisNorm.x) + ((1 - axisNorm.x * axisNorm.x) * fCos);
    theMat[1].x = axisNorm.x * axisNorm.y * (fInvCos) - (axisNorm.z * fSin);
    theMat[2].x = axisNorm.x * axisNorm.z * (fInvCos) + (axisNorm.y * fSin);

    theMat[0].y = axisNorm.x * axisNorm.y * (fInvCos) + (axisNorm.z * fSin);
    theMat[1].y = (axisNorm.y * axisNorm.y) + ((1 - axisNorm.y * axisNorm.y) * fCos);
    theMat[2].y = axisNorm.y * axisNorm.z * (fInvCos) - (axisNorm.x * fSin);

    theMat[0].z = axisNorm.x * axisNorm.z * (fInvCos) - (axisNorm.y * fSin);
    theMat[1].z = axisNorm.y * axisNorm.z * (fInvCos) + (axisNorm.x * fSin);
    theMat[2].z = (axisNorm.z * axisNorm.z) + ((1 - axisNorm.z * axisNorm.z) * fCos);

    return theMat;
}











void Transformations::updateTransformationMatrix()
{
    transformationMatrix = translationMatrix * rotationMatrix * scaleMatrix;
}

glm::mat4 Transformations::getTransformationMatrix(void)
{
    return transformationMatrix;

}
void Transformations::setTransformationMatrix(glm::mat4 transformationMatrix)
{
	this->transformationMatrix = transformationMatrix;
}


Transformations::~Transformations(void)
{
}
