#include "Scene.h"

Scene::Scene(void)
{
	//The root node is at the top of the scene graphy
	root = Factory::gameObject->getCopy();
	root->setName("root");
	root->setNewParent(0);
}
Scene::~Scene(void)
{
	//Do nothing ... yet
}

void Scene::update()
{
	//process transforms down the scene graph
	SceneTraversers::TransformsUpdater transformUpdateAction(root);
    transformUpdateAction.traverse(root);

	//update the current camera matrix
	currentCamera->update();
	currentCamera->updateWorldToCameraMatrix();

	GL_Lighting lightingGL;
	int numLights = lights.size();
	for(int i = 0; i < numLights; i++)
	{
		//update lights
		GL_Light lightGL;

		LightObject* lightObject = lights[i];
		lightGL.lightIntensity = glm::vec4(lightObject->rgb, 1);
		lightGL.lightAttenuation = lightObject->lightAttenuation;
		lightGL.cameraSpaceLightPos = currentCamera->getWorldToCameraMatrix()*glm::vec4(lightObject->getPosition(), 1);

		lightingGL.lights[i] = lightGL;
		
	}
	Singleton<GL_ShaderState>::Instance()->setLightData(lightingGL, numLights);

	//update 
	Singleton<GL_GameState>::Instance()->prepareRenderForOpaqueObjects();
	for(unsigned int i = 0; i < objects.size(); i++)
	{
		objects[i]->update();
	}
	
	if(TRANSPARENCY_ENABLED)
		Singleton<GL_GameState>::Instance()->prepareRenderForTransparentObjects();
	
	for(unsigned int i = 0; i < transparentObjects.size(); i++)
	{
		transparentObjects[i]->update();
	}

	if(TRANSPARENCY_ENABLED)
		Singleton<GL_GameState>::Instance()->finalizeTransparency();
	
}

GameObject* Scene::findGameObject(std::string name)
{
	SceneTraversers::Finder findAction(name);
    findAction.traverse(root);
    GameObject* result = findAction.resultStorer;
    return result;
}

//Delete GameObject
void Scene::deleteGameObject(std::string name)
{
    GameObject* gameObject = findGameObject(name);
    if(gameObject != 0 && !gameObject->isRoot())
    {
        deleteGameObject(gameObject);
    }
}
void Scene::deleteGameObject(GameObject* object)
{
    if(!object->isRoot())
    {
		SceneTraversers::Deleter deleteAction(object);
        deleteAction.traverse(object);
    }
}

//Add GameObject
GameObject* Scene::addGameObject(GameObject* object)
{
	return addGameObject(object, root);
}
GameObject* Scene::addGameObject(GameObject* object, GameObject* parentNode)
{
	object->setNewParent(parentNode);
	
	SceneTraversers::AddGameObject addGameObjectAction(this);
	addGameObjectAction.traverse(object);
	
    return object;
}
GameObject* Scene::addGameObject(GameObject* object, std::string parentName)
{
	GameObject* parentNode = this->findGameObject(parentName);
    if(parentNode != 0)
    {
		return addGameObject(object, parentNode);
    }

    return 0;
}

std::vector<GameObject*>& Scene::getObjects()
{
	return this->objects;
}
std::vector<GameObject*>& Scene::getTransparentObjects()
{
	return this->transparentObjects;
}
std::vector<LightObject*>& Scene::getLights()
{
	return this->lights;
}
std::vector<CameraObject*>& Scene::getCameras()
{
	return this->cameras;
}