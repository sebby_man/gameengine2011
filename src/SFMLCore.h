#pragma once

#include "GameState.h"
#include "Singleton.h"
#include "InputHandler.h"
#include "Factory.h"

#include <iostream>

#include <SFML/Window.hpp>



//==============================
// Keeps the application ticking
//==============================

class SFMLCore
{
public:
    SFMLCore(void);
    virtual ~SFMLCore(void);
private:

	sf::Window* window;
	void showFPS();
	std::string title;
	sf::Clock clock;
	int frameCount;
	float framerate;

};

