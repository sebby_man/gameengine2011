#include <gl3w/gl3w.h>
#include "Utils/FileUtils.h"
#include "Utils/GameUtils.h"
#include "Singleton.h"
#include "SFMLCore.h"
#include <boost/filesystem.hpp>
#include <time.h>

int main (int argc, char **argv)
{

	//Create random seed
	srand((unsigned int)(time(0)));

	//sets the root path of the executable
    Utils::setFilePath(boost::filesystem3::current_path());

    //run SFML, the media library. This will initiate the game
    Singleton<SFMLCore>::Instance();


    return 0;
}

