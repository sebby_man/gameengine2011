#pragma once

#include <glm/glm.hpp>
#include <vector>
#include <string>

#include "SceneTraversal.h"
#include "Transformations.h"
#include "GameObjects/GameObject.h"
#include "GameObjects/RenderObject.h"
#include "GameObjects/PhysicsObject.h"
#include "Physics/PhysicsWorld.h"
#include "Factory.h"

class Scene
{
public:

	Scene(void);
    ~Scene(void);

	//Updates all of the objects. World is responsible for calling this. 
	void update(); 

	//Add and remove game objects
	void deleteGameObject(std::string name);
    void deleteGameObject(GameObject* object);

	GameObject* addGameObject(GameObject* object);
    GameObject* addGameObject(GameObject* object, GameObject* parentObject);
    GameObject* addGameObject(GameObject* object, std::string parentName);

	GameObject* findGameObject(std::string name); //Looks through scene graph for object with this name

	CameraObject* currentCamera;
	
	std::vector<GameObject*>& getObjects();
	std::vector<GameObject*>& getTransparentObjects();
	std::vector<LightObject*>& getLights();
	std::vector<CameraObject*>& getCameras();
	
private:
	

	std::vector<GameObject*> objects;
	std::vector<GameObject*> transparentObjects;
	std::vector<LightObject*> lights;
	std::vector<CameraObject*> cameras;
	
	GameObject* root; //invisible GameObject that aids the scene graph
};












