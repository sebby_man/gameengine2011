#pragma once
#include "Rendering/GL_MeshPrimitive.h"
#include "GameObjects/AnimatedObject.h"
#include "GameObjects/JointObject.h"

class ColladaData
{
public:
	GL_MeshPrimitive* meshPrimitive;
	std::vector<AnimatedObject::AnimationClip> animationClips;
	std::vector<JointObject*> jointList;
	JointObject* rootJoint;

};