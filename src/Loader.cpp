#include "Loader.h"




Loader::Loader()
{
	loadMaterials();
}

void Loader::fillMaterialBlock(TiXmlElement* xmlElement, MaterialBlock& materialBlock)
{
	//set diffuse color
	TiXmlElement* diffuseColorElement = xmlElement->FirstChildElement("diffuseColor");
	if(diffuseColorElement)
	{
		float red = Utils::stringToFloat(diffuseColorElement->FirstChildElement("r")->FirstChild()->Value());
		float green = Utils::stringToFloat(diffuseColorElement->FirstChildElement("g")->FirstChild()->Value());
		float blue = Utils::stringToFloat(diffuseColorElement->FirstChildElement("b")->FirstChild()->Value());
		materialBlock.diffuseColor = glm::vec4(red, green, blue, 1);
	}

	//set specular color
	TiXmlElement* specularColorElement = xmlElement->FirstChildElement("specularColor");
	if(specularColorElement)
	{
		float red = Utils::stringToFloat(specularColorElement->FirstChildElement("r")->FirstChild()->Value());
		float green = Utils::stringToFloat(specularColorElement->FirstChildElement("g")->FirstChild()->Value());
		float blue = Utils::stringToFloat(specularColorElement->FirstChildElement("b")->FirstChild()->Value());
		materialBlock.specularColor = glm::vec4(red, green, blue, 1);
	}

	//set specular shininess
	TiXmlElement* specularShininessElement = xmlElement->FirstChildElement("specularShininess");
	if(specularShininessElement)
	{
		float specularShininess = Utils::stringToFloat(specularShininessElement->FirstChild()->Value());
		materialBlock.specularShininess = specularShininess;
	}

	//set transparency
	TiXmlElement* transparencyElement = xmlElement->FirstChildElement("transparency");
	if(transparencyElement)
	{
		float transparency = Utils::stringToFloat(transparencyElement->FirstChild()->Value());
		materialBlock.transparency = transparency;
	}
}

void Loader::loadMaterials()
{
	std::vector<std::pair<std::string, std::string>> filepaths = Singleton<Factory>::Instance()->materialFilenameMap;
	
	for(unsigned int i = 0; i < filepaths.size(); i++)
	{
		std::string fullFilepath = Utils::getFilePath(filepaths[i].second);
		TiXmlDocument doc;
		doc.LoadFile(fullFilepath.c_str());
		if(doc.Error())
			throw std::runtime_error(doc.ErrorDesc());
		TiXmlHandle docHandle(&doc);

		MaterialBlock materialBlock;
		/*materialBlock.diffuseColor = glm::vec4(1,1,1,1);
		materialBlock.specularColor = glm::vec4(1,1,1,1);
		materialBlock.specularShininess = .1f;*/
		fillMaterialBlock(doc.FirstChild("material")->ToElement(), materialBlock);

		materials[filepaths[i].first] = materialBlock;
	}

}

void Loader::loadSceneObject(const TiXmlElement* parentElement, Scene* scene, GameObject* parentObject)
{
	const TiXmlElement* child = parentElement->FirstChildElement("node");

	//loop over the top level scene graph nodes
	for(; child != 0; child = child->NextSiblingElement("node"))
	{
		//create game object based on the type
		std::string gameObjectType = child->Attribute("type");
		GameObject* object = getGameObjectByName(gameObjectType)->getCopy();

		//set name
		std::string name = child->Attribute("name");
		object->setName(name);

		//set translation
		const TiXmlElement* translateElement = child->FirstChildElement("translate");
		if(translateElement)
		{
			std::string translateData = translateElement->FirstChild()->Value();
			std::vector<std::string> translateComponents = Utils::parseSpaceSeparatedString(translateData);
			glm::vec3 translation(Utils::stringToFloat(translateComponents[0]), Utils::stringToFloat(translateComponents[1]), Utils::stringToFloat(translateComponents[2])); 
			
			//TODO: would rather avoid the cast
			PhysicsObject* checkForPhysicsObject = dynamic_cast<PhysicsObject*>(object);
			if(checkForPhysicsObject)
			{
				checkForPhysicsObject->setTranslation(translation, false);
			}
			else
			{
				object->translate(translation, false);
			}

			
			
		}

		//set rotation
		const TiXmlElement* rotateElement = child->FirstChildElement("rotate");
		if(rotateElement)
		{
			std::string rotateData = rotateElement->FirstChild()->Value();
			std::vector<std::string> rotateComponents = Utils::parseSpaceSeparatedString(rotateData);
			glm::vec3 rotationAxis(Utils::stringToFloat(rotateComponents[0]), Utils::stringToFloat(rotateComponents[1]), Utils::stringToFloat(rotateComponents[2])); 
			float angle = Utils::stringToFloat(rotateComponents[3]);
			object->rotate(rotationAxis, angle, false);
		}

		//set scale
		const TiXmlElement* scaleElement = child->FirstChildElement("scale");
		if(scaleElement)
		{
			std::string scaleData = scaleElement->FirstChild()->Value();
			std::vector<std::string> scaleComponents = Utils::parseSpaceSeparatedString(scaleData);
			glm::vec3 scale(Utils::stringToFloat(scaleComponents[0]), Utils::stringToFloat(scaleComponents[1]), Utils::stringToFloat(scaleComponents[2])); 
			object->scale(scale, false);
		}

		if(parentObject == 0)
			scene->addGameObject(object);
		else
			scene->addGameObject(object, parentObject);

		loadSceneObject(child, scene, object);
	}
}

Scene* Loader::loadScene(std::string filepath)
{
	std::string fullFilepath = Utils::getFilePath(filepath);
	TiXmlDocument doc;
	doc.LoadFile(fullFilepath.c_str());
	if(doc.Error())
		throw std::runtime_error(doc.ErrorDesc());
	TiXmlHandle docHandle(&doc);
	TiXmlElement* sceneElement = docHandle.FirstChild("scene").ToElement();
	if(sceneElement)
	{
		std::string sceneName = sceneElement->Attribute("id");
		std::cout << "loading scene: " << filepath << std::endl;

		//load in meshes
		TiXmlElement* meshesToLoad = sceneElement->FirstChildElement("meshes");
		if(meshesToLoad)
		{
			const TiXmlElement* child = meshesToLoad->FirstChildElement();
			for(; child != 0; child = child->NextSiblingElement())
			{
				std::string importType = child->Value();
				if(importType.compare("import_folder") == 0)
				{
					std::string folderName = child->FirstChild()->Value();
					loadMeshFolder(folderName);
				}
				else if(importType.compare("import_file") == 0)
				{
					std::string meshName = child->FirstChild()->Value();
					loadMesh(meshName);
				}
			}
		}

		//create scene graph
		TiXmlElement* sceneGraphElement = sceneElement->FirstChildElement("scene_graph");
		Scene* scene = new Scene();
		if(sceneGraphElement)
		{
			loadSceneObject(sceneGraphElement, scene, 0);
			return scene;
		}
	}
	return 0;
}

GameObject* Loader::getGameObjectByName(std::string name)
{
	boost::unordered_map<std::string, GameObject*> gameObjectList = Singleton<Factory>::Instance()->gameObjectList;
	boost::unordered_map<std::string, GameObject*>::iterator found = gameObjectList.find(name);
	if(found == gameObjectList.end())
	{
		std::cout << "Could not find the given game object: " << name << std::endl;
		return 0;
	}
	else
	{
		return found->second;
	}
}

MaterialBlock Loader::getMaterialByName(std::string name)
{
	boost::unordered_map<std::string, MaterialBlock>::iterator found = materials.find(name);
	if(found == materials.end())
	{
		std::cout << "Could not find the given material name: " << name << std::endl;
		return materials.at("DEFAULT");
	}
	else
	{
		return found->second;
	}
}

void Loader::loadMeshFile(std::string filename)
{
	std::cout << "loading mesh: " << filename << std::endl;
	ColladaData outputData = readColladaAsset(filename);
	unsigned int namePos = filename.find_last_of('/') + 1;
	std::string name = filename.substr(namePos, filename.find_last_of('.') - namePos);

	//give the render objects that use this COLLADA file the updated data (if its even referenced by render objects)
	boost::unordered_map<std::string, std::vector<RenderObject*>>::iterator found = meshToRenderObjectsMap.find(name);
	if(found != meshToRenderObjectsMap.end())
	{
		std::vector<RenderObject*> renderObjects = found->second;
		for(unsigned int i = 0; i < renderObjects.size(); i++)
		{
			RenderObject* renderObject = renderObjects[i];
			renderObject->loadColladaData(outputData);
		}
	}
	

}

void Loader::loadMesh(std::string name)
{
	boost::unordered_map<std::string, std::string> meshFilenameMap = Singleton<Factory>::Instance()->meshFilenameMap;
	boost::unordered_map<std::string, std::string>::iterator found = meshFilenameMap.find(name);
	if(found == meshFilenameMap.end())
	{
		std::cout << "Could not find the given mesh name: " << name << std::endl;
		return;
	}
	else
	{
		std::string filename = found->second;
		loadMeshFile(filename);
	}
}

void Loader::loadMeshFolder(std::string folderName)
{
	boost::unordered_map<std::string, std::vector <std::string> > meshFolderMap = Singleton<Factory>::Instance()->meshFolderMap;
	boost::unordered_map<std::string, std::vector <std::string> >::iterator found = meshFolderMap.find(folderName);
	if(found == meshFolderMap.end())
	{
		std::cout << "Could not find the given mesh folder name: " << folderName << std::endl;
		return;
	}
	else
	{
		std::vector<std::string> folderContents = found->second;
		for(unsigned int i = 0; i < folderContents.size(); i++)
		{
			loadMeshFile(folderContents[i]);
		}
	}
}


std::vector<float> Loader::parseDataIntoFloats(const std::string& configData, size_t size)
{

	std::vector<float> floats;
    floats.reserve(size);

	typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
	boost::char_separator<char> sep(" ");
	tokenizer tokens(configData, sep);
	for (tokenizer::iterator tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter)
	{
		floats.push_back((float)std::atof((*tok_iter).c_str()));
	}

	return floats;
}

std::vector<unsigned short> Loader::parseDataIntoUShorts(const std::string& configData, size_t size)
{
    std::vector<unsigned short> ushorts;
    ushorts.reserve(size);
    
	typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
	boost::char_separator<char> sep(" ");
	tokenizer tokens(configData, sep);
	for (tokenizer::iterator tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter)
	{
		ushorts.push_back((unsigned short)std::atoi((*tok_iter).c_str()));
	}   

    return ushorts;
}


glm::mat4 Loader::parseDataIntoMat4(const std::string& configData)
{
	//its ROW ORDER. Meaning the first 4 floats are the first row, not the first column
	std::vector<float> floats = parseDataIntoFloats(configData, 16);
	
	glm::mat4 matrix(0);
	matrix[0].x = floats[0];
	matrix[1].x = floats[1];
	matrix[2].x = floats[2];
	matrix[3].x = floats[3];

	matrix[0].y = floats[4];
	matrix[1].y = floats[5];
	matrix[2].y = floats[6];
	matrix[3].y = floats[7];

	matrix[0].z = floats[8];
	matrix[1].z = floats[9];
	matrix[2].z = floats[10];
	matrix[3].z = floats[11];

	matrix[0].w = floats[12];
	matrix[1].w = floats[13];
	matrix[2].w = floats[14];
	matrix[3].w = floats[15];

    return matrix;
}

std::vector<glm::vec3> Loader::parseTranslationData(const std::string& configData, int numTranslations)
{
	//its ROW ORDER. Meaning the first 4 floats are the first row, not the first column
	std::vector<glm::vec3> translationValues;
    translationValues.reserve(numTranslations);
	std::vector<float> floats = parseDataIntoFloats(configData, numTranslations*3);
	for(int i = 0; i < numTranslations; i++)
	{
		float x = floats[i*3 + 0];
		float y = floats[i*3 + 1];
		float z = floats[i*3 + 2];
		translationValues.push_back(glm::vec3(x,y,z));
	}
   
    return translationValues;
}

std::vector<glm::fquat> Loader::parseQuatData(const std::string& configData, int numQuats)
{
	std::vector<glm::fquat> quatValues;
    quatValues.reserve(numQuats);
	std::vector<float> floats = parseDataIntoFloats(configData, numQuats*4);
	for(int i = 0; i < numQuats; i++)
	{
		float x = floats[i*4 + 0];
		float y = floats[i*4 + 1];
		float z = floats[i*4 + 2];
		float w = floats[i*4 + 3];
		quatValues.push_back(glm::fquat(w,x,y,z));
	}
   
    return quatValues;
}

Vertex* Loader::loadVertexDataForStaticMesh(TiXmlElement* vertexData, int& numVertices, bool& containsPositions, bool& containsNormals, bool& containsUVs)
{
	std::string vertexCountString = vertexData->Attribute("count");
	numVertices = atoi(vertexCountString.c_str());

	std::string vertexDataString = vertexData->FirstChild()->Value();

	std::string truthString = "true";
	containsPositions = vertexData->Attribute("positions") == truthString;
	containsNormals = vertexData->Attribute("normals") == truthString;
	containsUVs = vertexData->Attribute("UVs") == truthString;
	if(containsPositions)
	{
		float width = (float)atof(vertexData->Attribute("width"));
		float height = (float)atof(vertexData->Attribute("height"));
		float depth = (float)atof(vertexData->Attribute("depth"));
	}

	typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
	boost::char_separator<char> sep(" ");
	tokenizer vertexDataTokens(vertexDataString, sep);
	tokenizer::iterator vertexDataIter = vertexDataTokens.begin();

	Vertex* vertices = new Vertex[numVertices];

	for(int i = 0; i < numVertices; i++)
	{
		if(containsPositions)
		{
			vertices[i].x = (float)std::atof((*(vertexDataIter++)).c_str());
			vertices[i].y = (float)std::atof((*(vertexDataIter++)).c_str());
			vertices[i].z = (float)std::atof((*(vertexDataIter++)).c_str());
		}

		if(containsNormals)
		{
			vertices[i].nx = (float)std::atof((*(vertexDataIter++)).c_str());
			vertices[i].ny = (float)std::atof((*(vertexDataIter++)).c_str());
			vertices[i].nz = (float)std::atof((*(vertexDataIter++)).c_str());
		}

		if(containsUVs)
		{
			vertices[i].s = (float)std::atof((*(vertexDataIter++)).c_str());
			vertices[i].t = (float)std::atof((*(vertexDataIter++)).c_str());
		}
	}   

	return vertices;

}

VertexSkeleton* Loader::loadVertexDataForAnimatedMesh(TiXmlElement* vertexData, int& numVertices, bool& containsPositions, bool& containsNormals, bool& containsUVs)
{


	std::string vertexCountString = vertexData->Attribute("count");
	numVertices = atoi(vertexCountString.c_str());

	std::string vertexDataString = vertexData->FirstChild()->Value();

	std::string truthString = "true";
	containsPositions = vertexData->Attribute("positions") == truthString;
	containsNormals = vertexData->Attribute("normals") == truthString;
	containsUVs = vertexData->Attribute("UVs") == truthString;
	if(containsPositions)
	{
		float width = (float)atof(vertexData->Attribute("width"));
		float height = (float)atof(vertexData->Attribute("height"));
		float depth = (float)atof(vertexData->Attribute("depth"));
	}

	typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
	boost::char_separator<char> sep(" ");
	tokenizer vertexDataTokens(vertexDataString, sep);
	tokenizer::iterator vertexDataIter = vertexDataTokens.begin();

	VertexSkeleton* vertices = new VertexSkeleton[numVertices];

	for(int i = 0; i < numVertices; i++)
	{
		if(containsPositions)
		{
			vertices[i].x = (float)std::atof((*(vertexDataIter++)).c_str());
			vertices[i].y = (float)std::atof((*(vertexDataIter++)).c_str());
			vertices[i].z = (float)std::atof((*(vertexDataIter++)).c_str());
		}

		if(containsNormals)
		{
			vertices[i].nx = (float)std::atof((*(vertexDataIter++)).c_str());
			vertices[i].ny = (float)std::atof((*(vertexDataIter++)).c_str());
			vertices[i].nz = (float)std::atof((*(vertexDataIter++)).c_str());
		}

		if(containsUVs)
		{
			vertices[i].s = (float)std::atof((*(vertexDataIter++)).c_str());
			vertices[i].t = (float)std::atof((*(vertexDataIter++)).c_str());
		}

		unsigned short numJoints = (unsigned short)std::atoi((*(vertexDataIter++)).c_str());
		vertices[i].numJoints = numJoints;

		for(unsigned short j = 0; j < numJoints; j++)
		{
			vertices[i].indexes[j] = (unsigned short)std::atoi((*(vertexDataIter++)).c_str());
			vertices[i].weights[j] = (float)std::atof((*(vertexDataIter++)).c_str());
		}
	}   

	return vertices;
	
}

std::vector<AnimatedObject::AnimationClip> Loader::loadAnimationData(TiXmlElement* animationData)
{

	std::vector<AnimatedObject::AnimationClip> animationClips;
	for(TiXmlElement* animationClipElement = animationData->FirstChildElement("animationClip"); animationClipElement != 0; animationClipElement = animationClipElement->NextSiblingElement("animationClip"))
	{
		AnimatedObject::AnimationClip animationClip;
		animationClip.duration = (float)atof(animationClipElement->Attribute("duration"));
		std::string loopType = animationClipElement->Attribute("loops");
		std::string trueString = "true";
		animationClip.isLoop = loopType == trueString;
		
		for(TiXmlElement* target = animationClipElement->FirstChildElement("target"); target != 0; target = target->NextSiblingElement("target"))
		{
			int numKeyframes = atoi(target->Attribute("num_keyframes"));
			AnimatedObject::JointAnimation* jointAnimation = new AnimatedObject::JointAnimation();
			jointAnimation->translations = parseTranslationData(target->FirstChildElement("translation")->FirstChild()->Value(), numKeyframes);
			jointAnimation->orientations = parseQuatData(target->FirstChildElement("rotation")->FirstChild()->Value(), numKeyframes);
			jointAnimation->keyframes = parseDataIntoFloats(target->FirstChildElement("time")->FirstChild()->Value(), numKeyframes);
			jointAnimation->jointIndex = atoi(target->Attribute("joint_id"));

			animationClip.animations.push_back(jointAnimation);
		}

		animationClips.push_back(animationClip);
	}

	return animationClips;
}



void Loader::loadSkeleton(TiXmlElement* node, JointObject* parentJoint, std::vector<JointObject*>& jointList)
{
	JointObject* joint = Factory::jointObject->getCopy(false, false);
	joint->setNewParent(parentJoint);
	std::string name = node->Attribute("id");
	joint->setName(name);
	joint->index = atoi(node->Attribute("joint_index"));
	jointList.push_back(joint);

	

	glm::fquat rotation = parseQuatData(node->FirstChildElement("rotation")->FirstChild()->Value(), 1)[0];
	joint->setRotation(rotation, false);
	
	glm::vec3 translation = parseTranslationData(node->FirstChildElement("translation")->FirstChild()->Value(), 1)[0];
	joint->setTranslation(translation, false);

	glm::mat4 inverseBindPoseMatrix = parseDataIntoMat4(node->FirstChildElement("inv_bind_pose")->FirstChild()->Value());
	joint->invBindPoseMatrix = inverseBindPoseMatrix;

	//loop over children nodes
	for(TiXmlElement* childNode = node->FirstChildElement("node"); childNode != 0; childNode = childNode->NextSiblingElement("node"))
	{
		loadSkeleton(childNode, joint, jointList);
	}
}


ColladaData Loader::readColladaAsset(std::string& fileName)
{
	std::string fullFilepath = Utils::getFilePath(fileName);
	TiXmlDocument doc;
	doc.LoadFile(fullFilepath.c_str());
	if(doc.Error())
		throw std::runtime_error(doc.ErrorDesc());
	TiXmlHandle docHandle(&doc);

	TiXmlElement* header = docHandle.FirstChildElement("Mesh").ToElement();

	std::string headerType = header->Attribute("type");

	TiXmlElement* elementArrayElement = header->FirstChildElement("element_array");
	int numElements = atoi(elementArrayElement->Attribute("count"));
	std::vector<GLushort> elementArray = parseDataIntoUShorts(elementArrayElement->FirstChild()->Value(), numElements);

	//the texture name may be ""
	std::string textureName = "";
	TiXmlElement* textureNameElement = header->FirstChildElement("texture_name");
	if(textureNameElement != 0)
	{
		textureName = textureNameElement->FirstChild()->Value();
	}

	TiXmlElement* vertexDataElement = header->FirstChildElement("vertex_data");
	int numVertices;
	bool containsPositions = false;
	bool containsNormals = false;
	bool containsUVs = false;

	ColladaData outputData;


	if(headerType == "animated")
	{
		VertexSkeleton* vertexData = loadVertexDataForAnimatedMesh(vertexDataElement, numVertices, containsPositions, containsNormals, containsUVs);
		
		TiXmlElement* skeletonElement = header->FirstChildElement("skeleton");
		loadSkeleton(skeletonElement->FirstChildElement("node"), 0, outputData.jointList);
		outputData.rootJoint = outputData.jointList[0];

		TiXmlElement* animationDataElement = header->FirstChildElement("animationData");
		outputData.animationClips = loadAnimationData(animationDataElement);
		
		outputData.meshPrimitive = new GL_MeshPrimitive(numVertices, vertexData, containsPositions, containsNormals, containsUVs, textureName, elementArray, GL_TRIANGLES, GL_STATIC_DRAW);    
	}
	else if(headerType == "static")
	{
		Vertex* vertexData = loadVertexDataForStaticMesh(vertexDataElement, numVertices, containsPositions, containsNormals, containsUVs);
		outputData.meshPrimitive = new GL_MeshPrimitive(numVertices, vertexData, containsPositions, containsNormals, containsUVs, textureName, elementArray, GL_TRIANGLES, GL_STATIC_DRAW);    
	}

	//maybe not needed
	//std::string name = docHandle.FirstChild("info").ToElement()->FirstChildElement("name")->FirstChild()->Value();

	return outputData;

}
