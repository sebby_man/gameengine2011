#pragma once
#include "GameObject.h"
#include <glm/glm.hpp>
#include <iostream>
#include "../Singleton.h"
#include "../Rendering/GL_ShaderState.h"
#include "CameraObject.h"

class CameraFirstPersonObject : public CameraObject
{
public:
	
	
	CameraFirstPersonObject(TiXmlDocument doc, CameraFirstPersonObject* basis);
	virtual CameraFirstPersonObject* getCopy();
	virtual CameraFirstPersonObject* getCopy(bool preserveTransforms, bool preserveChildren);
	virtual void setDefaultValues(bool preserveTransforms, bool preserveChildren);
	virtual ~CameraFirstPersonObject(void);
	virtual void init();
	virtual void update();

	virtual void rotate(float x, float y);
    virtual void zoom(float delta);
    virtual void pan(float x, float y);


protected:

	virtual void CalcMatrix(void);

	float currXZRads;
    float currYRads;


};
