#include "ExampleObject.h"
#include "../Factory.h"

ExampleObject::ExampleObject(TiXmlDocument doc, ExampleObject* basis) : GameObject(doc, basis == 0 ? Factory::gameObject : basis)
{
	
	if(basis != 0)
	{
		exampleString = basis->exampleString;
	}

	// See if this class has been overriden in the XML. If so, extract and set those variables
	TiXmlHandle docHandle(&doc);
	TiXmlElement* mainElement = docHandle.FirstChild("ExampleObject").ToElement();
	bool overridenInXML = (mainElement != 0);
	if(overridenInXML)
	{
		//set "exampleString"
		TiXmlElement* element = mainElement->FirstChildElement("exampleString");
		if(element != 0)
		{
			std::string exampleStringFromXML = element->FirstChild()->Value();
			exampleString = exampleStringFromXML;
			std::cout << exampleString << std::endl;
		}
	}

}

void ExampleObject::setDefaultValues(bool preserveTransforms, bool preserveChildren)
{
	GameObject::setDefaultValues(preserveTransforms, preserveChildren);
}
ExampleObject* ExampleObject::getCopy()
{
	ExampleObject* copy = new ExampleObject(*this);
	copy->setDefaultValues(false, false);
	return copy;
}

ExampleObject* ExampleObject::getCopy(bool preserveTransforms, bool preserveChildren)
{
	//note the copy constructor
	ExampleObject* copy = new ExampleObject(*this);
	copy->setDefaultValues(preserveTransforms, preserveChildren);
	return copy;
}

ExampleObject::~ExampleObject(void)
{
}

void ExampleObject::update()
{
	GameObject::update();
}