#pragma once
#include "GameObject.h"

class LightObject : public GameObject
{
public:
	
	glm::vec3 rgb;
	float lightAttenuation;

	LightObject(TiXmlDocument doc, LightObject* basis);
	virtual LightObject* getCopy();
	virtual LightObject* getCopy(bool preserveTransforms, bool preserveChildren);
	virtual void setDefaultValues(bool preserveTransforms, bool preserveChildren);
	virtual ~LightObject(void);
	virtual void update();
};

