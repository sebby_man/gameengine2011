#pragma once
#include "GameObject.h"
#include <glm/glm.hpp>
#include <iostream>
#include "../Singleton.h"
#include "../Rendering/GL_ShaderState.h"
#include "CameraObject.h"

class CameraThirdPersonObject : public CameraObject
{
public:
	
	
	CameraThirdPersonObject(TiXmlDocument doc, CameraThirdPersonObject* basis);
	virtual CameraThirdPersonObject* getCopy();
	virtual CameraThirdPersonObject* getCopy(bool preserveTransforms, bool preserveChildren);
	virtual void setDefaultValues(bool preserveTransforms, bool preserveChildren);
	virtual ~CameraThirdPersonObject(void);
	virtual void init();
	virtual void update();

	virtual void rotate(float x, float y);
    virtual void zoom(float delta);
    virtual void pan(float x, float y);

	glm::vec3 getLookAt(void);

protected:

	virtual void CalcMatrix(void);

	float currXZRads;
    float currYRads;
    float radius;
    glm::vec3 lookAt;

};
