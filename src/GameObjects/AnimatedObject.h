#pragma once
#include "RenderObject.h"
#include "JointObject.h"
#include <SFML/System.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include "../Utils/Clock.h"


class AnimatedObject : public RenderObject
{
public:
	AnimatedObject(TiXmlDocument doc, AnimatedObject* basis);
	virtual AnimatedObject* getCopy();
	virtual AnimatedObject* getCopy(bool preserveTransforms, bool preserveChildren);
	virtual void setDefaultValues(bool preserveTransforms, bool preserveChildren);
	virtual void loadColladaData(ColladaData& colladaData);
	virtual ~AnimatedObject(void);

	virtual void render();
	virtual void update();

	JointObject* rootJoint;
	std::vector<JointObject*> jointList;


	/*------------------------------
			Animation classes
	-------------------------------*/

	struct Animation
	{
		//stores the times at which key frames happen
		std::vector<float> keyframes;

		virtual void animate(AnimatedObject* actor, int keyframeCeil, float time){};
		//called every update function. Gets the current keyframe based on time
		void applyAnimation(AnimatedObject* actor, float time);

		Animation();
	};

	/*---------------------------------------------
			Translation animation (x, y, or z)
	-----------------------------------------------*/

	struct TranslateAnimation : public Animation
	{
		// component = 0 -> x translation
		// component = 1 -> y translation
		// component = 2 -> z translation

		int component;
		std::vector<float> values;

		virtual void animate(AnimatedObject* actor, int keyframeCeil, float time);
		TranslateAnimation();
	};

	/*---------------------------------------------------
			Rotation animation (quaternion based)
	----------------------------------------------------*/

	struct RotateAnimation : public Animation
	{
		// component = 0 -> x-component of axis of rotation
		// component = 1 -> y-component of axis of rotation
		// component = 2 -> z-component of axis of rotation
		// component = 3 -> angle

		int component;
		glm::vec3 axis;
		float angle;
		std::vector<float> values;

		virtual void animate(AnimatedObject* actor, int keyframeCeil, float time);

		RotateAnimation();
	};


	/*------------------------------------------------------------
			Joint animation (quaternion slerp)
	------------------------------------------------------------*/

	struct JointAnimation : public Animation
	{

		int jointIndex;
		std::vector<glm::fquat> orientations;
		std::vector<glm::vec3> translations;

		virtual void animate(AnimatedObject* actor, int keyframeCeil, float time);
		JointAnimation();
	};


	/*------------------------------------------------------------
			Animation clip holds various animations together
	------------------------------------------------------------*/

	struct AnimationClip
	{
		bool isLoop;
		float duration;

		std::vector<Animation*> animations;
		void continueAnimation(AnimatedObject* actor, float time);
	};

	AnimationClip animationClip;

	PausableClock animationClock;
	bool animationClipPlaying;
	void startAnimationClip();
	void pauseAnimationClip();
	void resumeAnimationClip();
	bool isAnimationInProgress();

};

