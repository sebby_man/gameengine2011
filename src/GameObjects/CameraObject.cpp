#include "CameraObject.h"
#include "../Factory.h"

CameraObject::CameraObject(TiXmlDocument doc, CameraObject* basis) : GameObject(doc, basis == 0 ? Factory::gameObject : basis)
{
	
	if(basis != 0)
	{
		//nothign to copy over
	}

	// See if this class has been overriden in the XML. If so, extract and set those variables
	TiXmlHandle docHandle(&doc);
	TiXmlElement* mainElement = docHandle.FirstChild("CameraObject").ToElement();
	bool overridenInXML = (mainElement != 0);
	if(overridenInXML)
	{
		//nothign to set
	}

}

void CameraObject::setDefaultValues(bool preserveTransforms, bool preserveChildren)
{
	GameObject::setDefaultValues(preserveTransforms, preserveChildren);
}


void CameraObject::update()
{
	this->cameraPos = this->getPosition();
	GameObject::update();
}


//Matrix calculations
void CameraObject::updateWorldToCameraMatrix(void)
{
    this->CalcMatrix();
    Singleton<GL_ShaderState>::Instance()->setWorldToCamera(this->worldToCameraMatrix);
}

void CameraObject::CalcLookAtMatrix()
{
    glm::vec3 rightDir = glm::normalize(glm::cross(this->lookDir, this->upDir));
    glm::vec3 perpUpDir = glm::cross(rightDir, this->lookDir);

    glm::mat4 rotMat(1.0f);
    rotMat[0] = glm::vec4(rightDir, 0.0f);
    rotMat[1] = glm::vec4(perpUpDir, 0.0f); 
    rotMat[2] = glm::vec4(-this->lookDir, 0.0f);

    rotMat = glm::transpose(rotMat);

    glm::mat4 transMat(1.0f);
    transMat[3] = glm::vec4(-this->cameraPos, 1.0f);

    this->worldToCameraMatrix = rotMat * transMat;
}

void CameraObject::activate()
{
	//When starting to use a new camera, call this method
	//this->updateWorldToCameraMatrix();
}

//Getters
glm::mat4 CameraObject::getWorldToCameraMatrix()
{
	return this->worldToCameraMatrix;
}
glm::vec3 CameraObject::getCameraPos(void)
{
	return this->cameraPos;
}
glm::vec3 CameraObject::getLookDir(void)
{
	return this->lookDir;
}
glm::vec3 CameraObject::getUpDir(void)
{
	return this->upDir;
}



CameraObject::~CameraObject(void)
{
	//Do nothing
}