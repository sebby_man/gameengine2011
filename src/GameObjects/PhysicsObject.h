#pragma once

#include <bullet/btBulletDynamicsCommon.h>
#include <glm/glm.hpp>

#include "GameObject.h"
#include "../Physics/PhysicsPrimitives.h"
#include "../Physics/PhysicsUtils.h"

/*--------------------------------------
PhysicsObject has physics properties for
integration with Bullet3D.
--------------------------------------*/
class PhysicsObject : public GameObject
{
public:

	//Constructors
	PhysicsObject(TiXmlDocument doc, PhysicsObject* basis);
	virtual PhysicsObject* getCopy();
	virtual PhysicsObject* getCopy(bool preserveTransforms, bool preserveChildren);
	virtual void setDefaultValues(bool preserveTransforms, bool preserveChildren);
	virtual ~PhysicsObject(void);

	virtual void update();
	
	PrimitiveData* primitiveData;
	btRigidBody* rigidBody;

	//Setters
	void setMass(float mass);
	void setRestitution(float restitution);
	void setFriction(float friction);

	//Getters
	float getMass();
	float getRestitution();
	float getFriction();

	//Transformation methods
	btTransform getBulletTransformationMatrix();
	virtual void translateX(float amount, bool personal);
    virtual void translateY(float amount, bool personal);
    virtual void translateZ(float amount, bool personal);
    virtual void translate(glm::vec3 vector, bool personal);
    virtual void setTranslationX(float amount, bool personal);
    virtual void setTranslationY(float amount, bool personal);
    virtual void setTranslationZ(float amount, bool personal);
    virtual void setTranslation(glm::vec3 vector, bool personal);
	virtual glm::vec3 getPosition();

    virtual void scaleX(float amount, bool personal);
    virtual void scaleY(float amount, bool personal);
    virtual void scaleZ(float amount, bool personal);
    virtual void scale(float amount, bool personal);
    virtual void scale(glm::vec3 vector, bool personal);
    virtual void scaleFromPoint(float amount, glm::vec3 scalePoint, bool personal);
    virtual void setScale(float amount, bool personal);
    virtual void setScale(glm::vec3 vector, bool personal);

    virtual void yaw(float angle, bool personal);
    virtual void pitch(float angle, bool personal);
    virtual void roll(float angle, bool personal);
    virtual void rotate(glm::vec3 axis, float angle, bool personal);
    virtual void setRotation(glm::vec3 axis, float angle, bool personal);

protected:
	void createRigidBody(float mass, float friction, float restitution);

	virtual void updateFinalTransforms();
    virtual void updateTransforms(bool personal);
};

