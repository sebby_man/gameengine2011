#include "RenderObject.h"
#include "../Loader.h"
#include "../ColladaData.h"

RenderObject::RenderObject(TiXmlDocument doc, RenderObject* basis) : GameObject(doc, basis == 0 ? Factory::gameObject : basis)
{
	visible = true;
	loaded = false;

	if(basis != 0)
	{
		mesh = basis->mesh;
		material = basis->material;
		shaderProgram = basis->shaderProgram;
	}

	// See if this class has been overriden in the XML. If so, extract and set those variables
	TiXmlHandle docHandle(&doc);
	TiXmlElement* mainElement = docHandle.FirstChild("RenderObject").ToElement();
	bool overridenInXML = (mainElement != 0);
	if(overridenInXML)
	{
		//set "mesh"
		TiXmlElement* meshElement = mainElement->FirstChildElement("mesh");
		if(meshElement != 0)
		{
			meshName = meshElement->FirstChild()->Value();
			Singleton<Loader>::Instance()->meshToRenderObjectsMap[meshName].push_back(this);
		}

		//set "material"
		TiXmlElement* materialElement = mainElement->FirstChildElement("material");
		if(materialElement != 0)
		{
			std::string materialName = materialElement->FirstAttribute()->Value();
			material = Singleton<Loader>::Instance()->getMaterialByName(materialName);
			Singleton<Loader>::Instance()->fillMaterialBlock(materialElement, material);
		}
		
		//set "shaderProgram"
		TiXmlElement* shaderElement = mainElement->FirstChildElement("shader");
		if(shaderElement != 0)
		{
			std::string shaderName = shaderElement->FirstChild()->Value();
			shaderProgram = Singleton<GL_ShaderState>::Instance()->findShaderProgram(shaderName);
		}
	}
}

void RenderObject::loadColladaData(ColladaData& colladaData)
{
	mesh = colladaData.meshPrimitive;
	this->material.getColorFromTexture = mesh->hasDiffuseColorTexture;
	loaded = true;
}

void RenderObject::setDefaultValues(bool preserveTransforms, bool preserveChildren)
{
	GameObject::setDefaultValues(preserveTransforms, preserveChildren);

	//if the underlying mesh hasn't been loaded yet, add this mesh to the list
	if(!loaded)
	{
		Singleton<Loader>::Instance()->meshToRenderObjectsMap[meshName].push_back(this);
	}
}
RenderObject* RenderObject::getCopy()
{
	RenderObject* copy = new RenderObject(*this);
	copy->setDefaultValues(false, false);
	return copy;
}
RenderObject* RenderObject::getCopy(bool preserveTransforms, bool preserveChildren)
{
	//note the copy constructor
	RenderObject* copy = new RenderObject(*this);
	copy->setDefaultValues(preserveTransforms, preserveChildren);
	return copy;
}

RenderObject::~RenderObject(void)
{
}

bool RenderObject::isTransparent()
{
	return mesh->hasAlphaTexture || material.transparency < .995f;
}

void RenderObject::update()
{
	GameObject::update();
	this->render();
}
void RenderObject::render()
{
	if(visible)
	{
		Singleton<GL_ShaderState>::Instance()->setForRender(this->shaderProgram, this->material, this->transformsFinal, this->uniformScaleFinal);
		this->mesh->render();
	}
}

