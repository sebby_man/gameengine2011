#include "PhysicsObject.h"
#include "../Factory.h"

PhysicsObject::PhysicsObject(TiXmlDocument doc, PhysicsObject* basis) : GameObject(doc, basis == 0 ? Factory::gameObject : basis)
{
	//Set initial values
	this->rigidBody = 0;

	float mass;
	float restitution;
	float friction;

	if(basis != 0)
	{
		mass = basis->getMass();
		restitution = basis->getRestitution();
		friction = basis->getFriction();
		primitiveData = basis->primitiveData;
	}

	// See if this class has been overriden in the XML. If so, extract and set those variables
	TiXmlHandle docHandle(&doc);
	TiXmlElement* mainElement = docHandle.FirstChild("PhysicsObject").ToElement();
	bool overridenInXML = (mainElement != 0);
	if(overridenInXML)
	{
		//set "mass"
		TiXmlElement* massElement = mainElement->FirstChildElement("mass");
		if(massElement != 0)
		{
			std::string massFromXML = massElement->FirstChild()->Value();
			mass = Utils::stringToFloat(massFromXML);
		}

		//set "restitution"
		TiXmlElement* restitutionElement = mainElement->FirstChildElement("restitution");
		if(restitutionElement != 0)
		{
			std::string restitutionFromXML = restitutionElement->FirstChild()->Value();
			restitution = Utils::stringToFloat(restitutionFromXML);
		}

		//set "friction"
		TiXmlElement* frictionElement = mainElement->FirstChildElement("friction");
		if(frictionElement != 0)
		{
			std::string frictionFromXML = frictionElement->FirstChild()->Value();
			friction = Utils::stringToFloat(frictionFromXML);
		}

		//set "primitiveData"
		TiXmlElement* primitiveElement = mainElement->FirstChildElement("primitive");
		if(primitiveElement != 0)
		{
			std::string primitiveType = primitiveElement->Attribute("type");
			if(primitiveType == "sphere")
			{
				float radius = 0;

				TiXmlElement* radiusElement = primitiveElement->FirstChildElement("radius");
				if(radiusElement != 0)
				{
					std::string radiusFromXML = radiusElement->FirstChild()->Value();
					radius = Utils::stringToFloat(radiusFromXML);
				}
				else
					std::cout << "No radius found!" << std::endl;

				this->primitiveData = new PrimitiveDataSphere(radius);
			}
			else if(primitiveType == "box")
			{
				float x;
				float y;
				float z;

				//x
				TiXmlElement* xElement = primitiveElement->FirstChildElement("x");
				if(xElement != 0)
				{
					std::string xFromXML = xElement->FirstChild()->Value();
					x = Utils::stringToFloat(xFromXML);
				}
				else
					std::cout << "No x dimension found!" << std::endl;

				//y
				TiXmlElement* yElement = primitiveElement->FirstChildElement("y");
				if(yElement != 0)
				{
					std::string yFromXML = yElement->FirstChild()->Value();
					y = Utils::stringToFloat(yFromXML);
				}
				else
					std::cout << "No y dimension found!" << std::endl;

				//z
				TiXmlElement* zElement = primitiveElement->FirstChildElement("z");
				if(zElement != 0)
				{
					std::string zFromXML = zElement->FirstChild()->Value();
					z = Utils::stringToFloat(zFromXML);
				}
				else
					std::cout << "No z dimension found!" << std::endl;

				glm::vec3 dimensions = glm::vec3(x,y,z);
				this->primitiveData = new PrimitiveDataBox(dimensions);
			}
		}
	}
	this->createRigidBody(mass,friction,restitution);
}

void PhysicsObject::setDefaultValues(bool preserveTransforms, bool preserveChildren)
{
	GameObject::setDefaultValues(preserveTransforms, preserveChildren);
	primitiveData = this->primitiveData->getCopy();
	createRigidBody(this->getMass(),this->getFriction(),this->getRestitution());
}
PhysicsObject* PhysicsObject::getCopy()
{
	PhysicsObject* copy = new PhysicsObject(*this);
	copy->setDefaultValues(false, false);
	return copy;
}
PhysicsObject* PhysicsObject::getCopy(bool preserveTransforms, bool preserveChildren)
{
	//note the copy constructor
	PhysicsObject* copy = new PhysicsObject(*this);
	copy->setDefaultValues(preserveTransforms, preserveChildren);
	return copy;
}

PhysicsObject::~PhysicsObject(void)
{
	//Do nothing
}

void PhysicsObject::createRigidBody(float mass, float friction, float restitution)
{
	bool isDynamic = (mass != 0.f);
	btCollisionShape* collisionShape = this->primitiveData->collisionShape;

	btVector3 localInertia(0,0,0);
	if (isDynamic)
		collisionShape->calculateLocalInertia(mass,localInertia);

	//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
	btDefaultMotionState* myMotionState = new btDefaultMotionState(btTransform::getIdentity());
	btRigidBody::btRigidBodyConstructionInfo cInfo(mass,myMotionState,collisionShape,localInertia);
	this->rigidBody = new btRigidBody(cInfo);
	this->rigidBody->setContactProcessingThreshold(BT_LARGE_FLOAT);
	this->rigidBody->setFriction(friction);
	this->rigidBody->setRestitution(restitution);
	this->rigidBody->setCcdMotionThreshold(.1f);
	this->rigidBody->setCcdSweptSphereRadius(.1f);
}

void PhysicsObject::update()
{
	btVector3 btScaleAmount = this->rigidBody->getCollisionShape()->getLocalScaling();
	glm::mat4 scaleMat = glm::mat4();
	scaleMat[0][0] = btScaleAmount.getX();
	scaleMat[1][1] = btScaleAmount.getY();
	scaleMat[2][2] = btScaleAmount.getZ();

	//TODO: physics object tranforms are not synched with its parents transforms. This is possible to do, but we have to determine if its worth it.
	btTransform bulletTransform = this->getBulletTransformationMatrix();
	this->transformsFinal = Utils::convertBulletTransformToGLM(bulletTransform) * scaleMat;
	updateTransforms(false);

	GameObject::update();
}

/*----------------------------------------
			    Setters
----------------------------------------*/
void PhysicsObject::setMass(float mass)
{
	this->rigidBody->setMassProps(mass,btVector3(0,0,0));
}
void PhysicsObject::setRestitution(float restitution)
{
	this->rigidBody->setRestitution(restitution);
}
void PhysicsObject::setFriction(float friction)
{
	this->rigidBody->setFriction(friction);
}	

/*----------------------------------------
			    Setters
----------------------------------------*/
float PhysicsObject::getMass()
{
	return this->rigidBody->getInvMass();
}
float PhysicsObject::getRestitution()
{
	return this->rigidBody->getRestitution();
}
float PhysicsObject::getFriction()
{
	return this->rigidBody->getFriction();
}


/*----------------------------------------
			Transformations
----------------------------------------*/
btTransform PhysicsObject::getBulletTransformationMatrix()
{
	return this->rigidBody->getWorldTransform();
}
//Translation
void PhysicsObject::translateX(float amount, bool personal)
{
	btTransform bulletTransform = this->rigidBody->getWorldTransform();
	btVector3 position = bulletTransform.getOrigin();
	position += btVector3(amount,0,0);
	bulletTransform.setOrigin(position);
	this->rigidBody->setWorldTransform(bulletTransform);
	this->updateTransforms(personal);
}
void PhysicsObject::translateY(float amount, bool personal)
{
    btTransform bulletTransform = this->rigidBody->getWorldTransform();
	btVector3 position = bulletTransform.getOrigin();
	position += btVector3(0,amount,0);
	bulletTransform.setOrigin(position);
	this->rigidBody->setWorldTransform(bulletTransform);
	this->updateTransforms(personal);
}
void PhysicsObject::translateZ(float amount, bool personal)
{
    btTransform bulletTransform = this->rigidBody->getWorldTransform();
	btVector3 position = bulletTransform.getOrigin();
	position += btVector3(0,0,amount);
	bulletTransform.setOrigin(position);
	this->rigidBody->setWorldTransform(bulletTransform);
	this->updateTransforms(personal);
}
void PhysicsObject::translate(glm::vec3 vector, bool personal)
{
    btTransform bulletTransform = this->rigidBody->getWorldTransform();
	btVector3 position = bulletTransform.getOrigin();
	position += btVector3(vector.x,vector.y,vector.z);
	bulletTransform.setOrigin(position);
	this->rigidBody->setWorldTransform(bulletTransform);
	this->updateTransforms(personal);
}
void PhysicsObject::setTranslationX(float amount, bool personal)
{
    btTransform bulletTransform = this->rigidBody->getWorldTransform();
	btVector3 position = bulletTransform.getOrigin();
	position += btVector3(amount,0,0);
	bulletTransform.setOrigin(position);
	this->rigidBody->setWorldTransform(bulletTransform);
	this->updateTransforms(personal);
}
void PhysicsObject::setTranslationY(float amount, bool personal)
{
    btTransform bulletTransform = this->rigidBody->getWorldTransform();
	btVector3 position = bulletTransform.getOrigin();
	position += btVector3(0,amount,0);
	bulletTransform.setOrigin(position);
	this->rigidBody->setWorldTransform(bulletTransform);
	this->updateTransforms(personal);
}
void PhysicsObject::setTranslationZ(float amount, bool personal)
{
    btTransform bulletTransform = this->rigidBody->getWorldTransform();
	btVector3 position = bulletTransform.getOrigin();
	position += btVector3(0,0,amount);
	bulletTransform.setOrigin(position);
	this->rigidBody->setWorldTransform(bulletTransform);
	this->updateTransforms(personal);
}
void PhysicsObject::setTranslation(glm::vec3 vector, bool personal)
{
	btTransform bulletTransform = this->rigidBody->getWorldTransform();
	btVector3 position = btVector3(vector.x,vector.y,vector.z);
	bulletTransform.setOrigin(position);
	this->rigidBody->setWorldTransform(bulletTransform);
	this->updateTransforms(personal);
}
glm::vec3 PhysicsObject::getPosition()
{
    return glm::vec3(transformsFinal[3]);
}
//Scale
void PhysicsObject::scaleX(float amount, bool personal)
{
    btVector3 scaleAmount = this->rigidBody->getCollisionShape()->getLocalScaling();
	scaleAmount += btVector3(amount,0,0);
	this->rigidBody->getCollisionShape()->setLocalScaling(scaleAmount);
	this->updateTransforms(personal);
}
void PhysicsObject::scaleY(float amount, bool personal)
{
    btVector3 scaleAmount = this->rigidBody->getCollisionShape()->getLocalScaling();
	scaleAmount += btVector3(0,amount,0);
	this->rigidBody->getCollisionShape()->setLocalScaling(scaleAmount);
	this->updateTransforms(personal);
}
void PhysicsObject::scaleZ(float amount, bool personal)
{
    btVector3 scaleAmount = this->rigidBody->getCollisionShape()->getLocalScaling();
	scaleAmount += btVector3(0,0,amount);
	this->rigidBody->getCollisionShape()->setLocalScaling(scaleAmount);
	this->updateTransforms(personal);
}
void PhysicsObject::scale(float amount, bool personal)
{
    btVector3 scaleAmount = this->rigidBody->getCollisionShape()->getLocalScaling();
	scaleAmount += btVector3(amount,amount,amount);
	this->rigidBody->getCollisionShape()->setLocalScaling(scaleAmount);
	this->updateTransforms(personal);
}
void PhysicsObject::scale(glm::vec3 vector, bool personal)
{
	btVector3 scaleAmount = this->rigidBody->getCollisionShape()->getLocalScaling();
	scaleAmount += btVector3(vector.x,vector.y,vector.z);
	this->rigidBody->getCollisionShape()->setLocalScaling(scaleAmount);
	this->updateTransforms(personal);
}
void PhysicsObject::setScale(float amount, bool personal)
{
    this->rigidBody->getCollisionShape()->setLocalScaling(btVector3(amount,amount,amount));
	this->updateTransforms(personal);
}
void PhysicsObject::setScale(glm::vec3 vector, bool personal)
{
    this->rigidBody->getCollisionShape()->setLocalScaling(btVector3(vector.x,vector.y,vector.z));
	this->updateTransforms(personal);
}
void PhysicsObject::scaleFromPoint(float amount, glm::vec3 scalePoint, bool personal)
{
    //Save for later
}
//Rotation
void PhysicsObject::yaw(float angle, bool personal)
{
	btTransform bulletTransform = this->rigidBody->getWorldTransform();
	btQuaternion oldOrientation = bulletTransform.getRotation();
	
	btQuaternion newOrientation(btVector3(0,1,0),angle);
	newOrientation = oldOrientation + newOrientation;

	bulletTransform.setRotation(newOrientation);
	this->rigidBody->setWorldTransform(bulletTransform);
	this->updateTransforms(personal);
}
void PhysicsObject::pitch(float angle, bool personal)
{
    btTransform bulletTransform = this->rigidBody->getWorldTransform();
	btQuaternion oldOrientation = bulletTransform.getRotation();
	
	btQuaternion newOrientation(btVector3(1,0,0),angle);
	newOrientation = oldOrientation + newOrientation;

	bulletTransform.setRotation(newOrientation);
	this->rigidBody->setWorldTransform(bulletTransform);
	this->updateTransforms(personal);
}
void PhysicsObject::roll(float angle, bool personal)
{
    btTransform bulletTransform = this->rigidBody->getWorldTransform();
	btQuaternion oldOrientation = bulletTransform.getRotation();
	
	btQuaternion newOrientation(btVector3(0,0,1),angle);
	newOrientation = oldOrientation + newOrientation;

	bulletTransform.setRotation(newOrientation);
	this->rigidBody->setWorldTransform(bulletTransform);
	this->updateTransforms(personal);
}
void PhysicsObject::rotate(glm::vec3 axis, float angle, bool personal)
{
    btTransform bulletTransform = this->rigidBody->getWorldTransform();
	btQuaternion oldOrientation = bulletTransform.getRotation();
	
	axis = glm::normalize(axis);
	btQuaternion newOrientation(btVector3(axis.x,axis.y,axis.z),angle);
	newOrientation = oldOrientation + newOrientation;

	bulletTransform.setRotation(newOrientation);
	this->rigidBody->setWorldTransform(bulletTransform);
	this->updateTransforms(personal);
}
void PhysicsObject::setRotation(glm::vec3 axis, float angle, bool personal)
{
    btTransform bulletTransform = this->rigidBody->getWorldTransform();
	axis = glm::normalize(axis);
	btQuaternion newOrientation(btVector3(axis.x,axis.y,axis.z),angle);
	bulletTransform.setRotation(newOrientation);
	this->rigidBody->setWorldTransform(bulletTransform);
	this->updateTransforms(personal);
}

//Update transformations
void PhysicsObject::updateTransforms(bool personal)
{
	uniformScaleWithHierarchy = true;
	this->transformsScene.setTransformationMatrix(this->transformsFinal);
	SceneTraversers::TransformsUpdater transformUpdateAction(this);
	transformUpdateAction.traverse(this);
}
void PhysicsObject::updateFinalTransforms()
{
    //Since there is no transformsPersonal, do nothing
}
