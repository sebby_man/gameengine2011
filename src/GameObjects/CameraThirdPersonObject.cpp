#include "CameraThirdPersonObject.h"
#include "../Factory.h"

CameraThirdPersonObject::CameraThirdPersonObject(TiXmlDocument doc, CameraThirdPersonObject* basis) : CameraObject(doc, basis == 0 ? Factory::cameraObject : basis)
{
	init();
	if(basis != 0)
	{
		//nothign to copy over
	}

	// See if this class has been overriden in the XML. If so, extract and set those variables
	TiXmlHandle docHandle(&doc);
	TiXmlElement* mainElement = docHandle.FirstChild("CameraThirdPersonObject").ToElement();
	bool overridenInXML = (mainElement != 0);
	if(overridenInXML)
	{
		//nothign to set
	}

}

void CameraThirdPersonObject::init()
{
	this->currXZRads = 0.0f;
    this->currYRads = 0.0f;
    this->radius = 10;
    this->lookAt = glm::vec3(0,0,0);
	this->type = THIRD_PERSON;
    //this->updateWorldToCameraMatrix();
}

void CameraThirdPersonObject::setDefaultValues(bool preserveTransforms, bool preserveChildren)
{
	this->init();
	CameraObject::setDefaultValues(preserveTransforms, preserveChildren);
}

CameraThirdPersonObject* CameraThirdPersonObject::getCopy()
{
	CameraThirdPersonObject* copy = new CameraThirdPersonObject(*this);
	copy->setDefaultValues(false, false);
	return copy;
}

CameraThirdPersonObject* CameraThirdPersonObject::getCopy(bool preserveTransforms, bool preserveChildren)
{
	//note the copy constructor
	CameraThirdPersonObject* copy = new CameraThirdPersonObject(*this);
	copy->setDefaultValues(preserveTransforms, preserveChildren);
	return copy;
}


//-----------------------------
// Camera Movements ///////////
//-----------------------------
void CameraThirdPersonObject::pan(float x, float y)
{
	glm::vec3 right = glm::normalize(glm::cross(this->lookDir,this->upDir));
	glm::vec3 up = this->upDir;
	glm::vec3 moveX = x*right;
	glm::vec3 moveY = y*up;
	this->lookAt += moveX;
	this->lookAt += moveY;
	//this->updateWorldToCameraMatrix();
}
void CameraThirdPersonObject::rotate(float x, float y)
{
	this->currXZRads += x;
	this->currYRads += y;
	//this->updateWorldToCameraMatrix();
}
void CameraThirdPersonObject::zoom(float distance)
{
	this->radius -= distance;
	//this->updateWorldToCameraMatrix();
}

void CameraThirdPersonObject::CalcMatrix(void)
{
    //Compute the position vector along the xz plane.
    float cosa = cosf(currXZRads);
    float sina = sinf(currXZRads);

    glm::vec3 currPos(sina, 0.0f, cosa);

    //Compute the "up" rotation axis.
    //This axis is a 90 degree rotation around the y axis. Just a component-swap and negate.
    glm::vec3 UpRotAxis;

    UpRotAxis.x = currPos.z;
    UpRotAxis.y = currPos.y;
    UpRotAxis.z = -currPos.x;

    //Now, rotate around this axis by the angle.
    //Framework::MatrixStack theStack;

    //theStack.SetIdentity();
    glm::mat4 xRotation = Transformations::getRotationMatrixRads(UpRotAxis, currYRads);
    currPos = glm::vec3(xRotation * glm::vec4(currPos, 0.0));

    //Set the position of the camera.
    glm::vec3 tempVec = currPos * float(radius);
    this->cameraPos = tempVec + lookAt;

    //Now, compute the up-vector.
    //The direction of the up-vector is the cross-product of currPos and UpRotAxis.
    //Rotate this vector around the currPos axis given m_currSpin.

    this->upDir = glm::normalize(glm::cross(currPos, UpRotAxis));
    this->lookDir = glm::normalize(this->lookAt - this->cameraPos);

    this->CalcLookAtMatrix();
}

//Getters
glm::vec3 CameraThirdPersonObject::getLookAt(void)
{
	return this->lookAt;
}

void CameraThirdPersonObject::update()
{
	CameraObject::update();
}

CameraThirdPersonObject::~CameraThirdPersonObject(void)
{
	//nothing yet
}