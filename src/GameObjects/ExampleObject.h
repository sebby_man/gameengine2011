#pragma once
#include "GameObject.h"

class ExampleObject : public GameObject
{
public:
	
	std::string exampleString;
	
	ExampleObject(TiXmlDocument doc, ExampleObject* basis);
	virtual ExampleObject* getCopy();
	virtual ExampleObject* getCopy(bool preserveTransforms, bool preserveChildren);
	virtual void setDefaultValues(bool preserveTransforms, bool preserveChildren);
	virtual ~ExampleObject(void);
	virtual void update();
};

