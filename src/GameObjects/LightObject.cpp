#include "LightObject.h"
#include "../Factory.h"

LightObject::LightObject(TiXmlDocument doc, LightObject* basis) : GameObject(doc, basis == 0 ? Factory::gameObject : basis)
{
	
	if(basis != 0)
	{
		this->rgb = basis->rgb;
		this->lightAttenuation = basis->lightAttenuation;
	}

	// See if this class has been overriden in the XML. If so, extract and set those variables
	TiXmlHandle docHandle(&doc);
	TiXmlElement* mainElement = docHandle.FirstChild("LightObject").ToElement();
	bool overridenInXML = (mainElement != 0);
	if(overridenInXML)
	{
		//set "rgb"
		TiXmlElement* rgbElement = mainElement->FirstChildElement("rgb");
		if(rgbElement != 0)
		{
			std::string rgbData = rgbElement->FirstChild()->Value();
			std::vector<std::string> rgbValues = Utils::parseSpaceSeparatedString(rgbData);
			rgb.x = Utils::stringToFloat(rgbValues[0]);
			rgb.y = Utils::stringToFloat(rgbValues[1]);
			rgb.z = Utils::stringToFloat(rgbValues[2]);
		}

		//set light attenuation
		TiXmlElement* lightAttenuationElement = mainElement->FirstChildElement("half_distance");
		if(lightAttenuationElement != 0)
		{
			std::string halfDistanceStr = lightAttenuationElement->FirstChild()->Value();
			float halfDistance = Utils::stringToFloat(halfDistanceStr);
			this->lightAttenuation = 1.0f / (halfDistance * halfDistance);
		}

	}

}

void LightObject::setDefaultValues(bool preserveTransforms, bool preserveChildren)
{
	GameObject::setDefaultValues(preserveTransforms, preserveChildren);
}
LightObject* LightObject::getCopy()
{
	LightObject* copy = new LightObject(*this);
	copy->setDefaultValues(false, false);
	return copy;
}

LightObject* LightObject::getCopy(bool preserveTransforms, bool preserveChildren)
{
	//note the copy constructor
	LightObject* copy = new LightObject(*this);
	copy->setDefaultValues(preserveTransforms, preserveChildren);
	return copy;
}

LightObject::~LightObject(void)
{
}


void LightObject::update()
{
	GameObject::update();
}