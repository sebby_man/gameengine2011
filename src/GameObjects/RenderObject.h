#pragma once
#include "GameObject.h"
#include "../Rendering/GL_MeshPrimitive.h"
#include "../Rendering/GL_ShaderState.h"

class ColladaData;

/*-----------------------------------------
RenderObject can be rendered with OpenGL
-----------------------------------------*/

class RenderObject : public GameObject
{
public:

	//Constructors
	RenderObject(TiXmlDocument doc, RenderObject* basis);
	virtual RenderObject* getCopy();
	virtual RenderObject* getCopy(bool preserveTransforms, bool preserveChildren);
	virtual void setDefaultValues(bool preserveTransforms, bool preserveChildren);
	virtual void loadColladaData(ColladaData& colladaData);
	virtual ~RenderObject(void);

	virtual void update();
	virtual void render();

	bool isTransparent();

	//Values
	std::string meshName;
	GL_MeshPrimitive* mesh;
	MaterialBlock material;
	GL_ShaderProgram* shaderProgram;
	bool visible;
	bool loaded;
protected:


};

