#pragma once
#include "GameObject.h"
#include <glm/glm.hpp>
#include <iostream>
#include "../Singleton.h"
#include "../Rendering/GL_ShaderState.h"

class CameraObject : public GameObject
{
public:
	

	CameraObject(TiXmlDocument doc, CameraObject* basis);
	//no copy, Camera is abstract
	virtual void setDefaultValues(bool preserveTransforms, bool preserveChildren);
	virtual ~CameraObject(void);
	virtual void update();

	enum Type
	{
		FIRST_PERSON,
		THIRD_PERSON
	};
	Type type;

	virtual void rotate(float x, float y){};
	virtual void zoom(float delta){};
	virtual void pan(float x, float y){};
	virtual void activate();

    glm::mat4 getWorldToCameraMatrix(void);
    glm::vec3 getCameraPos(void);
    glm::vec3 getLookDir(void);
    glm::vec3 getUpDir(void);

	void updateWorldToCameraMatrix(void);

protected:

    glm::mat4 worldToCameraMatrix;

	
	void CalcLookAtMatrix();
	virtual void CalcMatrix(void){};

	glm::vec3 upDir;
	glm::vec3 lookDir;
	glm::vec3 cameraPos;

};
