#pragma once
#include "GameObject.h"

class AnimatedObject;

class JointObject : public GameObject
{
public:

	int index;
	glm::mat4 invBindPoseMatrix;

	JointObject(TiXmlDocument doc, JointObject* basis);
	
	//special copy used for animated objects. This is a joint-only function, which is why there is no virtual keyword
	
	
	virtual JointObject* getCopy();
	virtual JointObject* getCopy(bool preserveTransforms, bool preserveChildren);
	virtual JointObject* getCopy(bool preserveTransforms, bool preserveChildren, AnimatedObject* controller);
	virtual void setDefaultValues(bool preserveTransforms, bool preserveChildren);
	virtual void setDefaultValues(bool preserveTransforms, bool preserveChildren, AnimatedObject* controller);
	virtual ~JointObject(void);
};

