#pragma once

#include <tinyxml/tinyxml.h>
#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include "../Transformations.h"
#include "../SceneTraversal.h"
#include "../Singleton.h"

/*------------------------------------------------------------
GameObject is the top-level object class for game components
In a way, it acts like a node in a scene graph. It handles basic
transformation. Subclasses handle more complicated functionality.
------------------------------------------------------------*/

class GameObject
{
public:
	//TODO: This needs to be fixed somehow
	friend struct SceneTraversers::TraversalAction;
	friend struct SceneTraversers::Updater;
	friend struct SceneTraversers::AddGameObject;
	friend struct SceneTraversers::TransformsUpdater;
	friend struct SceneTraversers::Finder;
	friend struct SceneTraversers::Deleter;

	//Constructors
	GameObject(TiXmlDocument doc, GameObject* basis);
	virtual GameObject* getCopy();
	virtual GameObject* getCopy(bool preserveTransforms, bool preserveChildren);
	virtual void setDefaultValues(bool preserveTransforms, bool preserveChildren);

	virtual ~GameObject(void);

	virtual void update();
	void setName(std::string name);
	std::string getName();

	//Scene graph stuff
	bool isRoot();
	void setNewParent(GameObject* newParent);
	void removeChild(GameObject* childToRemove);
	void removeChildren();

	//Transformation methods
	glm::mat4 getTransformation();

	void translateX(float amount, bool personal);
    void translateY(float amount, bool personal);
    void translateZ(float amount, bool personal);
    void translate(glm::vec3 vector, bool personal);
    void setTranslationX(float amount, bool personal);
    void setTranslationY(float amount, bool personal);
    void setTranslationZ(float amount, bool personal);
    void setTranslation(glm::vec3 vector, bool personal);
	glm::vec3 getPosition();

    void scaleX(float amount, bool personal);
    void scaleY(float amount, bool personal);
    void scaleZ(float amount, bool personal);
    void scale(float amount, bool personal);
    void scale(glm::vec3 vector, bool personal);
    void scaleFromPoint(float amount, glm::vec3 scalePoint, bool personal);
    void setScale(float amount, bool personal);
    void setScale(glm::vec3 vector, bool personal);

    void yaw(float angle, bool personal);
    void pitch(float angle, bool personal);
    void roll(float angle, bool personal);
    void rotate(glm::vec3 axis, float angle, bool personal);
    void setRotation(glm::vec3 axis, float angle, bool personal);
	void setRotation(glm::fquat rotation, bool personal);
	void setRotation(glm::mat4 rotation, bool personal);

protected:

	GameObject* attachedObject;

	std::vector<GameObject*> childObjects;
	GameObject* parent;
	std::string name;

    void updateFinalTransforms();
    void updateTransforms(bool personal);

    Transformations transformsScene;
    Transformations transformsPersonal;

    glm::mat4 transformsWithHierarchy;
    glm::mat4 transformsFinal;
    bool uniformScaleWithHierarchy;
    bool uniformScaleFinal;

	bool hidden;
};

