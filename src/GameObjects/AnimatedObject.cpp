#include "AnimatedObject.h"
#include "../Factory.h"
#include "../ColladaData.h"

AnimatedObject::AnimatedObject(TiXmlDocument doc, AnimatedObject* basis) : RenderObject(doc, basis == 0 ? Factory::renderObject : basis)
{
	rootJoint = 0;
	animationClipPlaying = false;

	if(basis != 0)
	{
		
	}

	// See if this class has been overriden in the XML. If so, extract and set those variables
	TiXmlHandle docHandle(&doc);
	TiXmlElement* mainElement = docHandle.FirstChild("AnimatedObject").ToElement();
	bool overridenInXML = (mainElement != 0);
	if(overridenInXML)
	{
		//nothing yet
	}

}


void AnimatedObject::loadColladaData(ColladaData& colladaData)
{
	RenderObject::loadColladaData(colladaData);
	
	//fills the joint list with the collada data. However, this data will be overwritten after the root copy during the next step
	this->jointList = colladaData.jointList;
	this->rootJoint = colladaData.rootJoint->getCopy(true, false, this);
	this->rootJoint->setNewParent(this);
	this->animationClip = colladaData.animationClips[0];

}


void AnimatedObject::setDefaultValues(bool preserveTransforms, bool preserveChildren)
{
	animationClipPlaying = false;
	
	if(!loaded) //if its not loaded yet, treat it normally
	{
		RenderObject::setDefaultValues(preserveTransforms, preserveChildren);
	}
	else //if it is loaded, we have to worry about the root joint, and overide GameObject's default copy behavior
	{

		std::vector<GameObject*> copiedChildren = this->childObjects;
		GameObject* prevAttachedObject = this->attachedObject;
		//this will end up copying the attached object
		RenderObject::setDefaultValues(preserveTransforms, false);
		//copy joint hierarchy
		rootJoint = this->rootJoint->getCopy(preserveTransforms, preserveChildren, this);
		rootJoint->setNewParent(this);
		

		//copy all the other children, overriding the copy within GameObject
		for(unsigned int i = 0; i < copiedChildren.size(); i++)
		{
			GameObject* child = copiedChildren[i];
			if(child != this->rootJoint && preserveChildren && child != prevAttachedObject)
			{
				child->getCopy(preserveTransforms, preserveChildren)->setNewParent(this);
			}
		}

		

	}
}

AnimatedObject* AnimatedObject::getCopy()
{
	AnimatedObject* copy = new AnimatedObject(*this);
	copy->setDefaultValues(false, false);
	return copy;
}

AnimatedObject* AnimatedObject::getCopy(bool preserveTransforms, bool preserveChildren)
{
	//note the copy constructor
	AnimatedObject* copy = new AnimatedObject(*this);
	copy->setDefaultValues(preserveTransforms, preserveChildren);
	return copy;
}


void AnimatedObject::render()
{
	if(visible)
	{
		int numJoints = this->jointList.size();
		glm::mat4* jointMatrices = new glm::mat4 [numJoints];
		for(int i = 0; i < numJoints; i++)
		{
			jointMatrices[i] = this->jointList[i]->getTransformation() * this->jointList[i]->invBindPoseMatrix;
		}

		Singleton<GL_ShaderState>::Instance()->setJointMatrices(jointMatrices, numJoints);
	
		RenderObject::render();
	}
}

void AnimatedObject::update()
{
	if(animationClipPlaying)
	{
		float timeElapsed = animationClock.GetElapsedTime();
		if(timeElapsed > animationClip.duration)
		{
			if(animationClip.isLoop)
			{
				this->startAnimationClip();
			}
			else
			{
				this->pauseAnimationClip();
			}
			
		}
		animationClip.continueAnimation(this, timeElapsed);
	}

	RenderObject::update();
}


AnimatedObject::~AnimatedObject(void)
{
}



/*---------------------------------

	ANIMATION CLASSES

----------------------------------*/


AnimatedObject::Animation::Animation()
{
	//nothing
}

AnimatedObject::RotateAnimation::RotateAnimation() : Animation()
{

}

AnimatedObject::TranslateAnimation::TranslateAnimation() : Animation()
{

}

AnimatedObject::JointAnimation::JointAnimation() : Animation()
{

}


void AnimatedObject::Animation::applyAnimation(AnimatedObject* actor, float time)
{
	int numKeyframes = this->keyframes.size();

	//if time is before or after the keyframes in this animation, do not animate
	if(time > keyframes[numKeyframes - 1]) return;
	if(time < keyframes[0]) return;

	for(unsigned int i = 1; i < keyframes.size(); i++)
	{
		if(keyframes[i] > time)
		{
			float startTime = keyframes[i-1];
			float endTime = keyframes[i];
			float timeNormalized = (time - startTime)/(endTime - startTime);

			animate(actor, i, timeNormalized);
			break;
		}
	}
}

void AnimatedObject::TranslateAnimation::animate(AnimatedObject* actor, int keyframeCeil, float time)
{

	float startValue = values[keyframeCeil-1];
	float endValue = values[keyframeCeil];
	float interpolatedValue = startValue + (endValue - startValue)*time;

	if(component == 0)			actor->setTranslationX(interpolatedValue, false);
	else if(component == 1)		actor->setTranslationY(interpolatedValue, false);
	else if(component == 2)		actor->setTranslationZ(interpolatedValue, false);
}

void AnimatedObject::RotateAnimation::animate(AnimatedObject* actor, int keyframeCeil, float time)
{
	float startValue = values[keyframeCeil-1];
	float endValue = values[keyframeCeil];
	float interpolatedValue = startValue + (endValue - startValue)*time;

	if(component == 3)
	{
		//change the rotation
		actor->setRotation(axis, interpolatedValue, false);
	}
	else
	{
		//change the axis (much rarer)
		glm::vec3 newAxis = axis;
		newAxis[component] = interpolatedValue;
		actor->setRotation(newAxis, angle, false);
	}
}

void AnimatedObject::JointAnimation::animate(AnimatedObject* actor, int keyframeCeil, float time)
{
	glm::vec3 startTranslation = translations[keyframeCeil-1];
	glm::vec3 endTranslation = translations[keyframeCeil];
	glm::vec3 interpolatedTranslation = glm::mix(startTranslation, endTranslation, time);
	actor->jointList[this->jointIndex]->setTranslation(interpolatedTranslation, false);

	glm::fquat startOrientation = orientations[keyframeCeil-1];
	glm::fquat endOrientation = orientations[keyframeCeil];

	float dot = glm::dot(startOrientation, endOrientation);
    if (dot > .9995f)
	{
		actor->jointList[this->jointIndex]->setRotation(startOrientation, false);
		return;
	}

	if(dot < 0)
	{
		endOrientation *= -1.0f;
	}
	
	
	glm::fquat interpolatedOrientation = glm::mix(startOrientation, endOrientation, time);
	actor->jointList[this->jointIndex]->setRotation(interpolatedOrientation, false);
	
		
}

void AnimatedObject::AnimationClip::continueAnimation(AnimatedObject* actor, float time)
{
	for(unsigned int i = 0; i < animations.size(); i++)
	{
		animations[i]->applyAnimation(actor, time);
	}
}


bool AnimatedObject::isAnimationInProgress()
{
	return animationClipPlaying;
}

void AnimatedObject::pauseAnimationClip()
{
	animationClipPlaying = false;
	animationClock.Pause();
}

void AnimatedObject::resumeAnimationClip()
{
	animationClock.Start();
	animationClipPlaying = true;
}

void AnimatedObject::startAnimationClip()
{
	this->animationClock.Reset();
	animationClipPlaying = true;
}