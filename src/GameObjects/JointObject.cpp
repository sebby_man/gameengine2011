#include "JointObject.h"
#include "../Factory.h"
#include "AnimatedObject.h"

JointObject::JointObject(TiXmlDocument doc, JointObject* basis) : GameObject(doc, basis == 0 ? Factory::gameObject : basis)
{
	
	if(basis != 0)
	{
		// nothing yet
	}

	// See if this class has been overriden in the XML. If so, extract and set those variables
	TiXmlHandle docHandle(&doc);
	TiXmlElement* mainElement = docHandle.FirstChild("JointObject").ToElement();
	bool overridenInXML = (mainElement != 0);
	if(overridenInXML)
	{
		//nothing to set yet
	}

}


void JointObject::setDefaultValues(bool preserveTransforms, bool preserveChildren, AnimatedObject* controller)
{
	std::vector<GameObject*> copiedChildren = this->childObjects;
	GameObject* prevAttachedObject = this->attachedObject;
	//this will clear the current children as well as add the attachment
	//joints must preserve transforms
	GameObject::setDefaultValues(true, false);

	for(unsigned int i = 0; i < copiedChildren.size(); i++)
	{
		GameObject* child = copiedChildren[i];
		JointObject* childJoint = dynamic_cast<JointObject*>(child);
		if(childJoint != 0)
		{
			//if child is a joint
			childJoint->getCopy(preserveTransforms, preserveChildren, controller)->setNewParent(this);
		}
		else
		{
			//if child is something else
			if(preserveChildren && child != prevAttachedObject)
			{
				child->getCopy(preserveTransforms, preserveChildren)->setNewParent(this);
			}
		}
	}
	//set this joint into the controller's list
	controller->jointList[this->index] = this;
}

void JointObject::setDefaultValues(bool preserveTransforms, bool preserveChildren)
{
	GameObject::setDefaultValues(preserveTransforms, preserveChildren);
}
JointObject* JointObject::getCopy()
{
	JointObject* copy = new JointObject(*this);
	copy->setDefaultValues(true, false);
	return copy;
}
JointObject* JointObject::getCopy(bool preserveTransforms, bool preserveChildren)
{
	//note the copy constructor
	JointObject* copy = new JointObject(*this);
	copy->setDefaultValues(preserveTransforms, preserveChildren);
	return copy;
}
JointObject* JointObject::getCopy(bool preserveTransforms, bool preserveChildren, AnimatedObject* controller)
{
	//note the copy constructor
	JointObject* copy = new JointObject(*this);
	copy->setDefaultValues(preserveTransforms, preserveChildren, controller);
	return copy;
}

JointObject::~JointObject(void)
{
}
