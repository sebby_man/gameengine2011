#include "CameraFirstPersonObject.h"
#include "../Factory.h"

CameraFirstPersonObject::CameraFirstPersonObject(TiXmlDocument doc, CameraFirstPersonObject* basis) : CameraObject(doc, basis == 0 ? Factory::cameraObject : basis)
{
	init();
	if(basis != 0)
	{
		//nothign to copy over
	}

	// See if this class has been overriden in the XML. If so, extract and set those variables
	TiXmlHandle docHandle(&doc);
	TiXmlElement* mainElement = docHandle.FirstChild("CameraFirstPersonObject").ToElement();
	bool overridenInXML = (mainElement != 0);
	if(overridenInXML)
	{
		//nothign to set
	}

}

void CameraFirstPersonObject::init()
{
	this->currXZRads = 0.0f;
    this->currYRads = 0.0f;
	this->type = FIRST_PERSON;
	//this->cameraPos = glm::vec3(0,0,0);
    //this->updateWorldToCameraMatrix();
}

void CameraFirstPersonObject::setDefaultValues(bool preserveTransforms, bool preserveChildren)
{
	this->init();
	CameraObject::setDefaultValues(preserveTransforms, preserveChildren);
}

CameraFirstPersonObject* CameraFirstPersonObject::getCopy()
{
	CameraFirstPersonObject* copy = new CameraFirstPersonObject(*this);
	copy->setDefaultValues(false, false);
	return copy;
}

CameraFirstPersonObject* CameraFirstPersonObject::getCopy(bool preserveTransforms, bool preserveChildren)
{
	//note the copy constructor
	CameraFirstPersonObject* copy = new CameraFirstPersonObject(*this);
	copy->setDefaultValues(preserveTransforms, preserveChildren);
	return copy;
}


//-----------------------------
// Camera Movements ///////////
//-----------------------------
void CameraFirstPersonObject::pan(float x, float y)
{
	glm::vec3 right = glm::normalize(glm::cross(this->lookDir,this->upDir));
	glm::vec3 up = this->upDir;
	glm::vec3 moveX = x*right;
	glm::vec3 moveY = y*up;
	this->cameraPos += moveX;
	this->cameraPos += moveY;
	//this->updateWorldToCameraMatrix();
}

void CameraFirstPersonObject::rotate(float x, float y)
{
	this->currXZRads += x/3.0f;
	this->currYRads -= y/3.0f;
	//this->updateWorldToCameraMatrix();
}

void CameraFirstPersonObject::zoom(float distance)
{
	this->cameraPos += distance*this->lookDir;
	//this->updateWorldToCameraMatrix();
}

void CameraFirstPersonObject::CalcMatrix(void)
{
    float cosa = cosf(currXZRads);
    float sina = sinf(currXZRads);
    glm::vec3 currPos(sina, 0.0f, cosa);

    glm::vec3 UpRotAxis;
    UpRotAxis.x = currPos.z;
    UpRotAxis.y = currPos.y;
    UpRotAxis.z = -currPos.x;

    glm::mat4 xRotation = Transformations::getRotationMatrixRads(UpRotAxis, currYRads);
	currPos = glm::vec3(xRotation * glm::vec4(currPos, 0.0));

	this->lookDir = glm::normalize(currPos);
    this->upDir = glm::normalize(glm::cross(currPos, UpRotAxis));

    this->CalcLookAtMatrix();
}

void CameraFirstPersonObject::update()
{
	CameraObject::update();
}

CameraFirstPersonObject::~CameraFirstPersonObject(void)
{
	//nothing yet
}