#include "GameObject.h"
#include "../Factory.h"
#include "../Loader.h"




GameObject::GameObject(TiXmlDocument doc, GameObject* basis)
{
	//Set initial values
	this->uniformScaleWithHierarchy = true;
	this->parent = 0;

	if(basis != 0)
	{
		this->name = basis->name;
	}

	// See if this class has been overriden in the XML. If so, extract and set those variables
	// This could overwrite default values given by the factory class above
	TiXmlHandle docHandle(&doc);
	TiXmlElement* mainElement = docHandle.FirstChild("GameObject").ToElement();
	bool overridenInXML = (mainElement != 0);
	if(overridenInXML)
	{
		//set "name"
		TiXmlElement* nameElement = mainElement->FirstChildElement("name");
		if(nameElement != 0)
		{
			this->name = nameElement->FirstChild()->Value();
		}
	}

	//see if there is an attached object
	TiXmlElement* attachedObjectElement = docHandle.FirstChild("attached").ToElement();
	if(attachedObjectElement)
	{
		std::string objectName = attachedObjectElement->FirstChild()->Value();
		attachedObject = Singleton<Loader>::Instance()->getGameObjectByName(objectName)->getCopy();
		attachedObject->setNewParent(this);
	}
	else
	{
		attachedObject = 0;
	}
}

void GameObject::setDefaultValues(bool preserveTransforms, bool preserveChildren)
{
	//set default values. TODO: maybe we do need a set initial values function after all
	this->uniformScaleWithHierarchy = true;
	this->parent = 0;

	if(!preserveTransforms)
	{
		this->transformsScene = Transformations();
		this->transformsPersonal = Transformations();
	}

	if(preserveChildren)
	{
		//attachments are also preserved, so no need to add them again
		std::vector<GameObject*> copiedChildren = childObjects;
		this->removeChildren();
		for(unsigned int i = 0; i < copiedChildren.size(); i++)
		{
			copiedChildren[i]->getCopy(preserveTransforms, preserveChildren)->setNewParent(this);
		}
	}
	else
	{
		this->removeChildren();

		if(attachedObject)
		{
			attachedObject = attachedObject->getCopy(preserveTransforms, preserveChildren);
			attachedObject->setNewParent(this);
		}
	}
}

GameObject* GameObject::getCopy()
{
	//note the copy constructor
	GameObject* copy = new GameObject(*this);
	copy->setDefaultValues(false, false);
	return copy;
}

GameObject* GameObject::getCopy(bool preserveTransforms, bool preserveChildren)
{
	//note the copy constructor
	GameObject* copy = new GameObject(*this);
	copy->setDefaultValues(preserveTransforms, preserveChildren);
	return copy;
}

GameObject::~GameObject(void)
{
	//Nothing here .. yet
}

void GameObject::update()
{
	//Do nothing (it's a boring GameObject)
}
void GameObject::setName(std::string name)
{
	this->name = name;
}
std::string GameObject::getName()
{
	return this->name;
}

/*----------------------------------------
			Transformations
----------------------------------------*/
//Transformation
glm::mat4 GameObject::getTransformation()
{
	return this->transformsFinal;
}
//Translation
void GameObject::translateX(float amount, bool personal)
{
    if(personal) transformsPersonal.translateX(amount);
	else transformsScene.translateX(amount);
    updateTransforms(personal);
}
void GameObject::translateY(float amount, bool personal)
{
    if(personal) transformsPersonal.translateY(amount);
	else transformsScene.translateY(amount);
    updateTransforms(personal);
}
void GameObject::translateZ(float amount, bool personal)
{
    if(personal) transformsPersonal.translateZ(amount);
	else transformsScene.translateZ(amount);
    updateTransforms(personal);
}
void GameObject::translate(glm::vec3 vector, bool personal)
{
    if(personal) transformsPersonal.translate(vector);
	else transformsScene.translate(vector);
    updateTransforms(personal);
}
void GameObject::setTranslationX(float amount, bool personal)
{
    if(personal) transformsPersonal.setTranslationX(amount);
	else transformsScene.setTranslationX(amount);
    updateTransforms(personal);
}
void GameObject::setTranslationY(float amount, bool personal)
{
    if(personal) transformsPersonal.setTranslationY(amount);
	else transformsScene.setTranslationY(amount);
    updateTransforms(personal);
}
void GameObject::setTranslationZ(float amount, bool personal)
{
    if(personal) transformsPersonal.setTranslationZ(amount);
	else transformsScene.setTranslationZ(amount);
    updateTransforms(personal);
}
void GameObject::setTranslation(glm::vec3 vector, bool personal)
{
	if(personal) transformsPersonal.setTranslation(vector);
	else transformsScene.setTranslation(vector);
    updateTransforms(personal);
}
glm::vec3 GameObject::getPosition()
{
    return glm::vec3(transformsFinal[3]);
}
//Scale
void GameObject::scaleX(float amount, bool personal)
{
    if(personal) transformsPersonal.scaleX(amount);
	else transformsScene.scaleX(amount);
    updateTransforms(personal);
}
void GameObject::scaleY(float amount, bool personal)
{
    if(personal) transformsPersonal.scaleY(amount);
	else transformsScene.scaleY(amount);
    updateTransforms(personal);
}
void GameObject::scaleZ(float amount, bool personal)
{
    if(personal) transformsPersonal.scaleZ(amount);
	else transformsScene.scaleZ(amount);
    updateTransforms(personal);
}
void GameObject::scale(float amount, bool personal)
{
    if(personal) transformsPersonal.scale(amount);
	else transformsScene.scale(amount);
    updateTransforms(personal);
}
void GameObject::scale(glm::vec3 vector, bool personal)
{
    if(personal) transformsPersonal.scale(vector);
	else transformsScene.scale(vector);
    updateTransforms(personal);
}
void GameObject::setScale(float amount, bool personal)
{
    if(personal) transformsPersonal.setScale(amount);
	else transformsScene.setScale(amount);
    updateTransforms(personal);
}
void GameObject::setScale(glm::vec3 vector, bool personal)
{
    if(personal) transformsPersonal.setScale(vector);
	else transformsScene.setScale(vector);
    updateTransforms(personal);
}

//Rotation
void GameObject::yaw(float angle, bool personal)
{
    if(personal) transformsPersonal.yaw(angle);
	else transformsScene.yaw(angle);
    updateTransforms(personal);
}
void GameObject::pitch(float angle, bool personal)
{
    if(personal) transformsPersonal.pitch(angle);
	else transformsScene.pitch(angle);
    updateTransforms(personal);
}
void GameObject::roll(float angle, bool personal)
{
    if(personal) transformsPersonal.roll(angle);
	else transformsScene.roll(angle);
    updateTransforms(personal);
}
void GameObject::rotate(glm::vec3 axis, float angle, bool personal)
{
    if(personal) transformsPersonal.rotate(axis,angle);
	else transformsScene.rotate(axis,angle);
    updateTransforms(personal);
}
void GameObject::setRotation(glm::vec3 axis, float angle, bool personal)
{
    if(personal) transformsPersonal.setRotation(axis,angle);
	else transformsScene.setRotation(axis,angle);
    updateTransforms(personal);
}

void GameObject::setRotation(glm::fquat rotation, bool personal)
{
	if(personal) transformsPersonal.setRotation(rotation);
	else transformsScene.setRotation(rotation);
	updateTransforms(personal);
}

void GameObject::setRotation(glm::mat4 rotation, bool personal)
{
	if(personal) transformsPersonal.setRotation(rotation);
	else transformsScene.setRotation(rotation);
	updateTransforms(personal);
}

/*------------------------------------------
		Other scene graph stuff
------------------------------------------*/
void GameObject::setNewParent(GameObject* newParent)
{
    //remove from previous parent (if parent != 0)
	if(this->parent != newParent)
	{
		if(!isRoot())
		{
			this->parent->removeChild(this);
		}

		parent = newParent;

		if(!isRoot())
		{
			newParent->childObjects.push_back(this);
			updateTransforms(false); //updates transform of object within the hierarchy structure
			updateTransforms(true); //update the final transform of the object, which concats the personal transform to the hierarchical transform
		}
	}
}
void GameObject::removeChild(GameObject* childToRemove)
{
	std::vector<GameObject*>::iterator it;
	for(it = childObjects.begin(); it != childObjects.end(); ++it)
	{
		if(*it == childToRemove)
		{
			childObjects.erase(it);
			break;
		}
	}
}

//TODO: this method is only used for copying purposes only, and completely
//ignores memory handling. Improve/integrate this better later
void GameObject::removeChildren()
{
	childObjects.clear();
}

bool GameObject::isRoot()
{
    return parent == 0;
}
void GameObject::updateTransforms(bool personal)
{
    if(personal)
    {
        updateFinalTransforms();
    }
    else
    {
        //set this node to non-uniform scale
        if(transformsScene.uniformScale == false)
        {
            uniformScaleWithHierarchy = false;
        }
    }
}
void GameObject::updateFinalTransforms()
{
    uniformScaleFinal = transformsPersonal.uniformScale && uniformScaleWithHierarchy;
    transformsFinal = transformsWithHierarchy*transformsPersonal.getTransformationMatrix();
}
