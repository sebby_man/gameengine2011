#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <iostream>
#include <vector>


namespace Utils
{

void printMatrix(glm::mat4 matrix);
void printVec3(glm::vec3 vector);
void printVec4(glm::vec4 vector);
void printQuat(glm::fquat quaternion);

};
