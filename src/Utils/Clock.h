#pragma once
 
#include <SFML/System.hpp>

class PausableClock
{
	public :
		
		PausableClock(bool paused = false): am_i_paused(paused), my_time(0.f), my_clock()
		{
			// Nothing else.
		}
 
		void Pause(void)
		{
			// If not yet paused...
			if (!am_i_paused)
			{
				am_i_paused = true;
				my_time += my_clock.getElapsedTime().asSeconds();
			}
		}
 
		void Start(void)
		{
			// If not yet started...
			if (am_i_paused)
			{
				my_clock.restart();
				am_i_paused = false;
			}
		}
 
		float GetElapsedTime(void) const
		{
			// If not paused...
			if (!am_i_paused)
				return my_time + my_clock.getElapsedTime().asSeconds();
			else
				return my_time;
		}
 
		void Reset(bool paused = false)
		{
			am_i_paused = paused;
			my_time = 0.f;
			my_clock.restart();
		}
 
	private :
		bool am_i_paused;
		float my_time;
		sf::Clock my_clock;
};
 

 