#include "MathUtils.h"


namespace Utils
{


//---------------------------------------------
// TRIANGULATE A POLYGON
//---------------------------------------------

float areaOfTriangle(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3)
{
    float sideA = glm::distance(p1, p2);
    float sideB = glm::distance(p2, p3);
    float sideC = glm::distance(p3, p1);

    float s = (sideA + sideB + sideC)/2;
    float area = sqrt(s*(s-sideA)*(s-sideB)*(s-sideC));


    return area;
}

const float EPSILON=0.0000000001f;

float Area(const std::vector<glm::vec2>& contour)
{

    int n = contour.size();

    float A=0.0f;

    for(int p=n-1,q=0; q<n; p=q++)
    {
        A+= contour[p].x*contour[q].y - contour[q].x*contour[p].y;
    }
    return A*0.5f;
}

bool InsideTriangle(float Ax, float Ay, float Bx, float By, float Cx, float Cy, float Px, float Py)
{
    float ax, ay, bx, by, cx, cy, apx, apy, bpx, bpy, cpx, cpy;
    float cCROSSap, bCROSScp, aCROSSbp;

    ax = Cx - Bx;
    ay = Cy - By;
    bx = Ax - Cx;
    by = Ay - Cy;
    cx = Bx - Ax;
    cy = By - Ay;
    apx= Px - Ax;
    apy= Py - Ay;
    bpx= Px - Bx;
    bpy= Py - By;
    cpx= Px - Cx;
    cpy= Py - Cy;

    aCROSSbp = ax*bpy - ay*bpx;
    cCROSSap = cx*apy - cy*apx;
    bCROSScp = bx*cpy - by*cpx;

    return ((aCROSSbp >= 0.0f) && (bCROSScp >= 0.0f) && (cCROSSap >= 0.0f));
};

bool Snip(const std::vector<glm::vec2>& contour,int u,int v,int w,int n,int *V)
{
    int p;
    float Ax, Ay, Bx, By, Cx, Cy, Px, Py;

    Ax = contour[V[u]].x;
    Ay = contour[V[u]].y;

    Bx = contour[V[v]].x;
    By = contour[V[v]].y;

    Cx = contour[V[w]].x;
    Cy = contour[V[w]].y;

    if ( EPSILON > (((Bx-Ax)*(Cy-Ay)) - ((By-Ay)*(Cx-Ax))) ) return false;

    for (p=0; p<n; p++)
    {
        if( (p == u) || (p == v) || (p == w) ) continue;
        Px = contour[V[p]].x;
        Py = contour[V[p]].y;
        if (InsideTriangle(Ax,Ay,Bx,By,Cx,Cy,Px,Py)) return false;
    }

    return true;
}

std::vector<glm::vec2> TriangulatePolygon(const std::vector<glm::vec2>& contour)
{

    std::vector<glm::vec2> result;
    /* allocate and initialize list of Vertices in polygon */


    glm::vec2 first = contour[0];
    glm::vec2 last = contour[contour.size()-1];

    float epsilon = .001f;
    if(!((abs(first.x - last.x) < epsilon) && (abs(first.y - last.y) < epsilon)))
    {
        //----ERROR
        std::cout << "ERROR: first and last points in the polygon must be equal" << std::endl;
        return result;
    }

    int n = contour.size() - 1;
    if ( n < 3 ) return result;

    int *V = new int[n];

    /* we want a counter-clockwise polygon in V */

    if ( 0.0f < Area(contour) )
        for (int v=0; v<n; v++) V[v] = v;
    else
        for(int v=0; v<n; v++) V[v] = (n-1)-v;

    int nv = n;

    /*  remove nv-2 Vertices, creating 1 triangle every time */
    int count = 2*nv;   /* error detection */

    for(int m=0, v=nv-1; nv>2; )
    {
        /* if we loop, it is probably a non-simple polygon */
        if (0 >= (count--))
        {
            //** Triangulate: ERROR - probable bad polygon!
            return result;
        }

        /* three consecutive vertices in current polygon, <u,v,w> */
        int u = v  ;
        if (nv <= u) u = 0;     /* previous */
        v = u+1;
        if (nv <= v) v = 0;     /* new v    */
        int w = v+1;
        if (nv <= w) w = 0;     /* next     */

        if ( Snip(contour,u,v,w,nv,V) )
        {
            int a,b,c,s,t;

            /* true names of the vertices */
            a = V[u];
            b = V[v];
            c = V[w];

            /* output Triangle */
            result.push_back( contour[a] );
            result.push_back( contour[b] );
            result.push_back( contour[c] );

            m++;

            /* remove v from remaining polygon */
            for(s=v,t=v+1; t<nv; s++,t++) V[s] = V[t];
            nv--;

            /* resest error detection counter */
            count = 2*nv;
        }
    }



    delete V;

    return result;
}

}
