#include "FileUtils.h"


namespace Utils
{

	std::string filePath;

	void setFilePath(boost::filesystem3::path programRoot)
	{
		filePath = programRoot.string();
	}


	std::string convertToFowardSlashes(std::string filepath)
	{
		//turn all back slashes to foward slashes
		size_t found = filepath.find('\\');
		while(found != std::string::npos)
		{
			filepath[found] = '/';
			found = filepath.find('\\');
		}
		return filepath;

	}



	std::string formatPath(std::string path)
	{
		//if we need to format for windows
		if(FILE_SLASH == '\\')
		{
			size_t found = path.find('/');
			while(found != std::string::npos)
			{
				path[found] = FILE_SLASH;
				found = path.find('/');
			}
		}
		return path;
	}

	std::string getFilePath(std::string subFolder)
	{
		std::string returnFilename = filePath + FILE_SLASH + formatPath(subFolder);
		return returnFilename;
	}

	bool stringToBool(std::string value)
	{
		int val = Utils::stringToInt(value);
		return (val == 1);
	}
	int stringToInt(std::string value)
	{
		return std::atoi(value.c_str());
	}

	float stringToFloat(std::string value)
	{
		return (float)std::atof(value.c_str());
	}

	std::vector<std::string> parseSpaceSeparatedString(std::string configData)
	{
		std::vector<std::string> tokens;
		std::istringstream iss(configData);
		std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter<std::vector<std::string> >(tokens));
		return tokens;
	}

	unsigned int getStringHash(const char* s)
	{
		unsigned int hash = 0;
		int c;

		while((c = *s++))
		{
			// hash = hash * 33 ^ c
			hash = ((hash << 5) + hash) ^ c;
		}
		return hash;
	}
}
