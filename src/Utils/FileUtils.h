#pragma once

#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <vector>
#include <iostream>
#include <boost/filesystem.hpp>

#ifdef _WIN32
#define FILE_SLASH '\\'
#else
#define FILE_SLASH '/'
#endif //WIN32




namespace Utils
{

	std::string convertToFowardSlashes(std::string filepath);

	void setFilePath(boost::filesystem3::path programRoot);
	std::string getFilePath(std::string subFolder);

	bool stringToBool(std::string value);
	int stringToInt(std::string value);
	float stringToFloat(std::string value);

	std::vector<std::string> parseSpaceSeparatedString(std::string configData);

	unsigned int getStringHash(const char* s);
};
