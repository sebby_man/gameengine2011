#pragma once

#include <glm/glm.hpp>
#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/BulletCollision/CollisionShapes/btConeShape.h>
#include <bullet/BulletCollision/CollisionShapes/btCapsuleShape.h>

#include <vector>

/*------------------------------------------
This class contains information about
different types of physics primitives,
including Spheres, Cubes, Meshes, and more
------------------------------------------*/


enum PRIMITIVE_TYPES {
	PRIMITIVE_BOX,       //box
	PRIMITIVE_SPHERE,    //sphere
	PRIMITIVE_CYLINDER,  //cylinder
	PRIMITIVE_CONE,      //cone
	PRIMITIVE_CAPSULE,   //capsule
	PRIMITIVE_MESH,      //mesh
};

struct PrimitiveData
{
	PrimitiveData(){}

	int primitiveType;
	btCollisionShape* collisionShape;

	//Creates the btCollisionShape based on the member variables
	virtual void initialize() = 0;
	virtual PrimitiveData* getCopy() = 0;
};
struct PrimitiveDataBox : public PrimitiveData
{
	glm::vec3 dimensions;

	PrimitiveDataBox(glm::vec3 dimensions) : PrimitiveData() 
	{
		this->dimensions = dimensions;
		this->initialize();
	}
	virtual void initialize()
	{
		btVector3 dimensionsBullet = btVector3(dimensions.x,dimensions.y,dimensions.z);
		this->collisionShape = new btBoxShape(dimensionsBullet);
		this->primitiveType = PRIMITIVE_BOX;
	}
	virtual PrimitiveDataBox* getCopy()
	{
		return new PrimitiveDataBox(dimensions);
	}
};

struct PrimitiveDataSphere : public PrimitiveData
{
	float radius;

	PrimitiveDataSphere(float radius) : PrimitiveData() 
	{
		this->radius = radius;
		this->initialize();
	}
	virtual void initialize()
	{
		this->collisionShape = new btSphereShape(radius);
		this->primitiveType = PRIMITIVE_SPHERE;
	}
	virtual PrimitiveDataSphere* getCopy()
	{
		return new PrimitiveDataSphere(this->radius);
	}
};

/*
struct PrimitiveDataCylinder : public PrimitiveData
{
	float radius;
	float height;
	PrimitiveDataCylinder() : PrimitiveData() {}
	virtual void initialize()
	{
		this->collisionShape = new btCylinderShape(btVector3(radius,radius,height));
		this->primitiveType = PRIMITIVE_CYLINDER;
	}
};

struct PrimitiveDataCone : public PrimitiveData
{
	float radius;
	float height;
	PrimitiveDataCone() : PrimitiveData() {}
	virtual void initialize()
	{
		this->collisionShape = new btConeShape(radius,height);
		this->primitiveType = PRIMITIVE_CONE;
	}
};

struct PrimitiveDataCapsule : public PrimitiveData
{
	float radius;
	float height;
	PrimitiveDataCapsule() : PrimitiveData() {}
	virtual void initialize()
	{
		this->collisionShape = new btCapsuleShape(radius, height);
		this->primitiveType = PRIMITIVE_CAPSULE;
	}
};
*/
/*
struct PrimitiveDataMesh : public PrimitiveData
{
	std::vector<float> positionData;
	std::vector<unsigned short> elementData;
	virtual void initialize()
	{
		//this should handle convexity
	}
};
*/
	