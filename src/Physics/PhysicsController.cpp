#include "PhysicsController.h"

PhysicsController::PhysicsController()
{
	Singleton<InputHandler>::Instance()->addEnterFrameEventListener(EnterFrameReceiver::from_method<PhysicsController,&PhysicsController::enterFrame>(this));
}
PhysicsController::~PhysicsController()
{
	//Do nothing
}

void PhysicsController::attachTo(PhysicsObject* physicsObject)
{
	this->agent = physicsObject;
}
void PhysicsController::enterFrame()
{
	if(agent != 0)
	{
		float power = 12;
		btRigidBody* body = this->agent->rigidBody;
		body->activate(true);
		InputHandler* inputHandler = Singleton<InputHandler>::Instance();
		if(inputHandler->isKeyDown(sf::Keyboard::Left))
		{
			body->applyCentralForce(btVector3(-power,0,0));
		}
		if(inputHandler->isKeyDown(sf::Keyboard::Right))
		{
			body->applyCentralForce(btVector3(power,0,0));
		}
		if(inputHandler->isKeyDown(sf::Keyboard::Up))
		{
			body->applyCentralForce(btVector3(0,power,0));
		}
		if(inputHandler->isKeyDown(sf::Keyboard::Down))
		{
			body->applyCentralForce(btVector3(0,-power,0));
		}
	}	
}