#pragma once

#include "../GameObjects/PhysicsObject.h"
#include "../InputHandler.h"
#include "../Singleton.h"

#include <bullet/btBulletDynamicsCommon.h>

class PhysicsController
{
public:
	PhysicsController();
	~PhysicsController();

	void attachTo(PhysicsObject* physicsObject);

private:
	PhysicsObject* agent;
	void keyDown(sf::Event event);
	void keyUp(sf::Event event);
	void enterFrame();
};