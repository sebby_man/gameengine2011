#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/BulletCollision/CollisionShapes/btConeShape.h>

#include <vector>

#include "../GameObjects/PhysicsObject.h"
#include "PhysicsPrimitives.h"
#include "PhysicsUtils.h"

class PhysicsWorld
{
public:
	PhysicsWorld();
	~PhysicsWorld();

	void addObject(PhysicsObject* object);
	void update();

private:
	btDefaultCollisionConfiguration* collisionConfiguration;
	btDynamicsWorld* dynamicsWorld;
	btBroadphaseInterface*	broadphase;
	btCollisionDispatcher*	dispatcher;
	btConstraintSolver*	solver;

	btRigidBody* createRigidBody(float mass, btTransform& startTransform, btCollisionShape* collisionShape);
};