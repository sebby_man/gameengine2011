#pragma once

#include <iostream>

#include "Rendering/GL_GameState.h"
#include "World.h"
#include "InputHandler.h"
#include "Physics/PhysicsWorld.h"

class GameState
{
public:
	GameState();
	virtual ~GameState(void);

    void enterFrame(void);
    void resizeApp(int width, int height);

private:
    World world;
};
