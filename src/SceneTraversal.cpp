#include "SceneTraversal.h"
#include "GameObjects/GameObject.h"
#include "GameObjects/JointObject.h"
#include "GameObjects/AnimatedObject.h"
#include "GameObjects/LightObject.h"
#include "Scene.h"

namespace SceneTraversers
{


/*-----------------------
		Finder
-----------------------*/
Finder::Finder(std::string name) : TraversalAction()
{
	this->name = name;
	this->resultStorer = 0;
	this->doneTraversing = false;
}
void Finder::traverse(GameObject* gameObject)
{
	//preorder
	if(gameObject->getName() == this->name)
	{
		this->resultStorer = gameObject;
		this->doneTraversing = true;
	}

	if(!this->doneTraversing)
	{
		//traverse over children
		std::vector<GameObject*> children = gameObject->childObjects;
		for(unsigned int i = 0; i < children.size(); i++)
		{
			traverse(children[i]);
		}
	}


}

/*-----------------------
		Deleter
-----------------------*/
Deleter::Deleter(GameObject* gameObject) : TraversalAction()
{
	if(!gameObject->isRoot())
	{
		gameObject->parent->removeChild(gameObject);
	}
}
void Deleter::traverse(GameObject* gameObject)
{
	//traverse over children
	std::vector<GameObject*> children = gameObject->childObjects;
	for(unsigned int i = 0; i < children.size(); i++)
	{
		traverse(children[i]);
	}

	//postorder
	delete gameObject;
}


/*-----------------------
	TransformsUpdater
-----------------------*/
TransformsUpdater::TransformsUpdater(GameObject* gameObject) : TraversalAction()
{
	glm::mat4 top;
	if(gameObject->isRoot())
		top = glm::mat4(1.0f);
	else
		top = gameObject->parent->transformsWithHierarchy;

	transformsStack.push(top);
}
void TransformsUpdater::traverse(GameObject* gameObject)
{
	//preorder
	//set nodes to non-uniform scale based on hierarchy
	if(!gameObject->isRoot())
	{
		gameObject->uniformScaleWithHierarchy = gameObject->parent->uniformScaleWithHierarchy && gameObject->uniformScaleWithHierarchy;
	}
	glm::mat4 transformsWithHierarchy = transformsStack.top()*gameObject->transformsScene.getTransformationMatrix();
	gameObject->transformsWithHierarchy = transformsWithHierarchy;
	gameObject->updateFinalTransforms();
	transformsStack.push(transformsWithHierarchy);

	//traverse over children
	std::vector<GameObject*> children = gameObject->childObjects;
	for(unsigned int i = 0; i < children.size(); i++)
	{
		traverse(children[i]);
	}

	//postorder
	transformsStack.pop();
}

/*-----------------------
		Updater
-----------------------*/
void Updater::traverse(GameObject* gameObject)
{
	gameObject->update();

	//traverse over children
	std::vector<GameObject*> children = gameObject->childObjects;
	for(unsigned int i = 0; i < children.size(); i++)
	{
		traverse(children[i]);
	}

}


/*-----------------------
		AddGameObject
-----------------------*/
AddGameObject::AddGameObject(Scene* scene)
{
	this->scene = scene;
}


void AddGameObject::traverse(GameObject* object)
{
	//special type check
	
	PhysicsObject* physicsObject = dynamic_cast<PhysicsObject*>(object);
	LightObject* lightObject = dynamic_cast<LightObject*>(object);
	RenderObject* renderObject = dynamic_cast<RenderObject*>(object);
	CameraObject* cameraObject = dynamic_cast<CameraObject*>(object);
	
	bool isTransparent = false;
	if(physicsObject != 0)
	{
		Singleton<PhysicsWorld>::Instance()->addObject(physicsObject);
	}
	else if(lightObject != 0)
	{
		scene->getLights().push_back(lightObject);
	}
	else if(renderObject != 0)
	{
		if(renderObject->isTransparent())
		{
			isTransparent = true;
		}
	}
	else if(cameraObject != 0)
	{
		scene->getCameras().push_back(cameraObject);
		scene->currentCamera = cameraObject;
	}



	if(isTransparent) scene->getTransparentObjects().push_back(object);
	else scene->getObjects().push_back(object);
	

	

	//traverse over children
	std::vector<GameObject*> children = object->childObjects;
	for(unsigned int i = 0; i < children.size(); i++)
	{
		traverse(children[i]);
	}

}


}; //End namespace