#include "InputHandler.h"

InputHandler::InputHandler()
{
	//Do nothing
}
InputHandler::~InputHandler()
{
	//Do nothing
}


/*---------------------------------
	Events
---------------------------------*/

void InputHandler::addInputEventListener(sf::Event::EventType eventType, InputReceiver receiverFunction)
{
	/*To use from another class:
	Singleton<InputHandler>::Instance()->addInputEventListener(sf::Event::MouseMoved, InputReceiver::from_method<World,&World::mouseMoved>(this));
	*/

	InputReceiverList* inputReceivers = 0;
	InputEventMap::iterator iter = this->inputEventMap.find(eventType);
	if(iter == this->inputEventMap.end())
		inputReceivers = new InputReceiverList();
	else
		inputReceivers = iter->second;
	inputReceivers->push_back(receiverFunction);
	inputEventMap[eventType] = inputReceivers;
}
void InputHandler::removeInputEventListener(sf::Event::EventType eventType, InputReceiver receiverFunction)
{	
	InputEventMap::iterator iter = this->inputEventMap.find(eventType);
	if(iter != this->inputEventMap.end())
	{
		InputReceiverList* receiverList = iter->second;
		for(unsigned int i = 0; i < receiverList->size(); i++)
		{
			InputReceiver receiver = receiverList->at(i);
			if(receiver.object_ptr == receiverFunction.object_ptr)
			{
				receiverList->erase(receiverList->begin() + i);
				break;
			}
		}
	}
}
void InputHandler::addEnterFrameEventListener(EnterFrameReceiver receiverFunction)
{
	/*To use from another class:
	Singleton<InputHandler>::Instance()->addEnterFrameEventListener(EnterFrameReceiver::from_method<GameState,&GameState::enterFrame>(this));
	*/
	
	enterFrameEvents.push_back(receiverFunction);
}
void InputHandler::removeEnterFrameListener(EnterFrameReceiver receiverFunction)
{
	for(unsigned int i = 0; i < enterFrameEvents.size(); i++)
	{
		EnterFrameReceiver receiver = enterFrameEvents.at(i);
		if(receiver.object_ptr == receiverFunction.object_ptr)
		{
			enterFrameEvents.erase(enterFrameEvents.begin() + i);
			break;
		}
	}
}

void InputHandler::processEvent(sf::Event sfEvent)
{
	//Process events
	InputEventMap::iterator iter = this->inputEventMap.find(sfEvent.type);
	if(iter != this->inputEventMap.end())
	{
		InputReceiverList* receiverList = iter->second;
		for(unsigned int i = 0; i < receiverList->size(); i++)
		{
			InputReceiver receiver = receiverList->at(i);
			receiver(sfEvent);
		}
	}

	//Update mouse position
	if(sfEvent.type == sf::Event::MouseMoved)
	{
		int x = sfEvent.mouseMove.x;
		int y = sfEvent.mouseMove.y;
		this->setMousePos(x,y);
	}
}
void InputHandler::enterFrame()
{
	for(unsigned int i = 0; i < this->enterFrameEvents.size(); i++)
	{
		EnterFrameReceiver receiver = this->enterFrameEvents.at(i);
		receiver();
	}
}

/*---------------------------------
	Keyboard
---------------------------------*/
bool InputHandler::isKeyDown(sf::Keyboard::Key keyCode)
{
	return sf::Keyboard::isKeyPressed(keyCode);
}
bool InputHandler::isShiftDown()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift);
}
bool InputHandler::isAltDown()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt) || sf::Keyboard::isKeyPressed(sf::Keyboard::RAlt);
}
bool InputHandler::isControlDown()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl);
}

/*---------------------------------
	Mouse
---------------------------------*/
bool InputHandler::isMouseMoving()
{
	return (!(mouseX == prevMouseX && mouseY == prevMouseY));
}
bool InputHandler::isMouseDown(sf::Mouse::Button button)
{
	return sf::Mouse::isButtonPressed(button);
}
bool InputHandler::isLeftMouseDown()
{
	return sf::Mouse::isButtonPressed(sf::Mouse::Left);
}
bool InputHandler::isRightMouseDown()
{
	return sf::Mouse::isButtonPressed(sf::Mouse::Right);
}
bool InputHandler::isMiddleMouseDown()
{
	return sf::Mouse::isButtonPressed(sf::Mouse::Middle);
}

glm::ivec2 InputHandler::getMousePos(void)
{
	return glm::ivec2(mouseX, mouseY);
}
glm::ivec2 InputHandler::getPrevMousePos(void)
{
	return glm::ivec2(prevMouseX,prevMouseY);
}
void InputHandler::setMousePos(int x, int y)
{
	this->prevMouseX = this->mouseX;
	this->prevMouseY = this->mouseY;
	this->mouseX = x;
	this->mouseY = y;
}