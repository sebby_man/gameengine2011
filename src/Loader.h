#pragma once

#include <tinyxml/tinyxml.h>
#include "Rendering/GL_MeshPrimitive.h"
#include "Factory.h"
#include "Scene.h"
#include "Utils/FileUtils.h"
#include "Utils/PrintUtils.h"
#include "ColladaData.h"
#include <dae.h>
#include <dom/domCOLLADA.h>
#include <boost/tokenizer.hpp>


class Loader
{
public:
	Loader();
	Scene* loadScene(std::string filepath);
	MaterialBlock getMaterialByName(std::string name);
	GameObject* getGameObjectByName(std::string name);

	boost::unordered_map<std::string, std::vector<RenderObject*>> meshToRenderObjectsMap;
	boost::unordered_map<std::string, MaterialBlock> materials;

	void fillMaterialBlock(TiXmlElement* xmlElement, MaterialBlock& materialBlock);

private:
	/*------------------
		loads mesh
	-------------------*/
	//Reads an asset COLLADA file
	ColladaData readColladaAsset(std::string& fileName);

	VertexSkeleton* loadVertexDataForAnimatedMesh(TiXmlElement* vertexData, int& numVertices, bool& containsPositions, bool& containsNormals, bool& containsUVs);
	Vertex* loadVertexDataForStaticMesh(TiXmlElement* vertexData, int& numVertices, bool& containsPositions, bool& containsNormals, bool& containsUVs);

	void loadSkeleton(TiXmlElement* node, JointObject* parentJoint, std::vector<JointObject*>& jointList);
	std::vector<AnimatedObject::AnimationClip> loadAnimationData(TiXmlElement* animationData);

	std::vector<float> parseDataIntoFloats(const std::string& configData, size_t size);
	std::vector<unsigned short> parseDataIntoUShorts(const std::string& configData, size_t size);
	glm::mat4 parseDataIntoMat4(const std::string& configData);
	std::vector<glm::vec3> Loader::parseTranslationData(const std::string& configData, int numTranslations);
	std::vector<glm::fquat> Loader::parseQuatData(const std::string& configData, int numQuats);
	/*------------------
		loads scene
	-------------------*/
	
	void loadMeshFile(std::string filename);
	void loadMesh(std::string name);
	void loadMeshFolder(std::string folderName);
	void loadSceneObject(const TiXmlElement* parentElement, Scene* scene, GameObject* parentObject);
	void loadMaterials();
};



