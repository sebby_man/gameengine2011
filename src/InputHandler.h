#pragma once

#define SRUTIL_DELEGATE_PREFERRED_SYNTAX

#include <SFML/Window.hpp>
#include <SFML/Window/Window.hpp>
#include <SFML/Window/Event.hpp>
#include <glm/glm.hpp>
#include <srutil/delegate/delegate.hpp>

#include <map>
#include <utility>
#include <vector>

/*--------------------------------------
This class handles all input/output events.
Includes: Keyboard, Mouse, Enter Frame, and more
--------------------------------------*/

typedef srutil::delegate<void (void)> EnterFrameReceiver;
typedef std::vector<EnterFrameReceiver> EnterFrameReceiverList;

typedef srutil::delegate<void (sf::Event)> InputReceiver;
typedef std::vector<InputReceiver> InputReceiverList;
typedef std::map<sf::Event::EventType,InputReceiverList*> InputEventMap;

class InputHandler
{

public:
	InputHandler();
	~InputHandler();

	//Events
	void enterFrame();
	void processEvent(sf::Event sfEvent);
	//Enter Frame
	void addEnterFrameEventListener(EnterFrameReceiver receiverFunction);
	void removeEnterFrameListener(EnterFrameReceiver receiverFunction);
	//Input
	void addInputEventListener(sf::Event::EventType eventType, InputReceiver receiverFunction);
	void removeInputEventListener(sf::Event::EventType eventType, InputReceiver receiverFunction);

	//Keyboard
	bool isKeyDown(sf::Keyboard::Key keyCode);
	bool isShiftDown();
	bool isAltDown();
	bool isControlDown();

	//Mouse
	bool isMouseMoving();
	bool isLeftMouseDown();
	bool isRightMouseDown();
	bool isMiddleMouseDown();

	glm::ivec2 getMousePos(void);
	glm::ivec2 getPrevMousePos(void);

	
private:
	InputEventMap inputEventMap;
	EnterFrameReceiverList enterFrameEvents;

	bool isMouseDown(sf::Mouse::Button button);
	void setMousePos(int x, int y);

	int mouseX;
	int mouseY;
	int prevMouseX;
	int prevMouseY;

	sf::Window* window;
};