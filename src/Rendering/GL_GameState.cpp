#include "GL_GameState.h"






GL_GameState::GL_GameState(void)
{
    gl3wInit();

    fzNear = .10f;
    fzFar = 1000.0f;
    fieldOfViewDeg = 45.0f;
    frustumScale = CalcFrustumScale(fieldOfViewDeg);

    cameraToClipMatrix = glm::mat4(0.0f);
    cameraToClipMatrix[0].x = frustumScale;
    cameraToClipMatrix[1].y = frustumScale;
    cameraToClipMatrix[2].z = (fzFar + fzNear) / (fzNear - fzFar);
    cameraToClipMatrix[2].w = -1.0f;
    cameraToClipMatrix[3].z = (2 * fzFar * fzNear) / (fzNear - fzFar);


    Singleton<GL_ShaderState>::Instance()->setCameraToClip(cameraToClipMatrix);

	glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);

    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glDepthFunc(GL_LEQUAL);
    glDepthRange(0.0f, 1.0f);

	glDisable(GL_STENCIL_TEST);

	glDisable(GL_MULTISAMPLE);
	//glDisable(GL_SAMPLE_SHADING);
	//glMinSampleShading(0.0);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	//glEnable(GL_POLYGON_SMOOTH);

	glDisable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	
	int value;
	glGetIntegerv(GL_MAX_VERTEX_UNIFORM_COMPONENTS, &value);
	std::cout << "num UBOS: " << value << std::endl;

	

	if(TRANSPARENCY_ENABLED)
	{
		Singleton<GL_ShaderState>::Instance()->setTransparencyOff();
		initTransparency();
	}
}

float GL_GameState::CalcFrustumScale(float fieldOfViewDegrees)
{
    float fFovRad = fieldOfViewDegrees * Utils::degToRad;
    return 1.0f / tan(fFovRad / 2.0f);
}


//Called whenever the window is resized. The new window size is given, in pixels.
//This is an opportunity to call glViewport or glScissor to keep up with the change in size.
void GL_GameState::reshape (int w, int h)
{
    width = w;
    height = h;

    cameraToClipMatrix[0].x = frustumScale * (height / (float)width);
    cameraToClipMatrix[1].y = frustumScale;//* (w / (float)h);


    Singleton<GL_ShaderState>::Instance()->setCameraToClip(cameraToClipMatrix);

    glViewport(0, 0, (GLsizei) width, (GLsizei) height);

	if(TRANSPARENCY_ENABLED)
	{
		resizeTransparency();
	}
}



Ray GL_GameState::getClickedRay(int x, int y)
{

    //get window coordinates
    float winX = (float)x;
    float winY = (float)y;


    //convert window space to NDC space for the far.
    //At windowZ = 0, the point lies on the near clipping plane
    glm::vec4 onFrustum;
    onFrustum[0] = 2*(winX - 0)/width - 1;
    onFrustum[1] = 2*(height - winY)/height - 1;
    onFrustum[2] = 2*0 - 1;
    onFrustum[3] = 1;

    //convert NDC space to clip space
    onFrustum *= fzNear;

    //convert clip space to camera space by inverting the cam to clip matrix
    glm::mat4 clipToCam = glm::inverse(cameraToClipMatrix);
	//TODO: get the worl to camera from elsewhere
    glm::mat4 camToWorld = glm::inverse(Singleton<GL_ShaderStateData>::Instance()->currWorldToCameraMatrix);
    glm::mat4 completeMatrix = camToWorld*clipToCam;

    glm::vec4 NDCSpaceClose = camToWorld*glm::vec4(0,0,0,1);
    onFrustum = completeMatrix*onFrustum;

    glm::vec3 rayStart = glm::vec3(NDCSpaceClose);
    glm::vec3 rayEnd = glm::vec3(onFrustum);
    glm::vec3 rayDir = rayEnd - rayStart;

    Ray ray(rayStart, rayDir);
    return ray;
}



void GL_GameState::prepareRenderForOpaqueObjects(void)
{

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClearDepth(1.0f);
	glClearStencil(0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}






void GL_GameState::initTransparency()
{	
	//create nodeCounter atomic counter
	glGenBuffers(1, &nodeCounter);
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, nodeCounter);
    glBufferData(GL_ATOMIC_COUNTER_BUFFER, sizeof(GLuint), 0, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, nodeCounter);
	glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);

	//assume that the screen size isnt greater than 1920x1080 (may have to make bigger later)
	zeroArrayForHeadsArray = new unsigned int[1920*1080];
	memset(zeroArrayForHeadsArray, 0, sizeof(unsigned int)*1920*1080);

	glActiveTexture(GL_TEXTURE0 + 1);
	glGenTextures(1, &headsArray);
	glBindTexture(GL_TEXTURE_2D, headsArray);

	//create global data array
	predictedNumberOfFragments = 2097152;
	unsigned int* zeroArray = new unsigned int[predictedNumberOfFragments*4];
	memset(zeroArray, 0, sizeof(unsigned int)*predictedNumberOfFragments*4);
	
	glGenBuffers(1, &globalsDataBufferObject);
    glBindBuffer(GL_TEXTURE_BUFFER, globalsDataBufferObject);
    glBufferData(GL_TEXTURE_BUFFER, sizeof(GLuint)*predictedNumberOfFragments*4, zeroArray, GL_DYNAMIC_DRAW);
	
	delete[] zeroArray;

	glActiveTexture(GL_TEXTURE0 + 2);
	glGenTextures(1, &globalsDataTextureBuffer);
	glBindTexture(GL_TEXTURE_BUFFER, globalsDataTextureBuffer);
	glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32UI, globalsDataBufferObject);
	glBindTexture(GL_TEXTURE_BUFFER, 0);
	
	
	
	//create screen covering quad
	unsigned short indexes[] =
    {
        0, 1, 2,
        2, 3, 0
    };

    std::vector<GLushort> indexData = std::vector<GLushort>(indexes, indexes + 6);
	
	//clip space positions
	float vertices[] =
    {
        -1, 1, -1,
		-1, -1, -1,
		1, -1, -1,
		1, 1, -1
    };
	
	Vertex* vertexData = new Vertex[4];
	for(int i = 0; i < 4; i++)
	{
		vertexData[i].x = vertices[i*3 + 0];
		vertexData[i].y = vertices[i*3 + 1];
		vertexData[i].z = vertices[i*3 + 2];
	}
	

	std::string textureName = "";
	screenCoveringQuad = new GL_MeshPrimitive(4, vertexData, true, false, false, textureName, indexData, GL_TRIANGLES, GL_STREAM_DRAW);
	transparencyResolveProgram = Singleton<GL_ShaderState>::Instance()->transparencyResolveProgram;


}

void GL_GameState::resizeTransparency()
{
	glActiveTexture(GL_TEXTURE0 + 1);
	glBindTexture(GL_TEXTURE_2D, headsArray);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI, width, height, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, zeroArrayForHeadsArray);
}



void GL_GameState::prepareRenderForTransparentObjects()
{
	//0x01 because in this case the stencil buffer is either 0 or 1. (stencil buffer is typically 8-bits, 0 to 255, so our mask could be 0xFF)
	//if a transparent fragment passes the depth test, give the stencil buffer a value of 1
	
	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_ALWAYS, 0x01, 0x01);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	Singleton<GL_ShaderState>::Instance()->setTransparencyOn();
	glDisable(GL_CULL_FACE);
	glDepthMask(GL_FALSE);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	//refresh the atomic counter to 0
	glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, nodeCounter);
	glm::uint32* pointer = (glm::uint32*)glMapBufferRange(
			GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(glm::uint32),
			GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
	
	*pointer = 0;
	glUnmapBuffer(GL_ATOMIC_COUNTER_BUFFER);
	

	//fill the heads array with zeros
	glActiveTexture(GL_TEXTURE0 + 1);
	glBindTexture(GL_TEXTURE_2D, headsArray);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RED_INTEGER, GL_UNSIGNED_INT, zeroArrayForHeadsArray);
	glBindImageTexture(1, headsArray, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI); 

	//setup the globals data
	glBindImageTexture(2, globalsDataTextureBuffer, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32UI);
	

}

void GL_GameState::finalizeTransparency()
{
	/*glm::uint32 counterAmount;
	glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, nodeCounter);
	glGetBufferSubData(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(glm::uint32), &counterAmount);
	if(counterAmount > 0)
	{
		std::cout << counterAmount << std::endl;
	}*/

	//when stencil buffer is equal to 1, it passes
	glStencilFunc(GL_EQUAL, 0x01, 0x01);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glEnable(GL_BLEND);
	Singleton<GL_ShaderState>::Instance()->useProgram(Singleton<GL_ShaderState>::Instance()->TRANSPARENCY_RESOLVE_PROGRAM);
	screenCoveringQuad->render();
	glDisable(GL_BLEND);

	Singleton<GL_ShaderState>::Instance()->setTransparencyOff();
	glEnable(GL_CULL_FACE);
	glDepthMask(GL_TRUE);
	glDisable(GL_STENCIL_TEST);
}







GL_GameState::~GL_GameState(void)
{
	delete[] zeroArrayForHeadsArray;
}
