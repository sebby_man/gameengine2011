#include "GL_ShaderState.h"

GL_ShaderState::GL_ShaderState(void)
{


    currentProgram = 0;

    //--------------------------------------
    // Create shaders
    //--------------------------------------

    std::vector<GLuint> shaderList;

    //creates no light shader
    shaderList.push_back(loadShader(GL_VERTEX_SHADER, "data/shaders/NoLighting.vert"));
    shaderList.push_back(loadShader(GL_FRAGMENT_SHADER, "data/shaders/ColorPassthrough.frag"));

    NO_LIGHT_PROGRAM = new NoColorProgram(createProgram(shaderList));
	namedProgramsMap["NO_LIGHT_PROGRAM"] = NO_LIGHT_PROGRAM;
    shaderList.clear();

	std::string fragShader = "data/shaders/FragmentLighting.frag";
	if(TRANSPARENCY_ENABLED) fragShader = "data/shaders/FragmentLightingTransparency.frag";
	
    //create the diffuse light phong shader
    shaderList.push_back(loadShader(GL_VERTEX_SHADER, "data/shaders/FragmentLighting.vert"));
    shaderList.push_back(loadShader(GL_FRAGMENT_SHADER, fragShader));

    GAUSSIAN_FRAG_LIGHTING_PROGRAM = new GaussianFragLightingProgram(createProgram(shaderList));
	namedProgramsMap["GAUSSIAN_FRAG_LIGHTING_PROGRAM"] = GAUSSIAN_FRAG_LIGHTING_PROGRAM;
	shaderList.clear();
	
	//create the animation shader
    shaderList.push_back(loadShader(GL_VERTEX_SHADER, "data/shaders/AnimationVert.vert"));
    shaderList.push_back(loadShader(GL_FRAGMENT_SHADER, fragShader));

    ANIMATION_PROGRAM = new AnimationProgram(createProgram(shaderList));
	namedProgramsMap["ANIMATION_PROGRAM"] = ANIMATION_PROGRAM;
	shaderList.clear();

	//create the transparency resolve shader
	if(TRANSPARENCY_ENABLED)
	{
		shaderList.push_back(loadShader(GL_VERTEX_SHADER, "data/shaders/TransparencyResolve.vert"));
		shaderList.push_back(loadShader(GL_FRAGMENT_SHADER, "data/shaders/TransparencyResolve.frag"));

		TRANSPARENCY_RESOLVE_PROGRAM = new TransparencyResolveProgram(createProgram(shaderList));
		namedProgramsMap["TRANSPARENCY_RESOLVE_PROGRAM"] = ANIMATION_PROGRAM;
		shaderList.clear();
	}


	//add to name map
	
	
	

    //--------------------------------------
    // Set uniform block data
    //--------------------------------------

    //set binding point locations
    projectionBlockBindingIndex = 0;
    lightBlockBindingIndex = 1;
    materialBlockBindingIndex = 2;

    //create Projection UBO
    glGenBuffers(1, &projectionUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, projectionUBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(ProjectionBlock), 0, GL_DYNAMIC_DRAW);

    //create GL_Lighting UBO
    glGenBuffers(1, &lightsUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, lightsUBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(GL_Lighting), 0, GL_STREAM_DRAW);

	glGenBuffers(1, &materialsUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, materialsUBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(MaterialBlock), 0, GL_STREAM_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);


    //tell the binding point locations to point to the static sized UBO's (note: materialsUBO is set in instantiateMaterials).
    glBindBufferBase(GL_UNIFORM_BUFFER, projectionBlockBindingIndex, projectionUBO);
    glBindBufferBase(GL_UNIFORM_BUFFER, lightBlockBindingIndex, lightsUBO);
	glBindBufferBase(GL_UNIFORM_BUFFER, materialBlockBindingIndex, materialsUBO);

    //tell the uniform blocks in the programs to point to binding points in the context (DO THIS FOR EVERY PROGRAM THAT USES THE UNIFORM for loop might be appropriate);

    //GLuint projectionBlockIndex = glGetUniformBlockIndex(DIFFUSE_LIGHT_PHONG_PROGRAM->theProgram, "Projection");
    //glUniformBlockBinding(DIFFUSE_LIGHT_PHONG_PROGRAM->theProgram, projectionBlockIndex, projectionBlockBindingIndex);

    //GLuint lightBlockIndex = glGetUniformBlockIndex(DIFFUSE_LIGHT_PHONG_PROGRAM->theProgram, "Light");
    //glUniformBlockBinding(DIFFUSE_LIGHT_PHONG_PROGRAM->theProgram, lightBlockIndex, lightBlockBindingIndex);

    //GLuint materialBlockIndex = glGetUniformBlockIndex(DIFFUSE_LIGHT_PHONG_PROGRAM->theProgram, "Material");
    //glUniformBlockBinding(DIFFUSE_LIGHT_PHONG_PROGRAM->theProgram, materialBlockIndex, materialBlockBindingIndex);



	
}

GL_ShaderProgram* GL_ShaderState::findShaderProgram(std::string name)
{
	std::map<std::string, GL_ShaderProgram*>::iterator found = namedProgramsMap.find(name);
	if(found != namedProgramsMap.end())
	{
		return found->second;
	}
	else
	{
		std::cout << "Shader Program " << name << " not found" << std::endl;
		return 0;
	}
}
/*void GL_ShaderState::instantiateMaterials(std::vector<MaterialBlock> materials)
{
    
    unsigned int numMaterials = materials.size();
    //request the alignment value
    int uniformBufferAlignSize = 0;
    glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &uniformBufferAlignSize);

    //get the correct size of each block
    sizeMaterialBlock = sizeof(MaterialBlock);
    sizeMaterialBlock += uniformBufferAlignSize - (sizeMaterialBlock % uniformBufferAlignSize);

    int sizeMaterialUniformBuffer = sizeMaterialBlock * numMaterials;

    //fill mtlBuffer with data, point to data with bufferPtr. The purpose of this code is to avoid allocating memory with 'new'
    std::vector<GLubyte> mtlBuffer;
    mtlBuffer.resize(sizeMaterialUniformBuffer, 0);

    GLubyte *bufferPtr = &mtlBuffer[0];

    for(size_t mtl = 0; mtl < numMaterials; ++mtl)
        memcpy(bufferPtr + (mtl * sizeMaterialBlock), &materials[mtl], sizeof(MaterialBlock));

    //set data into UBO
    glGenBuffers(1, &materialsUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, materialsUBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeMaterialUniformBuffer, bufferPtr, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

}*/

void GL_ShaderState::setForRender(GL_ShaderProgram* shaderProgram, MaterialBlock& material, glm::mat4& modelToWorld, bool uniformScale)
{
	
	glBindBuffer(GL_UNIFORM_BUFFER, materialsUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(MaterialBlock), &material); 
    
	useProgram(shaderProgram);
	Singleton<GL_ShaderStateData>::Instance()->uniformScale = uniformScale;
	Singleton<GL_ShaderStateData>::Instance()->currModelToWorld = modelToWorld;
    shaderProgram->setShaderForRender();
}

void GL_ShaderState::setJointMatrices(glm::mat4* jointMatrices, int numJoints)
{
	Singleton<GL_ShaderStateData>::Instance()->jointMatrices = jointMatrices;
	Singleton<GL_ShaderStateData>::Instance()->numJoints = numJoints;
}

void GL_ShaderState::setWorldToCamera(glm::mat4& worldToCamera)
{
    Singleton<GL_ShaderStateData>::Instance()->currWorldToCameraMatrix = worldToCamera;
}
void GL_ShaderState::setCameraToClip(glm::mat4& cameraToClip)
{
    //fill UBO
    glBindBuffer(GL_UNIFORM_BUFFER, projectionUBO);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(ProjectionBlock), glm::value_ptr(cameraToClip));
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    Singleton<GL_ShaderStateData>::Instance()->currCameraToClipMatrix = cameraToClip;
}


void GL_ShaderState::setLightData(GL_Lighting& lightData, int numLightsUsed)
{
    //make sure to send the light pos in lightData is in camera space, using the DIFFUSE_LIGHT_PHONG_SHADER is fine. But this means
    //that this function has to be called after setShaderWithModelToWorld

	//fill UBO
    glBindBuffer(GL_UNIFORM_BUFFER, lightsUBO);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(GL_Lighting), &lightData);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

	Singleton<GL_ShaderStateData>::Instance()->numLightsUsed = numLightsUsed;

}


void GL_ShaderState::setTransparencyOff()
{
	Singleton<GL_ShaderStateData>::Instance()->transparent = false;
}
void GL_ShaderState::setTransparencyOn()
{
	Singleton<GL_ShaderStateData>::Instance()->transparent = true;	
}

void GL_ShaderState::useProgram(GL_ShaderProgram* shaderProgram)
{
    //if the shader program is currently in use, don't re-bind it
    if(shaderProgram != currentProgram)
    {
        currentProgram = shaderProgram;

        if(shaderProgram == 0)
        {
            glUseProgram(0);
        }
        else
        {
            glUseProgram(shaderProgram->theProgram);
        }
    }
}

const char* GL_ShaderState::getShaderName(GLenum eShaderType)
{
    switch(eShaderType)
    {
    case GL_VERTEX_SHADER:
        return "vertex";
        break;
    case GL_GEOMETRY_SHADER:
        return "geometry";
        break;
    case GL_FRAGMENT_SHADER:
        return "fragment";
        break;
    }

    return 0;
}


GLuint GL_ShaderState::createShader(GLenum eShaderType, const std::string &strShaderFile, const std::string &strShaderName)
{
    GLuint shader = glCreateShader(eShaderType);
    const char *strFileData = strShaderFile.c_str();
    glShaderSource(shader, 1, (const GLchar**)&strFileData, NULL);

    glCompileShader(shader);

    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE)
    {
        GLint infoLogLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shader, infoLogLength, NULL, strInfoLog);

        fprintf(stderr, "Compile failure in %s shader named \"%s\". Error:\n%s\n",
                getShaderName(eShaderType), strShaderName.c_str(), strInfoLog);
        delete[] strInfoLog;
    }

    return shader;
}

GLuint GL_ShaderState::loadShader(GLenum eShaderType, const std::string &strShaderFilename)
{


    std::string filePath = Utils::getFilePath(strShaderFilename);

    std::ifstream shaderFile(filePath.c_str());
    if(!shaderFile.is_open())
    {
        fprintf(stderr, "Cannot load the shader file \"%s\" for the %s shader.\n",
                filePath.c_str(), getShaderName(eShaderType));
        return 0;
    }
    std::stringstream shaderData;
    shaderData << shaderFile.rdbuf();
    shaderFile.close();

    return createShader(eShaderType, shaderData.str(), strShaderFilename);
}

GLuint GL_ShaderState::createProgram(const std::vector<GLuint> &shaderList)
{
    GLuint program = glCreateProgram();

    for(size_t iLoop = 0; iLoop < shaderList.size(); iLoop++)
        glAttachShader(program, shaderList[iLoop]);

    glLinkProgram(program);

    GLint status;
    glGetProgramiv (program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        GLint infoLogLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
        fprintf(stderr, "Linker failure: %s\n", strInfoLog);
        delete[] strInfoLog;
    }

    return program;
}







GL_ShaderState::~GL_ShaderState(void)
{

}
