#pragma once

#include <gl3w/gl3w.h>
#include <vector>
#include "GL_ShaderState.h"
#include "../Raytracing/Face.h"
#include <algorithm>
#include "GL_GameState.h"


//32 bits
struct Vertex
{
	float x, y, z;
	float nx, ny, nz;
	float s, t;
};

//96 bits
struct VertexSkeleton
{
	float x, y, z;
	float nx, ny, nz;
	float s, t;
	float weights[8];
	unsigned short indexes[8];
	unsigned short numJoints;
	unsigned short padding[7]; // to align to 32 bytes

};



class GL_MeshPrimitive
{
public:

    void prepareForRender();

    void setDimensions(float widthInput, float heightInput, float depthInput);
    float width;
    float height;
    float depth;

	bool hasDiffuseColorTexture;
	bool hasAlphaTexture;
	int numElements;

    void render();

    GL_MeshPrimitive(unsigned int numVertices, Vertex* vertexData, bool containsPositions, bool containsNormals, bool containsUVs, std::string& textureFileName, std::vector<GLushort>& indexArray, GLenum primitiveType, GLenum usage);
	GL_MeshPrimitive(unsigned int numVertices, VertexSkeleton* vertexData, bool containsPositions, bool containsNormals, bool containsUVs, std::string& textureFileName, std::vector<GLushort>& indexArray, GLenum primitiveType, GLenum usage);
	virtual ~GL_MeshPrimitive(void);

	std::vector<Face*> faceList;



protected:

	void loadTextures(std::string& textureFileName);

    GLuint vao;
    GLuint vertexBufferObject;
    GLuint indexBufferObject;
    GLenum primitiveToDraw;

	GLuint mainTexture;




};

