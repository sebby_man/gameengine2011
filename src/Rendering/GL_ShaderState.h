#pragma once

#include <gl3w/gl3w.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <string.h>
#include <iostream>
#include "../Utils/FileUtils.h"
#include "GL_ShaderProgram.h"
#include "../Singleton.h"
#include "GL_MeshPrimitive.h"
#include "GL_ShaderStateData.h"
#include <map>
#include <gli/gli.hpp>
#include <gli/gli.hpp>
#include <gli/gtx/fetch.hpp>
#include <gli/gtx/gradient.hpp>
#include <gli/gtx/loader.hpp>


#define MAX_NUMBER_OF_LIGHTS 8


/*---------------------------------------------------------------------
	Uniform block c++ representations of the GLSL uniform blocks
----------------------------------------------------------------------*/

	struct ProjectionBlock
	{
		glm::mat4 cameraToClipMatrix;
	};

	struct GL_Light
	{
		glm::vec4 cameraSpaceLightPos;
		glm::vec4 lightIntensity;
		float lightAttenuation;
		float padding[3];
	};

	

	struct GL_Lighting
	{
		GL_Lighting()
		{
			ambientIntensity = glm::vec4(0.01f,0.01f,0.01f,0.01f);
			maxIntensity = 1.0f;
			gamma = 2.2f;
		}

		glm::vec4 ambientIntensity;
		float maxIntensity;
		float gamma;
		float padding[2];
		GL_Light lights[MAX_NUMBER_OF_LIGHTS];
	};

	struct MaterialBlock
	{
		glm::vec4 diffuseColor;
		glm::vec4 specularColor;
		float specularShininess;
		float transparency;
		int getColorFromTexture;
		float padding;
	};




class GL_ShaderState
{
public:
	
	static enum AttributeIndex
	{
		POSITIONS = 0,
		TEXCOORDS = 1,
		NORMALS = 2,
		JOINT_WEIGHTS = 3,
		JOINT_INDEXES = 5,
		NUM_JOINTS = 7,
		

	};

	/*------------------------------
		The shader programs
	-------------------------------*/
	GL_ShaderProgram* findShaderProgram(std::string name);
	AnimationProgram* ANIMATION_PROGRAM;
    GaussianFragLightingProgram* GAUSSIAN_FRAG_LIGHTING_PROGRAM;
    NoColorProgram* NO_LIGHT_PROGRAM;
	TransparencyResolveProgram* TRANSPARENCY_RESOLVE_PROGRAM;

	GLuint transparencyResolveProgram;


    void setForRender(GL_ShaderProgram* shaderProgram, MaterialBlock& material, glm::mat4& modelToWorld, bool uniformScale);
    void setJointMatrices(glm::mat4* jointMatrices, int numJoints);
	void setWorldToCamera(glm::mat4& newWorldToCamera);
    void setCameraToClip(glm::mat4& newCameraToClip);
    void setLightData(GL_Lighting& lightData, int numLightsUsed);
	void setTransparencyOn();
	void setTransparencyOff();
    void useProgram(GL_ShaderProgram* shaderProgram);

    //void instantiateMaterials(std::vector<MaterialBlock> materials);
    GL_ShaderState();
    virtual ~GL_ShaderState(void);

    

protected:


    //UBO's
    GLuint projectionUBO;
    GLuint projectionBlockBindingIndex;

    GLuint lightsUBO;
    GLuint lightBlockBindingIndex;

    GLuint materialsUBO;
    GLuint materialBlockBindingIndex;

	std::map<std::string, GL_ShaderProgram*> namedProgramsMap;
    GL_ShaderProgram* currentProgram;

    GLuint createProgram(const std::vector<GLuint> &shaderList);
    GLuint loadShader(GLenum eShaderType, const std::string &strShaderFilename);
    GLuint createShader(GLenum eShaderType, const std::string &strShaderFile, const std::string &strShaderName);
    const char* getShaderName(GLenum eShaderType);


};
