#include "GL_MeshLibrary.h"



GL_MeshLibrary::GL_MeshLibrary(void)
{

}


GL_MeshPrimitive* GL_MeshLibrary::getBoundingBoxMesh(float width, float height, float depth)
{
    float halfWidth = width/2;
    float halfHeight = height/2;
    float halfDepth = depth/2;

    float vertices[] =
    {
        -halfWidth, -halfHeight, +halfDepth,
        -halfWidth, +halfHeight, +halfDepth,
        +halfWidth, +halfHeight, +halfDepth,
        +halfWidth, -halfHeight, +halfDepth,
        -halfWidth, +halfHeight, -halfDepth,
        +halfWidth, +halfHeight, -halfDepth,
        +halfWidth, -halfHeight, -halfDepth,
        -halfWidth, -halfHeight, -halfDepth,
    };


    unsigned short indexes[] =
    {
        0, 1,
        1, 2,
        2, 3,
        3, 0,

        4, 5,
        5, 6,
        6, 7,
        7, 4,

        6, 3,
        2, 5,

        7, 0,
        1, 4,
    };

    std::vector<GLfloat> vertexData = std::vector<GLfloat>(vertices, vertices + sizeof(vertices) / sizeof(float));
    std::vector<GLushort> indexData = std::vector<GLushort>(indexes, indexes + sizeof(indexes) / sizeof(unsigned short));


    /*GL_MeshPrimitive* boundingBoxPrim = new GL_MeshPrimitive();
    boundingBoxPrim->setPositions(vertexData);
    //AABBprim->setColors(colorData);
    boundingBoxPrim->appendElementArray(indexData);
    boundingBoxPrim->setPrimitiveType(GL_LINES);
    boundingBoxPrim->prepareForRender();

    return boundingBoxPrim;*/

	return 0;

}

GL_MeshPrimitive* GL_MeshLibrary::getLineSegmentMesh(glm::vec3 start, glm::vec3 end)
{
    std::vector<GLfloat> vertexData;
    vertexData.push_back(start.x);
    vertexData.push_back(start.y);
    vertexData.push_back(start.z);

    vertexData.push_back(end.x);
    vertexData.push_back(end.y);
    vertexData.push_back(end.z);


    std::vector<GLushort> indexData;

    indexData.push_back(0);
    indexData.push_back(1);


    /*GL_MeshPrimitive* meshPrim = new GL_MeshPrimitive();
    meshPrim->setPositions(vertexData);
    meshPrim->appendElementArray(indexData);
    meshPrim->setPrimitiveType(GL_LINES);
    meshPrim->prepareForRender();

    return meshPrim;*/

	return 0;
}


GL_MeshLibrary::~GL_MeshLibrary(void)
{

}
