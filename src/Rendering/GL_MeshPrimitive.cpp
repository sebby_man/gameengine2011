#include "GL_MeshPrimitive.h"

GL_MeshPrimitive::GL_MeshPrimitive(unsigned int numVertices, Vertex* vertexData, bool containsPositions, bool containsNormals, bool containsUVs, std::string& textureFileName, std::vector<GLushort>& indexArray, GLenum primitiveType, GLenum usage)
{
	
	//allocated space for vertex buffer object
	int vertexSize = sizeof(Vertex);
	int vertexBufferSize = vertexSize * numVertices;


    //create and bind array buffer, set data
    glGenBuffers(1, &vertexBufferObject);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, vertexBufferSize, vertexData, usage);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

	numElements = indexArray.size();
	size_t indexBufferSize = numElements * sizeof(GLushort);

    //create and bind element array buffer, set data to the stored element array, then close buffer
    glGenBuffers(1, &indexBufferObject);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBufferSize, &indexArray[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


    //create and bind vao
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    //bind array buffer again
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);

	//enable attribute positions (position is always enabled)
	if(containsPositions) glEnableVertexAttribArray(GL_ShaderState::POSITIONS);
	if(containsNormals) glEnableVertexAttribArray(GL_ShaderState::NORMALS);
	if(containsUVs) glEnableVertexAttribArray(GL_ShaderState::TEXCOORDS);


	int offset = 0;
	

	//set position attrib pointer
	glVertexAttribPointer(GL_ShaderState::POSITIONS, 3, GL_FLOAT, GL_FALSE, vertexSize, (void*)(offset));
	offset += sizeof(float)*3;

	//set normals attrib pointer
	glVertexAttribPointer(GL_ShaderState::NORMALS, 3, GL_FLOAT, GL_FALSE, vertexSize, (void*)(offset));
	offset += sizeof(float)*3;

	//set textures attrib pointer
	glVertexAttribPointer(GL_ShaderState::TEXCOORDS, 2, GL_FLOAT, GL_FALSE, vertexSize, (void*)(offset));
	offset += sizeof(float)*2;

    //bind element array
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);

	primitiveToDraw = primitiveType;

    //unbind vao, vbo, and element array
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	delete[] vertexData;

	if(containsUVs)
	{
		loadTextures(textureFileName);
		this->hasDiffuseColorTexture = true;
	}
	else
	{
		this->hasDiffuseColorTexture = false;
		this->hasAlphaTexture = false;
	}






}


GL_MeshPrimitive::GL_MeshPrimitive(unsigned int numVertices, VertexSkeleton* vertexData, bool containsPositions, bool containsNormals, bool containsUVs, std::string& textureFileName, std::vector<GLushort>& indexArray, GLenum primitiveType, GLenum usage)
{
	int vertexSize = sizeof(VertexSkeleton);
	int vertexBufferSize = vertexSize * numVertices;

    //create and bind array buffer, set data to null initially
    glGenBuffers(1, &vertexBufferObject);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, vertexBufferSize, vertexData, usage);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

	numElements = indexArray.size();
	size_t indexBufferSize = numElements * sizeof(GLushort);

    //create and bind element array buffer, set data to the stored element array, then close buffer
    glGenBuffers(1, &indexBufferObject);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBufferSize, &indexArray[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


    //create and bind vao
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    //bind array buffer again
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);

	if(containsPositions) glEnableVertexAttribArray(GL_ShaderState::POSITIONS);
	if(containsNormals) glEnableVertexAttribArray(GL_ShaderState::NORMALS);
	if(containsUVs) glEnableVertexAttribArray(GL_ShaderState::TEXCOORDS);

	glEnableVertexAttribArray(GL_ShaderState::JOINT_WEIGHTS);
	glEnableVertexAttribArray(GL_ShaderState::JOINT_WEIGHTS+1);
	glEnableVertexAttribArray(GL_ShaderState::JOINT_INDEXES);
	glEnableVertexAttribArray(GL_ShaderState::JOINT_INDEXES+1);
	glEnableVertexAttribArray(GL_ShaderState::NUM_JOINTS);
	

	int offset = 0;
	
	//set position attrib pointer
	glVertexAttribPointer(GL_ShaderState::POSITIONS, 3, GL_FLOAT, GL_FALSE, vertexSize, (void*)(offset));
	offset += sizeof(float)*3;
	
	//set normals attrib pointer
	glVertexAttribPointer(GL_ShaderState::NORMALS, 3, GL_FLOAT, GL_FALSE, vertexSize, (void*)(offset));
	offset += sizeof(float)*3;

	//set texture attrib pointer
	glVertexAttribPointer(GL_ShaderState::TEXCOORDS, 2, GL_FLOAT, GL_FALSE, vertexSize, (void*)(offset));
	offset += sizeof(float)*2;

	//set jointWeights attrib pointer (part 1)
	glVertexAttribPointer(GL_ShaderState::JOINT_WEIGHTS, 4, GL_FLOAT, GL_FALSE, vertexSize, (void*)(offset));
	offset += sizeof(float)*4;

	//set jointWeights attrib pointer (part 1)
	glVertexAttribPointer(GL_ShaderState::JOINT_WEIGHTS + 1, 4, GL_FLOAT, GL_FALSE, vertexSize, (void*)(offset));
	offset += sizeof(float)*4;

	//set jointIndexes attrib pointer (part 1)
	glVertexAttribIPointer(GL_ShaderState::JOINT_INDEXES, 4, GL_UNSIGNED_SHORT, vertexSize, (void*)(offset));
	offset += sizeof(unsigned short)*4;

	//set jointIndexes attrib pointer (part 2)
	glVertexAttribIPointer(GL_ShaderState::JOINT_INDEXES + 1, 4, GL_UNSIGNED_SHORT, vertexSize, (void*)(offset));
	offset += sizeof(unsigned short)*4;

	//set numJoints attrib pointer
	glVertexAttribIPointer(GL_ShaderState::NUM_JOINTS, 1, GL_UNSIGNED_SHORT, vertexSize, (void*)(offset));
	offset += sizeof(unsigned short);

	
    //bind element array
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);

	primitiveToDraw = primitiveType;

    //unbind vao, vbo, and element array
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	delete[] vertexData;

	if(containsUVs)
	{
		loadTextures(textureFileName);
		this->hasDiffuseColorTexture = true;
	}
	else
	{
		this->hasDiffuseColorTexture = false;
		this->hasAlphaTexture = false;
	}
}


void GL_MeshPrimitive::loadTextures(std::string& textureFileName)
{	
	/*------------------------------
		load the main RGBA texture
	---------------------------------*/
	gli::texture2D loadedSkinTexture = gli::load(Utils::getFilePath("data/images/" + textureFileName));
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, &mainTexture);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mainTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, GLint(loadedSkinTexture.levels()-1));
	
	GLenum glFormatSource;
	GLenum glFormatInternal;
	gli::format textureFormat = loadedSkinTexture.format();
	if(textureFormat == gli::RGB8U)
	{
		glFormatSource = GL_RGB;
		glFormatInternal = GL_RGB8;
		this->hasAlphaTexture = false;
	}
	else if (textureFormat == gli::RGBA8U)
	{
		glFormatSource = GL_RGBA;
		glFormatInternal = GL_RGBA8;
		this->hasAlphaTexture = true;
	}

	glTexStorage2D(GL_TEXTURE_2D, GLint(loadedSkinTexture.levels()), glFormatInternal, GLsizei(loadedSkinTexture[0].dimensions().x), GLsizei(loadedSkinTexture[0].dimensions().y));

	for(gli::texture2D::level_type Level = 0; Level < loadedSkinTexture.levels(); ++Level)
	{
		glTexSubImage2D(
			GL_TEXTURE_2D, 
			GLint(Level), 
			0, 0, 
			GLsizei(loadedSkinTexture[Level].dimensions().x), 
			GLsizei(loadedSkinTexture[Level].dimensions().y), 
			glFormatSource, GL_UNSIGNED_BYTE, 
			loadedSkinTexture[Level].data());
	}
	
	glBindTexture(GL_TEXTURE_2D, 0);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
}

void GL_MeshPrimitive::setDimensions(float widthInput, float heightInput, float depthInput)
{
    width = widthInput;
    height = heightInput;
    depth = depthInput;
}


void GL_MeshPrimitive::prepareForRender(void)
{

    

}


void GL_MeshPrimitive::render()
{
	//TODO: move to different spot

	//set texture for render
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mainTexture);


    glBindVertexArray(vao);
    glDrawElements(primitiveToDraw, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
}









GL_MeshPrimitive::~GL_MeshPrimitive(void)
{
}
