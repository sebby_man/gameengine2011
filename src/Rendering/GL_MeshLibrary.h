#pragma once

#include "GL_MeshPrimitive.h"
#include "../Utils/FileUtils.h"
#include "../Singleton.h"
#include "../Utils/MathUtils.h"
#include "../Transformations.h"

class GL_MeshLibrary
{
public:



    GL_MeshPrimitive* getBoundingBoxMesh(float width, float height, float depth);
    GL_MeshPrimitive* getLineSegmentMesh(glm::vec3 start, glm::vec3 end);

    GL_MeshLibrary();
    virtual ~GL_MeshLibrary(void);

};
