#pragma once

#include <gl3w/gl3w.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "GL_ShaderStateData.h"
#include "../Singleton.h"
#include "../GameConfig.h"

struct GL_ShaderProgram
{
    GLuint theProgram;
    GL_ShaderProgram(GLuint shader)
    {
        theProgram = shader;
    }
    virtual void setShaderForRender() = 0;
};


struct TransparencyResolveProgram : public GL_ShaderProgram
{

    TransparencyResolveProgram(GLuint shader) : GL_ShaderProgram(shader){ };

    void setShaderForRender(){ }
};

struct NoColorProgram : public GL_ShaderProgram
{
    GLuint modelToClipMatrixUnif;

    NoColorProgram(GLuint shader) : GL_ShaderProgram(shader)
    {
        modelToClipMatrixUnif = glGetUniformLocation(theProgram, "modelToClipMatrix");
    };

    void setShaderForRender()
    {
		glm::mat4 modelToClip = Singleton<GL_ShaderStateData>::Instance()->currCameraToClipMatrix*Singleton<GL_ShaderStateData>::Instance()->currWorldToCameraMatrix*Singleton<GL_ShaderStateData>::Instance()->currModelToWorld;
        glUniformMatrix4fv(modelToClipMatrixUnif, 1, GL_FALSE, glm::value_ptr(modelToClip));
    }
};



struct GaussianFragLightingProgram : public GL_ShaderProgram
{


    //in vert
    GLuint modelToCameraMatrixUnif;
    GLuint normalsModelToCameraMatrixUnif;
	//in frag
    GLuint numLightsUsedUnif;
	GLuint opaqueProcessingIndex; 
	GLuint transparentProcessingIndex;

    GaussianFragLightingProgram(GLuint shader) : GL_ShaderProgram(shader)
    {
        modelToCameraMatrixUnif = glGetUniformLocation(theProgram, "modelToCameraMatrix");
        normalsModelToCameraMatrixUnif = glGetUniformLocation(theProgram, "normalModelToCameraMatrix");
        numLightsUsedUnif = glGetUniformLocation(theProgram, "numLightsUsed");
		
		if(TRANSPARENCY_ENABLED)
		{
			opaqueProcessingIndex = glGetSubroutineIndex(theProgram, GL_FRAGMENT_SHADER, "processOpacity"); 
			transparentProcessingIndex = glGetSubroutineIndex(theProgram, GL_FRAGMENT_SHADER, "processTransparency"); 	
		}
	};

    void setShaderForRender()
    {
        //set model to camera
		glm::mat4 modelToCamera = Singleton<GL_ShaderStateData>::Instance()->currWorldToCameraMatrix*Singleton<GL_ShaderStateData>::Instance()->currModelToWorld;
        glUniformMatrix4fv(modelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr(modelToCamera));

        //set normals
        glm::mat3 normalsModelToCamera(modelToCamera);
		if(!Singleton<GL_ShaderStateData>::Instance()->uniformScale)
        {
            normalsModelToCamera = glm::transpose(glm::inverse(normalsModelToCamera));
        }
        glUniformMatrix3fv(normalsModelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr(normalsModelToCamera));

		//set numLights (TODO: maybe this should be part of the light UBO but find out how padding will work with it)
		glUniform1i(numLightsUsedUnif, Singleton<GL_ShaderStateData>::Instance()->numLightsUsed);
		
		if(TRANSPARENCY_ENABLED)
		{
			if(Singleton<GL_ShaderStateData>::Instance()->transparent)
				glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &transparentProcessingIndex);
			else 
				glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &opaqueProcessingIndex);
		}
	}


};

struct AnimationProgram : public GL_ShaderProgram
{
	//in vert
	GLuint jointMatricesUnif;
	GLuint worldToCameraMatrixUnif;
    GLuint normalsWorldToCameraMatrixUnif;

	//in frag
    GLuint numLightsUsedUnif;
	GLuint opaqueProcessingIndex; 
	GLuint transparentProcessingIndex;


    AnimationProgram(GLuint shader) : GL_ShaderProgram(shader)
    {
        jointMatricesUnif = glGetUniformLocation(theProgram, "jointMatrices");
		worldToCameraMatrixUnif = glGetUniformLocation(theProgram, "worldToCameraMatrix");
        normalsWorldToCameraMatrixUnif = glGetUniformLocation(theProgram, "normalWorldToCameraMatrix");
        numLightsUsedUnif = glGetUniformLocation(theProgram, "numLightsUsed");
		if(TRANSPARENCY_ENABLED)
		{
			opaqueProcessingIndex = glGetSubroutineIndex(theProgram, GL_FRAGMENT_SHADER, "processOpacity"); 
			transparentProcessingIndex = glGetSubroutineIndex(theProgram, GL_FRAGMENT_SHADER, "processTransparency"); 	
		}
	};

    void setShaderForRender()
    {

		//set world to camera
		glm::mat4 worldToCamera = Singleton<GL_ShaderStateData>::Instance()->currWorldToCameraMatrix;
        glUniformMatrix4fv(worldToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr(worldToCamera));

		//set normals model to camera
		glm::mat3 normalsWorldToCamera(worldToCamera);
		if(!Singleton<GL_ShaderStateData>::Instance()->uniformScale)
        {
            normalsWorldToCamera = glm::transpose(glm::inverse(normalsWorldToCamera));
        }
        glUniformMatrix3fv(normalsWorldToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr(normalsWorldToCamera));

		//set numLights (TODO: maybe this should be part of the light UBO but find out how padding will work with it)
		glUniform1i(numLightsUsedUnif, Singleton<GL_ShaderStateData>::Instance()->numLightsUsed);


		glm::mat4* jointMatrices = Singleton<GL_ShaderStateData>::Instance()->jointMatrices;
		int numJoints = Singleton<GL_ShaderStateData>::Instance()->numJoints;
		glUniformMatrix4fv(jointMatricesUnif, numJoints, GL_FALSE, glm::value_ptr(jointMatrices[0]));
		delete[] jointMatrices;

		if(TRANSPARENCY_ENABLED)
		{
			if(Singleton<GL_ShaderStateData>::Instance()->transparent)
				glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &transparentProcessingIndex);
			else 
				glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &opaqueProcessingIndex);
		}
    }


};
