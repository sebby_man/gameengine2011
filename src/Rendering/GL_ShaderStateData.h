#pragma once
#include <glm/glm.hpp>




struct GL_ShaderStateData
{

	
	//TODO: is it best to store these here, might be faster to tell shader the matrices directly, but makes it less nice
	glm::mat4 currModelToWorld;
	bool uniformScale;
	glm::mat4 currWorldToCameraMatrix;
    glm::mat4 currCameraToClipMatrix;
	int numLightsUsed;
	glm::mat4* jointMatrices;
	int numJoints;
	bool transparent;
};