#pragma once
#include <gl3w/gl3w.h>
#include <string>
#include <math.h>
#include "GL_ShaderState.h"
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "../Raytracing/Ray.h"
#include "../Utils/PrintUtils.h"
#include "../Utils/FileUtils.h"
#include "../Utils/MathUtils.h"

class GL_MeshPrimitive;

/*------------------------

------------------------*/

class GL_GameState
{
public:

	//necessary GL commands before rendering
    void prepareRenderForOpaqueObjects(void);
	void prepareRenderForTransparentObjects();
	void finalizeTransparency(void);
	//reshape the GL viewport
    void reshape(int width, int height);

	//returns the world-space ray based on where the user clicks
    Ray getClickedRay(int x, int y);


    GL_GameState();
    virtual ~GL_GameState(void);

    int width;
    int height;

    float fieldOfViewDeg;


protected:


	//for transparency
	void initTransparency();
	void resizeTransparency();
	

	void drawScreenCoveringQuad();
	GL_MeshPrimitive* screenCoveringQuad;
	GLuint transparencyResolveProgram;

	GLuint* zeroArrayForHeadsArray;
	GLuint headsArray;
	GLuint globalsDataBufferObject;
	GLuint globalsDataTextureBuffer;
	GLuint nodeCounter;
	int predictedNumberOfFragments;



    glm::mat4 cameraToClipMatrix;
    float fzNear;
    float fzFar;
    float CalcFrustumScale(float fieldOfViewDegrees);
    float frustumScale;





};
