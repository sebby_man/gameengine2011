#include "SFMLCore.h"



SFMLCore::SFMLCore(void)
{


	//Create the window
	int width = 1024;
	int height = 768;
	title = "Game Engine";
	window = new sf::Window(sf::VideoMode(width, height, 32), title);
	
	window->setActive();
	//window->setVerticalSyncEnabled(true);



	//instantiate the gl side
	Singleton<GL_GameState>::Instance();

	//start the asset loading process
	Singleton<Factory>::Instance()->loadGameObjects();

	//instantiate the game
	Singleton<GameState>::Instance();

	//resize the game
    Singleton<GameState>::Instance()->resizeApp(width, height);

	int numFrames = 0;
	sf::Clock clock;

	//Handle events
	frameCount = 0;
    while (window->isOpen())
    {

		Singleton<InputHandler>::Instance()->enterFrame();
		window->display();
		this->showFPS();


        sf::Event myEvent;
		while (window->pollEvent(myEvent))
        {
			Singleton<InputHandler>::Instance()->processEvent(myEvent);
            
			if(myEvent.type == sf::Event::Resized)
            {

				Singleton<GameState>::Instance()->resizeApp(window->getSize().x, window->getSize().y);
            }
			
			if (myEvent.type == sf::Event::Closed)
            {
                window->close();
            }
        }
    }
}


void SFMLCore::showFPS()
{
	//Show fps
	frameCount++;
	float currentTime = clock.getElapsedTime().asSeconds();
	
	if(currentTime >= 1.0f)
	{	
		//std::ostringstream ss;
		//ss << title << " (fps: " << (frameCount/currentTime) << " )";
		//window->setTitle(ss.str());
		std::cout << frameCount/currentTime << std::endl;

		clock.restart();
		frameCount = 0;
	}
}


SFMLCore::~SFMLCore(void)
{
	//Do nothing
}