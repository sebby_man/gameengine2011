#pragma once

#include <float.h>
#include <iostream>
#include <map>
#include <vector>

#include "GameObjects/CameraObject.h"
#include "GameObjects/CameraFirstPersonObject.h"
#include "GameObjects/CameraThirdPersonObject.h"
#include "Utils/FileUtils.h"
#include "Utils/PrintUtils.h"
#include "Scene.h"
#include "Singleton.h"
#include "InputHandler.h"
#include "Factory.h"
#include "Loader.h"
#include "Physics/PhysicsPrimitives.h"
#include "Physics/PhysicsController.h"
#include "Physics/PhysicsUtils.h"
#include "Rendering/GL_GameState.h"
#include "Raytracing/Ray.h"

#include <SFML/Window/Event.hpp>
#include <boost/filesystem.hpp>


/*---------------------------------------------------------
The World acts as a container for all of the game objects,
sort of like a scene graph with additional control
---------------------------------------------------------*/
class World
{
public:

	World();
    ~World();

	//Initializers
	void initScenes();
	void initListeners();

	//Removers
	void removeListeners();

	//Update all the GameObjects. Rendering happens here
	void update();

	//Input/Output
    void keyPressed(sf::Event);
	void mousePressed(sf::Event);
	void mouseMoved(sf::Event);
	void mouseScrollWheelMoved(sf::Event);

protected:
	
	Scene* currentScene;
	std::vector<Scene*> scenes;
};

