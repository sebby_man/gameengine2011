#include "World.h"

World::World(void)
{
	initScenes();
	initListeners();
}
World::~World(void)
{
	//TODO: Delete camera here?
	this->removeListeners();
}

/*-------------------------------
	Initializers and Removers
-------------------------------*/
void World::initListeners()
{
	//Event listeners
	Singleton<InputHandler>::Instance()->addInputEventListener(sf::Event::MouseMoved, InputReceiver::from_method<World,&World::mouseMoved>(this));
	Singleton<InputHandler>::Instance()->addInputEventListener(sf::Event::MouseButtonPressed, InputReceiver::from_method<World,&World::mousePressed>(this));
	Singleton<InputHandler>::Instance()->addInputEventListener(sf::Event::MouseWheelMoved, InputReceiver::from_method<World,&World::mouseScrollWheelMoved>(this));
	Singleton<InputHandler>::Instance()->addInputEventListener(sf::Event::KeyPressed, InputReceiver::from_method<World,&World::keyPressed>(this));
}
void World::removeListeners()
{
	Singleton<InputHandler>::Instance()->removeInputEventListener(sf::Event::MouseMoved, InputReceiver::from_method<World,&World::mouseMoved>(this));
	Singleton<InputHandler>::Instance()->removeInputEventListener(sf::Event::MouseButtonPressed, InputReceiver::from_method<World,&World::mousePressed>(this));
	Singleton<InputHandler>::Instance()->removeInputEventListener(sf::Event::MouseWheelMoved, InputReceiver::from_method<World,&World::mouseScrollWheelMoved>(this));
	Singleton<InputHandler>::Instance()->removeInputEventListener(sf::Event::KeyPressed, InputReceiver::from_method<World,&World::keyPressed>(this));
}


//TODO: get rid of this! for testing purposes only
AnimatedObject* testModel;


void World::initScenes()
{

	/*
	//create the most basic lighting, where the light is attached to the camera
	GL_Lighting lightingGL;
	GL_Light lightGL;

	LightObject* lightObject = Factory::lightObject->getCopy();
	lightGL.lightIntensity = glm::vec4(lightObject->rgb, 1);
	lightGL.lightAttenuation = lightObject->lightAttenuation;
	lightGL.cameraSpaceLightPos = glm::vec4(0,0,0,1);

	lightingGL.lights[0] = lightGL;
	Singleton<GL_ShaderState>::Instance()->setLightData(lightingGL, 1);
	*/

	//Load some scene files
	Scene* scene1 = Singleton<Loader>::Instance()->loadScene("data/scenes/scene1.xml");
	this->scenes.push_back(scene1);


	//target the alien for animation (temporary)
	GameObject* alien = scene1->findGameObject("alien");
	if(alien)
	{
		testModel = dynamic_cast<AnimatedObject*>(alien);
	}
	
	
	/*
	//testing physics stuff - DELETE LATER
	for(int i = 0; i < 100; i++)
	{
		PhysicsObject* thing = Factory::randomThing->getCopy();
		thing->setTranslationY(3.0f*i + 10.0f,false);
		float rand1 = rand()/((float)RAND_MAX);
		float rand2 = rand()/((float)RAND_MAX);
		float rand3 = rand()/((float)RAND_MAX);
		//thing->setScale(glm::vec3(rand1,rand2,rand3),false);
		thing->setTranslationX(rand1, false);

		scene1->addGameObject(thing);
		PhysicsController* controller = new PhysicsController();
		controller->attachTo(thing);
	}
	
	PhysicsObject* floor = Factory::floor->getCopy();
	floor->setScale(glm::vec3(100,.5,100),false);
	scene1->addGameObject(floor);

	*/
	/*PhysicsObject* wall1 = Factory::floor->getCopy();
	wall1->setScale(glm::vec3(.5,7.5,10),false);
	wall1->translateX(-9.5, false);
	scene1->addGameObject(wall1);

	PhysicsObject* wall2 = Factory::floor->getCopy();
	wall2->setScale(glm::vec3(.5,7.5,10),false);
	wall2->translateX(9.5, false);
	scene1->addGameObject(wall2);

	PhysicsObject* wall3 = Factory::floor->getCopy();
	wall3->setScale(glm::vec3(10,7.5,.5),false);
	wall3->translateZ(-9.5, false);
	scene1->addGameObject(wall3);

	PhysicsObject* wall4 = Factory::floor->getCopy();
	wall4->setScale(glm::vec3(10,7.5,.5),false);
	wall4->translateZ(9.5, false);
	scene1->addGameObject(wall4);*/

	//Update currentScene
	this->currentScene = scene1;

}


/*-----------------------------
		Input/Output 
-----------------------------*/
void World::keyPressed(sf::Event event)
{
	sf::Keyboard::Key keyCode = event.key.code;
	if(keyCode == sf::Keyboard::C)
	{
		std::vector<CameraObject*>::iterator iter = std::find(this->currentScene->getCameras().begin(),this->currentScene->getCameras().end(),this->currentScene->currentCamera);
		if(iter != this->currentScene->getCameras().end())
		{
			++iter;
			if(iter == this->currentScene->getCameras().end())
			{
				iter = this->currentScene->getCameras().begin();
			}
			this->currentScene->currentCamera = *iter;
			this->currentScene->currentCamera->activate();
		}
	}
	else if(keyCode == sf::Keyboard::X)
	{
		TRANSPARENCY_ENABLED = !TRANSPARENCY_ENABLED;
	}
	else if(keyCode == sf::Keyboard::V)
	{
		testModel->visible = !testModel->visible;
	}
}
void World::mousePressed(sf::Event event)
{
	//Throw an object onto the scene
	InputHandler* inputHandler = Singleton<InputHandler>::Instance();
	bool altIsDown = inputHandler->isAltDown();
	if(event.mouseButton.button == sf::Mouse::Right && !altIsDown)
	{
		/*PhysicsObject* projectile = Factory::randomThing->getCopy();

		Ray r = Singleton<GL_GameState>::Instance()->getClickedRay(event.MouseButton.X,event.MouseButton.Y);

		float speed = 2000.0f;
		btVector3 velocity = Utils::convertGLMVectorToBullet(r.direction*speed);

		float spinAmount = 20.0f;
		btVector3 spin = btVector3(1,1,1)*spinAmount;
		glm::vec3 pos = this->currentScene->currentCamera->getCameraPos() + this->currentScene->currentCamera->getLookDir()*10.0f;
		
		
		this->currentScene->addGameObject(projectile);
		projectile->setTranslation(pos,false);
		projectile->rigidBody->setLinearFactor(btVector3(1,1,1));
		projectile->rigidBody->applyCentralForce(velocity);
		//projectile->rigidBody->applyTorque(spin);
		//projectile->rigidBody->applyCentralForce(velocity);*/
	}

}
void World::mouseMoved(sf::Event event)
{
	InputHandler* inputHandler = Singleton<InputHandler>::Instance();
	glm::ivec2 mousePos = inputHandler->getMousePos();
	glm::ivec2 prevMousePos = inputHandler->getPrevMousePos();
	int x = mousePos.x;
	int y = mousePos.y;
	int prevX = prevMousePos.x;
	int prevY = prevMousePos.y;
	int mouseXDiff = (x - prevX);
	int mouseYDiff = (y - prevY);

	bool altIsDown = true;//inputHandler->isAltDown();
	if(inputHandler->isLeftMouseDown() && altIsDown)
	{	
		float scaleFactor = .008f;
		float mouseXDifference = -(float)mouseXDiff * scaleFactor;
		float mouseYDifference = -(float)mouseYDiff * scaleFactor;
		this->currentScene->currentCamera->rotate(mouseXDifference,mouseYDifference);
	}
	else if(inputHandler->isMiddleMouseDown() && altIsDown)
	{
		float scaleFactor = .01f;
		float mouseXDifference = -(float)mouseXDiff * scaleFactor;
		float mouseYDifference = (float)mouseYDiff * scaleFactor;
		this->currentScene->currentCamera->pan(mouseXDifference,mouseYDifference);
	}
	else if(inputHandler->isRightMouseDown() && altIsDown)
	{
		float scaleFactor = .05f;
		float mouseYDifference = -(float)mouseYDiff * scaleFactor;
		this->currentScene->currentCamera->zoom(mouseYDifference);
	}
}
void World::mouseScrollWheelMoved(sf::Event event)
{
	int delta = event.mouseWheel.delta;
	float scaleFactor = 1.0f;
	this->currentScene->currentCamera->zoom(delta*scaleFactor);
}

/*----------------------------
		Other
----------------------------*/
void World::update()
{

	this->currentScene->update();


	//WASD movement
	bool wDown = Singleton<InputHandler>::Instance()->isKeyDown(sf::Keyboard::W);
	bool aDown = Singleton<InputHandler>::Instance()->isKeyDown(sf::Keyboard::A);
	bool sDown = Singleton<InputHandler>::Instance()->isKeyDown(sf::Keyboard::S);
	bool dDown = Singleton<InputHandler>::Instance()->isKeyDown(sf::Keyboard::D);
	bool spaceDown = Singleton<InputHandler>::Instance()->isKeyDown(sf::Keyboard::Space);
	

	if(wDown || aDown || sDown || dDown || spaceDown)
	{
		if(!testModel->isAnimationInProgress())
		{
			testModel->resumeAnimationClip();
		}

		float magnitude = .01f;

		glm::vec3 translation(0.0f);
		bool firstPersonCamera = this->currentScene->currentCamera->type == CameraObject::FIRST_PERSON;
		if(firstPersonCamera)
		{
			glm::vec3 lookDir = this->currentScene->currentCamera->getLookDir();
			float angle = atan2(lookDir.x, lookDir.z);
			testModel->setRotation(glm::vec3(0,1,0), Utils::radToDeg*angle, false);
			translation.x = lookDir.x;
			translation.z = lookDir.z;
		}
		
		if(wDown)
		{
			if(firstPersonCamera) testModel->translate(translation*magnitude, false);
			else  testModel->translateZ(.01f, false);
		}
		if(aDown)
		{
			testModel->translateX(-.01f, false);
		}
		if(sDown)
		{
			if(firstPersonCamera) testModel->translate(translation*-magnitude, false);
			else testModel->translateZ(-.01f, false);
		}
		if(dDown)
		{
			testModel->translateX(.01f, false);
		}


	}
	else
	{
		testModel->pauseAnimationClip();
	}

	bool upDown = Singleton<InputHandler>::Instance()->isKeyDown(sf::Keyboard::Up);
	bool downDown = Singleton<InputHandler>::Instance()->isKeyDown(sf::Keyboard::Down);

	if(upDown)
	{
		testModel->material.transparency += .005f;
		if(testModel->material.transparency > 1.0f)
		{
			testModel->material.transparency = 1.0f;
		}
	}
	else if(downDown)
	{
		testModel->material.transparency -= .005f;
		if(testModel->material.transparency < 0.0f)
		{
			testModel->material.transparency = 0.0f;
		}
	}


	
	
}