#pragma once

#include <glm/glm.hpp>



struct Face
{
    Face(void);
    virtual ~Face(void);

    glm::vec3 vertices[3];
    glm::vec3 center;
    glm::vec3 normal;
};


