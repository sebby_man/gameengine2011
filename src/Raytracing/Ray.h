#pragma once

#include <glm/glm.hpp>



class Ray
{
public:
    glm::vec3 start;
    glm::vec3 end;
    glm::vec3 direction;

    glm::vec3 directionInverse;
    int sign[3];


    //default constructor
	Ray(){};

    Ray(glm::vec3& startPoint, glm::vec3& rayDir);
    virtual ~Ray(void);

};