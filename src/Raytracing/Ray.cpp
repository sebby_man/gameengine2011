#include "Ray.h"



	Ray::Ray(glm::vec3& startPoint, glm::vec3& rayDir)
	{
		start = startPoint;
		direction = glm::normalize(rayDir);

		directionInverse = 1.0f/direction;
		sign[0] = (directionInverse[0] < 0);
		sign[1] = (directionInverse[1] < 0);
		sign[2] = (directionInverse[2] < 0);
	}


	Ray::~Ray(void)
	{
	}

