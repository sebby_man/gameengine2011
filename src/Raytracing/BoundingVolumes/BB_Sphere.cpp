#include "BB_Sphere.h"


BB_Sphere::BB_Sphere(float radiusToUse)
{
    radius = radiusToUse;
}

RayIntersectionData* BB_Sphere::intersect(const Ray &ray) const
{

    float compB = 2 * glm::dot(ray.direction, ray.start);
    float compC = glm::dot(ray.start, ray.start) - radius*radius;

    float discriminant = compB*compB - 4.0f*compC;
    if(discriminant >= 0.0f)
    {
        float discriminantSqrt = sqrt(discriminant);
        float t0 = (-compB - discriminantSqrt)/2;
        if(t0 > 0)
        {
            RayIntersectionData* intersectionData = new RayIntersectionData();
            intersectionData->t = t0;
            glm::vec3 position = ray.start + ray.direction*t0;
            intersectionData->position = position;
            intersectionData->normal = position/radius;
            return validateIntersection(intersectionData, ray);
        }
        else
        {
            float t1 = (-compB + discriminantSqrt)/2;
            if(t1 > 0)
            {
                RayIntersectionData* intersectionData = new RayIntersectionData();
                intersectionData->t = t1;
                glm::vec3 position = ray.start + ray.direction*t1;
                intersectionData->position = position;
                intersectionData->normal = position/radius;
                return validateIntersection(intersectionData, ray);
            }
        }
    }


    //no collision
    return 0;


}


BB_Sphere::~BB_Sphere(void)
{
}

