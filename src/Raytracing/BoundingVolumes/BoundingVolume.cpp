#include "BoundingVolume.h"


BoundingVolume::BoundingVolume(void)
{
}


BoundingVolume::~BoundingVolume(void)
{
}

RayIntersectionData* BoundingVolume::validateIntersection(RayIntersectionData* intersectionData, const Ray& ray) const
{
    if(glm::dot(intersectionData->normal, -ray.direction) < 0)
    {
        intersectionData->normal*=-1;
        return intersectionData;
        //return intersectionData;
    }
    else
    {
        return intersectionData;
    }
}

