#pragma once

#include "../Ray.h"
#include <algorithm>
#include <iostream>
#include "../RayIntersectionData.h"


class BoundingVolume
{
public:
    BoundingVolume(void);
    virtual ~BoundingVolume(void);

    virtual RayIntersectionData* intersect(const Ray &ray) const = 0;
    RayIntersectionData* validateIntersection(RayIntersectionData* intersectionData, const Ray& ray) const;
};


