#include "BB_Mesh.h"


BB_Mesh::BB_Mesh(std::vector<Face*>& faceListToUse, float width, float height, float depth)
{
    faceList = faceListToUse;

    float radius = glm::length(glm::vec3(width, height, depth))/2;
    sphereBoundingVolume = new BB_Sphere(radius);
}

RayIntersectionData* BB_Mesh::intersect(const Ray &ray) const
{
    if(sphereBoundingVolume->intersect(ray) != 0)
    {
        float tBest = 1000001;
        glm::vec3 normalBest;


        for(unsigned int i = 0; i < faceList.size(); i++)
        {
            Face* face = faceList[i];
            const float EPSILON = .01f;

            glm::vec3 p1 = face->vertices[0];
            glm::vec3 p2 = face->vertices[1];
            glm::vec3 p3 = face->vertices[2];


            glm::vec3 normal = face->normal;
            float t = glm::dot(normal, p1 - ray.start) / glm::dot(normal, ray.direction);

            if(t >= 0)
            {
                glm::vec3 intersectPt = ray.start + t*ray.direction;

                float s = Utils::areaOfTriangle(p1,p2,p3);
                float s1 = Utils::areaOfTriangle(intersectPt, p2, p3)/s;
                float s2 = Utils::areaOfTriangle(intersectPt, p3, p1)/s;
                float s3 = Utils::areaOfTriangle(intersectPt, p1, p2)/s;

                if(s1 >= 0 && s2 >= 0 && s3 >= 0 && s1 <= 1 && s2 <= 1 && s3 <= 1)
                {
                    if(abs((s1 + s2 + s3) - 1) < EPSILON)
                    {
                        if(t < tBest)
                        {
                            tBest = t;
                            normalBest = normal;
                        }
                    }
                }
            }
        }

        if(tBest < 1000000)
        {
            RayIntersectionData* intersectionData = new RayIntersectionData();
            intersectionData->t = tBest;
            intersectionData->position = ray.start + ray.direction*tBest;
            intersectionData->normal = normalBest;
            return validateIntersection(intersectionData, ray);
        }
    }

    return 0;
}


BB_Mesh::~BB_Mesh(void)
{
}

