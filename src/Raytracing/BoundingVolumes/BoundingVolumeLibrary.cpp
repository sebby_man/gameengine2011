#include "BoundingVolumeLibrary.h"


BoundingVolumeLibrary::BoundingVolumeLibrary(void)
{

}


BoundingVolumeLibrary::~BoundingVolumeLibrary(void)
{
}

BB_Mesh* BoundingVolumeLibrary::getGenericMeshBoundingVolume(GL_MeshPrimitive* mesh)
{
    return new BB_Mesh(mesh->faceList, mesh->width, mesh->height, mesh->depth);
}

