#include "BB_Cylinder.h"


BB_Cylinder::BB_Cylinder(float heightToUse, float radiusToUse)
{
    height = heightToUse;
    radius = radiusToUse;
}

RayIntersectionData* BB_Cylinder::intersect(const Ray &ray) const
{
    //x^2 + z^2 - .25 = 0

    float t = -1;
    float rayDirX = ray.direction.x;
    float rayDirY = ray.direction.y;
    float rayDirZ = ray.direction.z;

    float rayStartX = ray.start.x;
    float rayStartY = ray.start.y;
    float rayStartZ = ray.start.z;


    float Aq = rayDirX*rayDirX + rayDirZ*rayDirZ;
    float Bq = 2.0f*rayStartX*rayDirX + 2.0f*rayStartZ*rayDirZ;
    float Cq = rayStartX*rayStartX + rayStartZ*rayStartZ - radius*radius;

    float discriminant = (Bq*Bq - 4*Aq*Cq);
    if(discriminant >= 0)
    {
        float discriminantSqrt = sqrt(discriminant);
        float t0 = (-Bq - discriminantSqrt)/(2*Aq);
        if(t0 > 0)
        {
            t = t0;
        }
        else
        {
            float t1 = (-Bq + discriminantSqrt)/(2*Aq);
            if(t1 > 0)
            {
                t = t1;
            }
        }
    }

    if(t > 0)
    {
        float intersectionPosY = rayStartY + t*rayDirY;
        if(intersectionPosY < height/2 && intersectionPosY > -height/2)
        {
            RayIntersectionData* intersectionData = new RayIntersectionData();
            intersectionData->t = t;
            glm::vec3 position = ray.start + ray.direction*t;
            intersectionData->position = position;
            intersectionData->normal = glm::normalize(glm::vec3(position.x, 0, position.z));
            return validateIntersection(intersectionData, ray);
        }
    }

    //intersect with top cap

    glm::vec3 planeNormalTop(0,1,0);
    glm::vec3 posInPlaneTop(0, height/2, 0);
    float tTop = glm::dot(planeNormalTop, posInPlaneTop - ray.start)/glm::dot(planeNormalTop, ray.direction);

    glm::vec3 planeNormalBottom(0,-1,0);
    glm::vec3 posInPlaneBottom(0, -height/2, 0);
    float tBottom = glm::dot(planeNormalBottom, posInPlaneBottom - ray.start)/glm::dot(planeNormalBottom, ray.direction);

    //intersect with top cap
    if(tTop > 0 && ((tTop < tBottom && tBottom > 0) || (tTop > tBottom && tBottom < 0)))
    {
        float distanceFromCenter = glm::distance(ray.start + tTop*ray.direction, posInPlaneTop);
        if(distanceFromCenter < radius)
        {
            RayIntersectionData* intersectionData = new RayIntersectionData();
            intersectionData->t = tTop;
            intersectionData->position = ray.start + ray.direction*tTop;
            intersectionData->normal = planeNormalTop;
            return validateIntersection(intersectionData, ray);
        }
    }

    //intersect with bottom cap
    if(tBottom > 0 && ((tBottom < tTop && tTop > 0) || (tBottom > tTop && tTop < 0)))
    {
        float distanceFromCenter = glm::distance(ray.start + tBottom*ray.direction, posInPlaneBottom);
        if(distanceFromCenter < radius)
        {
            RayIntersectionData* intersectionData = new RayIntersectionData();
            intersectionData->t = tBottom;
            intersectionData->position = ray.start + ray.direction*tBottom;
            intersectionData->normal = planeNormalBottom;
            return validateIntersection(intersectionData, ray);

        }
    }


    //no collision
    return 0;

}


BB_Cylinder::~BB_Cylinder(void)
{
}

