#include "BB_Cube.h"


BB_Cube::BB_Cube(const glm::vec3 &min, const glm::vec3 &max)
{
    parameters[0] = min;
    parameters[1] = max;
}


RayIntersectionData* BB_Cube::intersect(const Ray &ray) const
{
    float t0 = 0;
    float t1 = 1000000;

    float tmin, tmax, tymin, tymax, tzmin, tzmax;

    float value = (parameters[ray.sign[0]])[0];
    float value2 = ((parameters[ray.sign[0]])[0] - ray.start[0]);
    float value3 = ((parameters[ray.sign[0]])[0] - ray.start[0]) * ray.directionInverse[0];

    tmin = ((parameters[ray.sign[0]])[0] - ray.start[0]) * ray.directionInverse[0];
    tmax = ((parameters[1-ray.sign[0]])[0] - ray.start[0]) * ray.directionInverse[0];
    tymin = ((parameters[ray.sign[1]])[1] - ray.start[1]) * ray.directionInverse[1];
    tymax = ((parameters[1-ray.sign[1]])[1] - ray.start[1]) * ray.directionInverse[1];

    if ( (tmin > tymax) || (tymin > tmax) )
    {
        //ray doesn't intersect
        return 0;
    }
    if (tymin > tmin)
    {
        tmin = tymin;
    }
    if (tymax < tmax)
    {
        tmax = tymax;
    }

    tzmin = ((parameters[ray.sign[2]])[2] - ray.start[2]) * ray.directionInverse[2];
    tzmax = ((parameters[1-ray.sign[2]])[2] - ray.start[2]) * ray.directionInverse[2];

    if ( (tmin > tzmax) || (tzmin > tmax) )
    {
        //ray doesn't intersect
        return 0;
    }
    if (tzmin > tmin)
    {
        tmin = tzmin;
    }
    if (tzmax < tmax)
    {
        tmax = tzmax;
    }

    if((tmin < t1) && (tmax > t0))
    {
        float t;
        if(tmin < 0) t = tmax;
        else if(tmax < 0) t = tmin;
        else t = std::min(tmin, tmax);


        RayIntersectionData* intersectionData = new RayIntersectionData();
        intersectionData->t = t;
        glm::vec3 position = ray.start + ray.direction*t;
        intersectionData->position = position;

        float EPSILON = .01f;
        if(position.x < 0)
        {


            if(abs(position.x - parameters[0].x) < EPSILON)
            {
                intersectionData->normal = glm::vec3(-1,0,0);
                return validateIntersection(intersectionData, ray);
            }
        }
        else
        {
            if(abs(position.x - parameters[1].x) < EPSILON)
            {
                intersectionData->normal = glm::vec3(1,0,0);
                return validateIntersection(intersectionData, ray);
            }
        }

        if(position.y < 0)
        {
            if(abs(position.y - parameters[0].y) < EPSILON)
            {
                intersectionData->normal = glm::vec3(0,-1,0);
                return validateIntersection(intersectionData, ray);
            }
        }
        else
        {
            if(abs(position.y - parameters[1].y) < EPSILON)
            {
                intersectionData->normal = glm::vec3(0,1,0);
                return validateIntersection(intersectionData, ray);
            }
        }

        if(position.z < 0)
        {

            if(abs(position.z - parameters[0].z) < EPSILON)
            {
                intersectionData->normal = glm::vec3(0,0,-1);
                return validateIntersection(intersectionData, ray);
            }
        }
        else
        {
            if(abs(position.z - parameters[1].z) < EPSILON)
            {
                intersectionData->normal = glm::vec3(0,0,1);
                return validateIntersection(intersectionData, ray);
            }
        }


    }
    else
    {
        return 0;
    }

    return 0;
}


BB_Cube::~BB_Cube(void)
{
}



