#pragma once

#include "BoundingVolume.h"
#include "../Face.h"
#include <vector>
#include "../../Utils/MathUtils.h"
#include "BB_Sphere.h"



class BB_Mesh : public BoundingVolume
{
public:
    BB_Mesh(std::vector<Face*>& faceListToUse, float width, float height, float depth);
    virtual ~BB_Mesh(void);

    RayIntersectionData* intersect(const Ray &ray) const;

    std::vector<Face*> faceList;

    BB_Sphere* sphereBoundingVolume;

};


