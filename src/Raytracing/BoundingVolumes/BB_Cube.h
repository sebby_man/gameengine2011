#pragma once
#include "BoundingVolume.h"




class BB_Cube : public BoundingVolume
{
public:

    BB_Cube(const glm::vec3 &min, const glm::vec3 &max);
    virtual ~BB_Cube(void);

    RayIntersectionData* intersect(const Ray &ray) const;

protected:

    glm::vec3 parameters[2];
};


