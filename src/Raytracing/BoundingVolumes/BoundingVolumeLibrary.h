#pragma once

#include "BB_Cube.h"
#include "BB_Cylinder.h"
#include "BB_Sphere.h"
#include "BB_Mesh.h"
#include <glm/glm.hpp>
#include "../../Rendering/GL_MeshPrimitive.h"


class BoundingVolumeLibrary
{
public:
    BoundingVolumeLibrary(void);
    virtual ~BoundingVolumeLibrary(void);

    BB_Mesh* getGenericMeshBoundingVolume(GL_MeshPrimitive* mesh);

};


