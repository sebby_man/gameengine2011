#pragma once
#include "BoundingVolume.h"



class BB_Sphere : public BoundingVolume
{
public:
    BB_Sphere(float radiusToUse);
    virtual ~BB_Sphere(void);

    RayIntersectionData* intersect(const Ray &ray) const;

    float radius;
};

