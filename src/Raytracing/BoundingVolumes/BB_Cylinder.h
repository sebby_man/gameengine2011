#pragma once
#include "BoundingVolume.h"




class BB_Cylinder : public BoundingVolume
{
public:
    BB_Cylinder(float heightToUse, float radiusToUse);
    virtual ~BB_Cylinder(void);

    RayIntersectionData* intersect(const Ray &ray) const;

    float height;
    float radius;
};
