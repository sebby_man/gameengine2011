#pragma once

#include <glm/glm.hpp>


struct RayIntersectionData
{
    RayIntersectionData(void);
    virtual ~RayIntersectionData(void);

    float t;
    glm::vec3 position;
    glm::vec3 normal;

};


