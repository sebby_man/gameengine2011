These are all the GameObjects that appear in the game. Each file is an instance of some archetype within the data/class_archetypes folder. The game objects, like class archetypes, may override the values of their ancestor archetypes.

For example:

Let's say we want to create the game object Chair which derives from PhysicsObjectArchetype. The file would look like:

=================================
-----------CHAIR.xml-------------
=================================
<derived_class id="PhysicsObjectArchetype"/>

<!-- we may override PhysicsObjectArchetype, but we chose not to -->

<!--
<PhysicsObjectArchetype>
	<mass>10</mass>
	<restitution>1</restitution>
	<friction>1</friction>
</PhysicsObjectArchetype>
-->

<!-- we are overriding RenderObjectArchetype, telling it to use a different mesh and material than the default values-->
<RenderObjectArchetype>
	<mesh>chair</mesh>
	<shader>DiffuseLightingPhong</shader>
	<material>SMOOTH</material>
</RenderObjectArchetype>

=================================
-------------END-----------------
=================================


Some important notes: 

If we override an archetype as we just did, we must override ALL THE VALUES present in the archetype. Otherwise some values will not be set in the code, leading to unexpected results.

Overview of the pipeline:
Let's say we have a scene file with a Chair inside of it. When the scene is loaded, it will notice that Chair has the derived_class PhysicsObjectArchetype. The code will recognize that this archetype corresponds to the class PhysicsObject. The parent of PhysicsObject is RenderObject and the parent of RenderObject is GameObject. The important feature of GameObject (which every object in the game must derive from) is that it takes an XML as its constructor. So upon loading Chair, the code makes a new object in the following fashion:

	PhysicsObject* randomName = new PhysicsObject("data/game_objects/Chair.xml")
	
First PhysicsObject checks to see if Chair.xml overrides PhysicsObjectArchetype. We know it doesn't because the element was commented out inside the XML file. Because it is not overriden, it simply reads in the default values specified by PhysicsObjectArchetype.xml. Now PhysicsObject passes the XML to the constructor of its parent, RenderObject, which checks if Chair.xml overrides RenderObjectArchetype. It does, so we set those values and do not read the RenderObjectArchetype.xml file. The XML is finally passed to GameObject which doesn't do anything with it.

What we have now is a PhysicsObject that has all the properties specified in Chair.xml.

It's also important to notice that the files inside data/class_archetypes work in nearly identitical fashion to the files in data/game_objects, minus the derived_class element. Due to the similarities, game_object files can be treated as class_archetypes (but not the other way around).



	