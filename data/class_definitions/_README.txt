These files specify the default behavior of the GameObject-derived classes in the source code. Please do not rename or move files unless you know what you are doing.

With that said, these files exist so that it easy to customize class behaviors without having to recompile the source.